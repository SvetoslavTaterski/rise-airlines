﻿using DataContracts;
using DataContracts.Aircrafts;
using BusinessLogic.Commands;
using DataContracts.Exceptions;
using static Airlines.Persistence.Basic.GetDataFromFiles;
using static BusinessLogic.DatabaseOperations.FlightOperations;
using static BusinessLogic.DatabaseOperations.AirlineOperations;
using static BusinessLogic.DatabaseOperations.AirportOperations;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0060 // Remove unused parameter
#pragma warning disable IDE0018 // Inline variable declaration
#pragma warning disable IDE0045 // Convert to conditional expression
#pragma warning disable IDE0059 // Unnecessary assignment of a value
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
#pragma warning disable CS8602 // Dereference of a possibly null reference.

namespace Airlines
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //PrintFlightWithUsing();
            //PrintAirlinesWithUsing();
            //PrintAirportsWithUsing();
            //Console.WriteLine("Airports-----------------------------------");
            //PrintAirportById(1);
            //PrintAirportByName("Heathrow Airport");
            //PrintAirportsByCountry("USA");

            //var airport = new Persistence.Basic.Entities.Airport()
            //{
            //    AirportName = "Septemvri Airport",
            //    Country = "Bulgaria",
            //    City = "Septemvri",
            //    Code = "SEP",
            //    RunwayCount = 4,
            //    Founded = DateOnly.Parse("1930-07-01")
            //};

            ////AddAirport(airport);

            ////RemoveAirportById(5);

            //var airport = new Persistence.Basic.Entities.Airport()
            //{
            //    AirportName = "SeptemvriWEEE Airport",
            //    Country = "Bulgaria",
            //    City = "Septemvri",
            //    Code = "SPP",
            //    RunwayCount = 4,
            //    Founded = DateOnly.Parse("1980-07-01")
            //};

            //UpdateAirport(airport);

            Console.WriteLine("Airlines------------------------------");

            //PrintAirlineById(1);
            //PrintAirlineByName("Delta Air Lines");
            //PrintAirlinesByFleetSize(280);

            //var airline = new Persistence.Basic.Entities.Airline()
            //{
            //    AirlineName = "Septemri Airline",
            //    AirlineDescription = "Random Airline description bla bla bla",
            //    FleetSize = 280,
            //    Founded = DateOnly.Parse("1980-07-01")
            //};

            //AddAirline(airline);

            //RemoveAirlineById(4);

            //var airline = new Persistence.Basic.Entities.Airline()
            //{
            //    AirlineName = "Air France",
            //    AirlineDescription = "New description bla bla bla",
            //    FleetSize = 220,
            //    AirlineId = 3,
            //    Founded = DateOnly.Parse("1980-07-01")
            //};

            //UpdateAirline(airline);

            //Console.WriteLine("Flights-------------------");
            //PrintFlightById(1);
            //PrintFlightByFlightNumber("ABC123");
            //PrintFlightsByTimeDifference(240);

            //var flight = new Persistence.Basic.Entities.Flight()
            //{
            //    FlightNumber = "SVE123",
            //    Departure = DateTime.Parse("2024-04-23 21:00:00.000"),
            //    Arrival = DateTime.Parse("2024-04-23 21:30:00.000"),
            //    FromAirportId = 1,
            //    ToAirportId = 2,
            //    TimeDifference = 250
            //};

            //AddFlight(flight);

            //RemoveFlightById(4);

            //var flight = new Persistence.Basic.Entities.Flight()
            //{
            //    FlightId = 1,
            //    FromAirportId = 1,
            //    ToAirportId = 2,
            //    Departure = DateTime.Parse("2024-04-17 08:00:00.000"),
            //    Arrival = DateTime.Parse("2024-04-17 12:00:00.000"),
            //    FlightNumber = "NEW123",
            //    TimeDifference = 300
            //};

            //UpdateFlight(flight);

            HashSet<Airport> airports;
            HashSet<Airline> airlines;
            HashSet<Flight> flights;
            List<Aircraft> aircrafts;
            LinkedList<Flight> route = new LinkedList<Flight>();
            Queue<ICommand> commandsQueue = new Queue<ICommand>();
            var graph = new Graph();

            GetData(out airports, out airlines, out flights, out aircrafts, out graph);

            bool isBatchModeOn = false;

            string command = "";

            try
            {
                while (command != "end")
                {
                    Console.WriteLine("Do you want to run the program on batch mode: batch start / batch no / batch run / batch cancel");
                    Console.WriteLine("Enter command: exist <airline name> | list <input data> <from> | end");
                    Console.WriteLine("| search <search type> <search item name> | sort <input data> [order]");
                    Console.WriteLine();
                    Console.WriteLine("Enter command for flight management: route new | route add <Flight Identifier> | route remove | route print");
                    Console.WriteLine("Enter command to find route: route find <DestinationAirport> <SourceAirport> | route check <Start Airport> <End Airport>");
                    Console.WriteLine("Enter command to find route by given strategy: route search <Start Airport> <End Airport> <Strategy>");
                    Console.WriteLine();
                    Console.WriteLine("Enter command to reserve cargo or ticket: reserve cargo <Flight Identifier> <Cargo Weight> <Cargo Volume>");
                    Console.WriteLine("reserve ticket <Flight Identifier> <Seats> <Small Baggage Count> <Large Baggage Count>");
                    command = Console.ReadLine();

                    string[] commandLine = command.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                    string whatToDo = commandLine[0];

                    if (whatToDo == "exist")
                    {
                        ExistCommand commandToExecute = ExistCommand.CreateExistCommand(commandLine, airlines);

                        if (isBatchModeOn)
                        {
                            commandsQueue.Enqueue(commandToExecute);
                        }
                        else
                        {
                            Console.WriteLine(commandToExecute.Execute());
                        }
                    }
                    else if (whatToDo == "list")
                    {
                        ListCommand commandToExecute = ListCommand.CreateListCommand(commandLine, airports);

                        if (isBatchModeOn)
                        {
                            commandsQueue.Enqueue(commandToExecute);
                        }
                        else
                        {
                            Console.WriteLine(commandToExecute.Execute());
                        }
                    }
                    else if (whatToDo == "sort")
                    {
                        SortCommand commandToExecute = SortCommand.CreateSortCommand(commandLine, airports, airlines, flights);

                        if (isBatchModeOn)
                        {
                            commandsQueue.Enqueue(commandToExecute);
                        }
                        else
                        {
                            Console.WriteLine(commandToExecute.Execute());
                        }
                    }
                    else if (whatToDo == "search")
                    {
                        SearchCommand commandToExecute = SearchCommand.CreateSearchCommand(commandLine, airports, airlines, flights);

                        if (isBatchModeOn)
                        {
                            commandsQueue.Enqueue(commandToExecute);
                        }
                        else
                        {
                            Console.WriteLine(commandToExecute.Execute());
                        }
                    }
                    else if (whatToDo == "route")
                    {
                        string routeCommand = commandLine[1];

                        if (routeCommand == "new")
                        {
                            NewCommand commandToExecute = NewCommand.CreateNewCommand(route);

                            if (isBatchModeOn)
                            {
                                commandsQueue.Enqueue(commandToExecute);
                            }
                            else
                            {
                                Console.WriteLine(commandToExecute.Execute());
                            }
                        }
                        else if (routeCommand == "add")
                        {
                            AddFlightCommand commandToExecute = AddFlightCommand.CreateAddFlightCommand(commandLine[2], route, flights);

                            if (isBatchModeOn)
                            {
                                commandsQueue.Enqueue(commandToExecute);
                            }
                            else
                            {
                                Console.WriteLine(commandToExecute.Execute());
                            }
                        }
                        else if (routeCommand == "remove")
                        {
                            RemoveCommand commandToExecute = RemoveCommand.CreateRemoveCommand(route);

                            if (isBatchModeOn)
                            {
                                commandsQueue.Enqueue(commandToExecute);
                            }
                            else
                            {
                                Console.WriteLine(commandToExecute.Execute());
                            }
                        }
                        else if (routeCommand == "print")
                        {
                            PrintCommand commandToExecute = PrintCommand.CreatePrintCommand(route);

                            if (isBatchModeOn)
                            {
                                commandsQueue.Enqueue(commandToExecute);
                            }
                            else
                            {
                                Console.WriteLine(commandToExecute.Execute());
                            }
                        }
                        else if (routeCommand == "find")
                        {
                            string destinationAirport = commandLine[2];
                            string sourceAirport = commandLine[3];

                            FindCommand commandToExecute = FindCommand.CreateFindCommand(graph, destinationAirport, sourceAirport);

                            if (isBatchModeOn)
                            {
                                commandsQueue.Enqueue(commandToExecute);
                            }
                            else
                            {
                                Console.WriteLine(commandToExecute.Execute());
                            }
                        }
                        else if (routeCommand == "check")
                        {
                            string startingAirport = commandLine[2];
                            string destinationAIrport = commandLine[3];

                            CheckCommand commandToExecute = CheckCommand.CreateCheckCommand(graph, destinationAIrport, startingAirport);

                            if (isBatchModeOn)
                            {
                                commandsQueue.Enqueue(commandToExecute);
                            }
                            else
                            {
                                Console.WriteLine(commandToExecute.Execute());
                            }
                        }
                        else if (routeCommand == "search")
                        {
                            string startingAirport = commandLine[2];
                            string destinationAIrport = commandLine[3];
                            string strategyName = commandLine[4];

                            if (strategyName == "cheap")
                            {
                                SearchRouteCommand commandToExecute = SearchRouteCommand.CreateSearchRouteCommand(startingAirport, destinationAIrport, graph);

                                if (isBatchModeOn)
                                {
                                    commandsQueue.Enqueue(commandToExecute);
                                }
                                else
                                {
                                    Console.WriteLine(commandToExecute.Execute());
                                }
                            }
                            else if (strategyName == "short")
                            {
                                SearchRouteCommand commandToExecute = SearchRouteCommand.CreateSearchRouteCommand(startingAirport, destinationAIrport, graph);

                                if (isBatchModeOn)
                                {
                                    commandsQueue.Enqueue(commandToExecute);
                                }
                                else
                                {
                                    Console.WriteLine(commandToExecute.ExecuteByTime());
                                }
                            }
                            else if (strategyName == "stops")
                            {
                                SearchRouteCommand commandToExecute = SearchRouteCommand.CreateSearchRouteCommand(startingAirport, destinationAIrport, graph);

                                if (isBatchModeOn)
                                {
                                    commandsQueue.Enqueue(commandToExecute);
                                }
                                else
                                {
                                    Console.WriteLine(commandToExecute.ExecuteByStops());
                                }
                            }
                            else
                            {
                                throw new InvalidInputException("Invalid strategy");
                            }
                        }
                        else
                        {
                            throw new InvalidCommandException("Invalid command");
                        }
                    }
                    else if (whatToDo == "reserve")
                    {
                        string whatToReserve = commandLine[1];

                        if (whatToReserve == "cargo")
                        {
                            string id = commandLine[2];
                            int weight = int.Parse(commandLine[3]);
                            double volume = double.Parse(commandLine[4]);

                            ReserveCargoCommand commandToExecute = ReserveCargoCommand.CreateReserveCargoCommand(id, weight, volume, flights);

                            if (isBatchModeOn)
                            {
                                commandsQueue.Enqueue(commandToExecute);
                            }
                            else
                            {
                                Console.WriteLine(commandToExecute.Execute());
                            }
                        }
                        else if (whatToReserve == "ticket")
                        {
                            string id = commandLine[2];
                            int seats = int.Parse(commandLine[3]);
                            int smallBaggageCount = int.Parse(commandLine[4]);
                            int largeBaggageCount = int.Parse(commandLine[5]);

                            ReserveTicketCommand commandToExecute = ReserveTicketCommand.CreateReserveTicketCommand(id, seats, smallBaggageCount, largeBaggageCount, flights);

                            if (isBatchModeOn)
                            {
                                commandsQueue.Enqueue(commandToExecute);
                            }
                            else
                            {
                                Console.WriteLine(commandToExecute.Execute());
                            }
                        }
                        else
                        {
                            throw new InvalidCommandException("Invalid command");
                        }
                    }
                    else if (whatToDo == "end")
                    {
                        continue;
                    }
                    else if (whatToDo == "batch")
                    {
                        string batchCommand = commandLine[1];

                        if (batchCommand == "start")
                        {
                            isBatchModeOn = true;
                        }
                        else if (batchCommand == "run")
                        {
                            foreach (var commandToExecute in commandsQueue)
                            {
                                Console.WriteLine(commandToExecute.Execute());
                            }
                            commandsQueue.Clear();
                        }
                        else if (batchCommand == "cancel")
                        {
                            isBatchModeOn = false;
                            commandsQueue.Clear();
                        }
                        else if (batchCommand == "no")
                        {
                            isBatchModeOn = false;
                        }
                        else
                        {
                            throw new InvalidCommandException("Invalid command");
                        }
                    }
                    else
                    {
                        throw new InvalidCommandException("Invalid command");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}

