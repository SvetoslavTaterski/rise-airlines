﻿using DataContracts;
using DataContracts.Aircrafts;
using DataContracts.Exceptions;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
namespace Airlines.Persistence.Basic
{
    public static class GetDataFromFiles
    {
        public static void GetData(out HashSet<Airport> airports, out HashSet<Airline> airlines, out HashSet<Flight> flights, out List<Aircraft> aircrafts, out Graph graph)
        {
            airports = [];
            airlines = [];
            flights = [];
            aircrafts = [];
            graph = new Graph();

            using (StreamReader sr = new StreamReader("../../../../Airlines.Persistence.Basic/CSVFiles/AirportData.csv"))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    string[] propertyData = line.Split(", ", StringSplitOptions.RemoveEmptyEntries);

                    if (propertyData.Length == 4)
                    {
                        string identifier = propertyData[0];
                        string name = propertyData[1];
                        string city = propertyData[2];
                        string country = propertyData[3];

                        var newAirport = new Airport(identifier, name, city, country);

                        airports.Add(newAirport);
                    }
                    else
                    {
                        Console.WriteLine("Invalid input!");
                    }
                }
            }

            using (StreamReader sr = new StreamReader("../../../../Airlines.Persistence.Basic/CSVFiles/AirlineData.csv"))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    string name = line;

                    var newAirline = new Airline(name);

                    airlines.Add(newAirline);

                }
            }

            using (StreamReader sr = new StreamReader("../../../../Airlines.Persistence.Basic/CSVFiles/AircraftData.csv"))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    string[] propertyData = line.Split(", ", StringSplitOptions.RemoveEmptyEntries);

                    if (propertyData.Length == 4)
                    {
                        string name = propertyData[0];

                        if (propertyData.Any(e => e == "-"))
                        {
                            if (propertyData[3] == "-")
                            {
                                int load = int.Parse(propertyData[1]);
                                double volume = double.Parse(propertyData[2]);

                                var newCargoAircraft = new CargoAircraft(name, load, volume);
                                aircrafts.Add(newCargoAircraft);
                            }
                            else if (propertyData[1] == "-" && propertyData[2] == "-")
                            {
                                int seats = int.Parse(propertyData[3]);

                                var newPrivateAircraft = new PrivateAircraft(name, seats);
                                aircrafts.Add(newPrivateAircraft);
                            }
                        }
                        else if (!propertyData.Any(e => e == "-"))
                        {
                            int load = int.Parse(propertyData[1]);
                            double volume = double.Parse(propertyData[2]);
                            int seats = int.Parse(propertyData[3]);

                            var newPassengerAircraft = new PassengerAircraft(name, load, volume, seats);
                            aircrafts.Add(newPassengerAircraft);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid input!");
                    }
                }
            }

            using (StreamReader sr = new StreamReader("../../../../Airlines.Persistence.Basic/CSVFiles/FlightData.csv"))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    string[] propertyData = line.Split(",", StringSplitOptions.RemoveEmptyEntries);

                    if (propertyData.Length == 5)
                    {
                        string id = propertyData[0];
                        string departureAirport = propertyData[1];
                        string arrivalAirport = propertyData[2];
                        double price = double.Parse(propertyData[3]);
                        double time = double.Parse(propertyData[4]);

                        var newFlight = new Flight(id, departureAirport, arrivalAirport, price, time);

                        flights.Add(newFlight);
                    }
                    else
                    {
                        throw new InvalidInputException("Invalid input!");
                    }
                }
            }

            using (StreamReader sr = new StreamReader("../../../../Airlines.Persistence.Basic/CSVFiles/FlightRouteData.csv"))
            {
                string line;
                List<string> lines = [];
                Dictionary<string, bool> airportsAdded = [];

                while ((line = sr.ReadLine()) != null)
                {
                    lines.Add(line);
                }

                string startAirport = lines[0];


                for (int i = 1; i < lines.Count; i++)
                {
                    string flightIdentifier = lines[i];

                    Flight? currentFlight = flights.First(f => f.Id == flightIdentifier);

                    string arrivalAirport = currentFlight.ArrivalAirport;
                    string departureAirport = currentFlight.DepartureAirport;
                    double price = currentFlight.Price;
                    double time = currentFlight.Time;

                    graph.AddFlight(departureAirport, arrivalAirport, price, time);
                }
            }
        }
    }
}

