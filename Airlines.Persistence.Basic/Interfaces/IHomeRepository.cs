﻿namespace Airlines.Persistence.Basic.Interfaces;

public interface IHomeRepository
{
    public int GetCount<TEntity>() where TEntity : class;
}
