﻿using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic.Interfaces
{
    public interface IAirportRepository
    {
        List<Airport> GetAllAirports();

        Airport GetAirportById(int id);

        Airport GetAirportByName(string name);

        List<Airport> GetAirportsByCountry(string country);

        void AddAirport(Airport airport);

        void RemoveAirportById(int id);

        void UpdateAirport(Airport airport);

        void CreateAirport(Airport airport);
    }
}
