﻿using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic.Interfaces
{
    public interface IAirlineRepository
    {
        List<Airline> GetAllAirlines();

        Airline GetAirlineById(int id);

        Airline GetAirlineByName(string name);

        List<Airline> GetAirlinesByFleetSize(int fleetSize);

        void AddAirline(Airline airline);

        void RemoveAirlineById(int id);

        void UpdateAirline(Airline airline);

        void CreateAirline(Airline airline);
    }
}
