﻿using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic.Interfaces
{
    public interface IFlightRepository
    {
        List<Flight> GetAllFlights();

        Flight GetFlightById(int id);

        Flight GetFlightByFlightNumber(string flightNumber);

        List<Flight> GetFlightsByTimeDifference(int timeDifference);

        void AddFlight(Flight flight);

        void RemoveFlightById(int id);

        void UpdateFlight(Flight flight);

        void CreateFlight(Flight flight);
    }
}
