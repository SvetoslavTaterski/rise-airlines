﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;

#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable IDE0305 // Simplify collection initialization
#pragma warning disable IDE0021 // Use expression body for constructor
namespace Airlines.Persistence.Basic.Classes
{
    public class FlightRepository : IFlightRepository, IDisposable
    {
        private readonly RISEAirlinesContext _context;

        public FlightRepository(RISEAirlinesContext context)
        {
            _context = context;
        }

        public void Dispose()
        {

        }

        public void CreateFlight(Flight flight)
        {
            if (flight.FlightNumber.Length > 6)
            {
                throw new ArgumentException("Flight number must be 6 characters or less.", nameof(flight.FlightNumber));
            }

            var newFlight = new Flight
            {
                FlightNumber = flight.FlightNumber,
                FromAirportId = flight.FromAirportId,
                ToAirportId = flight.ToAirportId,
                Departure = flight.Departure,
                Arrival = flight.Arrival,
                TimeDifference = flight.TimeDifference
            };

            _context.Flights.Add(newFlight);
            _context.SaveChanges();
        }

        public List<Flight> GetAllFlights()
        {
            using var context = new RISEAirlinesContext();
            return context.Flights.ToList();
        }

        public Flight GetFlightByFlightNumber(string flightNumber)
        {
            using var context = new RISEAirlinesContext();
            var flight = context.Flights.FirstOrDefault(a => a.FlightNumber == flightNumber);

            if (flight == null)
            {
                throw new ArgumentNullException("There is no such Flight");
            }
            else
            {
                return flight;
            }
        }

        public Flight GetFlightById(int id)
        {
            using var context = new RISEAirlinesContext();
            var flight = context.Flights.FirstOrDefault(a => a.FlightId == id);

            if (flight == null)
            {
                throw new ArgumentNullException("There is no such Flight");
            }
            else
            {
                return flight;
            }
        }

        public List<Flight> GetFlightsByTimeDifference(int timeDifference)
        {
            using var context = new RISEAirlinesContext();
            var flights = context.Flights.Where(a => a.TimeDifference == timeDifference).ToList();

            if (flights.Count == 0)
            {
                throw new InvalidOperationException("The collection is empty!");
            }
            else
            {
                return flights;
            }
        }


        public void AddFlight(Flight flight)
        {
            using var context = new RISEAirlinesContext();

            context.Flights.Add(flight);
            context.SaveChanges();
        }

        public void RemoveFlightById(int id)
        {
            using var context = new RISEAirlinesContext();

            var flightToRemove = GetFlightById(id);

            context.Flights.Remove(flightToRemove);

            context.SaveChanges();
        }

        public void UpdateFlight(Flight flight)
        {
            using var context = new RISEAirlinesContext();

            context.Flights.Update(flight);
            context.SaveChanges();
        }
    }
}
