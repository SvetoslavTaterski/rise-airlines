﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;

namespace Airlines.Persistence.Basic.Classes;

#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable IDE0290 // Use primary constructor
public class HomeRepository : IHomeRepository, IDisposable
{
    private readonly RISEAirlinesContext _context;

    public HomeRepository(RISEAirlinesContext context)
    {
        _context = context;
    }

    public void Dispose()
    {

    }

    public int GetCount<TEntity>() where TEntity : class
        => _context.Set<TEntity>().Count();
}
