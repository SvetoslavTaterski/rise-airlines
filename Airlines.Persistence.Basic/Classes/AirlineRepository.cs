﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;

#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable IDE0305 // Simplify collection initialization
namespace Airlines.Persistence.Basic.Classes
{
    public class AirlineRepository : IAirlineRepository, IDisposable
    {
        private readonly RISEAirlinesContext _context;

        public AirlineRepository(RISEAirlinesContext context)
        {
            _context = context;
        }

        public void Dispose()
        {

        }

        public void AddAirline(Airline airline)
        {
            using var context = new RISEAirlinesContext();

            context.Airlines.Add(airline);
            context.SaveChanges();
        }

        public List<Airline> GetAirlinesByFleetSize(int fleetSize)
        {
            using var context = new RISEAirlinesContext();
            var airlines = context.Airlines.Where(a => a.FleetSize == fleetSize).ToList();

            if (airlines.Count == 0)
            {
                throw new InvalidOperationException("The collection is empty!");
            }
            else
            {
                return airlines;
            }
        }

        public Airline GetAirlineById(int id)
        {
            using var context = new RISEAirlinesContext();
            var airline = context.Airlines.FirstOrDefault(a => a.AirlineId == id);

            if (airline == null)
            {
                throw new ArgumentNullException("There is no such Airline");
            }
            else
            {
                return airline;
            }
        }

        public Airline GetAirlineByName(string name)
        {
            using var context = new RISEAirlinesContext();
            var airline = context.Airlines.FirstOrDefault(a => a.AirlineName == name);

            if (airline == null)
            {
                throw new ArgumentNullException("There is no such Airline");
            }
            else
            {
                return airline;
            }
        }

        public List<Airline> GetAllAirlines()
        {
            using var context = new RISEAirlinesContext();
            return context.Airlines.ToList();
        }

        public void RemoveAirlineById(int id)
        {
            using var context = new RISEAirlinesContext();

            var airlineToRemove = GetAirlineById(id);

            context.Airlines.Remove(airlineToRemove);

            context.SaveChanges();
        }

        public void UpdateAirline(Airline airline)
        {
            using var context = new RISEAirlinesContext();

            context.Airlines.Update(airline);
            context.SaveChanges();
        }

        public void CreateAirline(Airline airline)
        {
            if (airline.FleetSize < 0)
            {
                throw new ArgumentException("Fleet size must be a positive number.", nameof(airline.FleetSize));
            }

            var newAirline = new Airline
            {
                AirlineName = airline.AirlineName,
                Founded = airline.Founded,
                FleetSize = airline.FleetSize,
                AirlineDescription = airline.AirlineDescription
            };

            _context.Airlines.Add(newAirline);
            _context.SaveChanges();
        }
    }
}
