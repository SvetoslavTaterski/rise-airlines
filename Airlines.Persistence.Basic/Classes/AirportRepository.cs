﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;

#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable IDE0290 // Use primary constructor
namespace Airlines.Persistence.Basic.Classes
{
    public class AirportRepository : IAirportRepository, IDisposable
    {
        private readonly RISEAirlinesContext _context;

        public AirportRepository(RISEAirlinesContext context)
        {
            _context = context;
        }

        public void Dispose()
        {

        }
        public void AddAirport(Airport airport)
        {
            using var context = new RISEAirlinesContext();

            context.Airports.Add(airport);
            context.SaveChanges();
        }

        public Airport GetAirportById(int id)
        {
            using var context = new RISEAirlinesContext();
            var airport = context.Airports.FirstOrDefault(a => a.AirportId == id);

            if (airport == null)
            {
                throw new ArgumentNullException("There is no such Airport");
            }
            else
            {
                return airport;
            }
        }

        public Airport GetAirportByName(string name)
        {
            using var context = new RISEAirlinesContext();
            var airport = context.Airports.FirstOrDefault(a => a.AirportName == name);

            if (airport == null)
            {
                throw new ArgumentNullException("There is no such Airport");
            }
            else
            {
                return airport;
            }
        }

        public List<Airport> GetAirportsByCountry(string country)
        {
            using var context = new RISEAirlinesContext();
            var airports = context.Airports.Where(a => a.Country == country).ToList();

            if (airports.Count == 0)
            {
                throw new InvalidOperationException("The collection is empty!");
            }
            else
            {
                return airports;
            }
        }

        public List<Airport> GetAllAirports()
        {
            using var context = new RISEAirlinesContext();
            var airports = context.Airports.ToList();

            if (airports.Count == 0)
            {
                throw new InvalidOperationException("The collection is empty!");
            }
            else
            {
                return airports;
            }
        }

        public void RemoveAirportById(int id)
        {
            using var context = new RISEAirlinesContext();

            var airportToRemove = GetAirportById(id);

            context.Airports.Remove(airportToRemove);

            context.SaveChanges();
        }

        public void UpdateAirport(Airport airport)
        {
            using var context = new RISEAirlinesContext();

            context.Airports.Update(airport);
            context.SaveChanges();
        }

        public void CreateAirport(Airport airport)
        {
            if (airport.RunwayCount < 0)
            {
                throw new ArgumentException("Runway count must be a positive number.", nameof(airport.RunwayCount));
            }

            var newAirport = new Airport
            {
                AirportName = airport.AirportName,
                Country = airport.Country,
                City = airport.City,
                Code = airport.Code,
                RunwayCount = airport.RunwayCount,
                Founded = airport.Founded,
            };

            _context.Airports.Add(newAirport);
            _context.SaveChanges();
        }
    }
}
