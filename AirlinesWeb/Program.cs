using Airlines.Persistence.Basic.Classes;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Configuration;

namespace AirlinesWeb;

#pragma warning disable IDE0058 // Expression value is never used
public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.
        builder.Services.AddDbContext<RISEAirlinesContext>(options =>
                options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

        builder.Services.AddScoped<IFlightRepository, FlightRepository>();
        builder.Services.AddScoped<IAirportRepository, AirportRepository>();
        builder.Services.AddScoped<IAirlineRepository, AirlineRepository>();
        builder.Services.AddScoped<IHomeRepository, HomeRepository>();

        builder.Services.AddControllersWithViews();

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (!app.Environment.IsDevelopment())
        {
            app.UseExceptionHandler("/Home/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseStaticFiles();

        app.UseRouting();

        app.UseAuthorization();

        app.MapControllerRoute(
            name: "default",
            pattern: "{controller=Home}/{action=Index}/{id?}");

        app.Run();
    }
}
