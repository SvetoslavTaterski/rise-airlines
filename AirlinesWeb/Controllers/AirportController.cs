﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AirlinesWeb.Controllers;
#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0060 // Remove unused parameter
#pragma warning disable IDE0022 // Use expression body for method
#pragma warning disable IDE0021 // Use expression body for constructor
public class AirportController : Controller
{
    private readonly IAirportRepository _airportRepository;

    public AirportController(IAirportRepository airportRepository)
    {
        _airportRepository = airportRepository;
    }

    // GET: AirportController
    public ActionResult Index(string searchTerm, string country, string city, string code, string runwayCount)
    {
        var airports = _airportRepository.GetAllAirports();

        if (!string.IsNullOrEmpty(searchTerm))
        {
            airports = airports.Where(f => f.AirportName.Contains(searchTerm)).ToList();
        }

        if (!string.IsNullOrEmpty(country))
        {
            airports = airports.Where(f => f.Country.Contains(country)).ToList();
        }

        if (!string.IsNullOrEmpty(city))
        {
            airports = airports.Where(f => f.City.ToString().Contains(city)).ToList();
        }

        if (!string.IsNullOrEmpty(code))
        {
            airports = airports.Where(f => f.Code.ToString().Contains(code)).ToList();
        }

        if (!string.IsNullOrEmpty(runwayCount))
        {
            airports = airports.Where(a => a.RunwayCount == int.Parse(runwayCount)).ToList();
        }

        ViewBag.CurrentFilter = searchTerm;

        return View(airports);
    }

    // GET: AirportController/Details/5
    public ActionResult Details(int id)
    {
        return View();
    }

    // GET: AirportController/Create
    public ActionResult Create()
    {
        return View();
    }

    // POST: AirportController/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult CreateNewAirport(Airport airport)
    {
        if (airport.Founded < new DateOnly(2024, 1, 1))
        {
            ModelState.AddModelError("Founded", "The date should be earlier than now");
        }

        if (!string.IsNullOrEmpty(airport.AirportName))
        {
            ModelState.AddModelError("AirportName", "Name must be filled");
        }

        if (!string.IsNullOrEmpty(airport.City))
        {
            ModelState.AddModelError("City", "City must be filled");
        }

        if (!string.IsNullOrEmpty(airport.Country))
        {
            ModelState.AddModelError("Country", "Country must be filled");
        }

        if (airport.RunwayCount > 0)
        {
            ModelState.AddModelError("RunwayCount", "Runways must be positive number");
        }

        if (airport.Code.Length == 3 && !string.IsNullOrEmpty(airport.AirportName) && !airport.Code.All(char.IsDigit))
        {
            ModelState.AddModelError("Code", "Code must be 3 char lenght");
        }

        if (ModelState.IsValid)
        {
            _airportRepository.CreateAirport(airport);

            return RedirectToAction(nameof(Index));
        }

        return RedirectToAction(nameof(Index));
    }

    // GET: AirportController/Edit/5
    public ActionResult Edit(int id)
    {
        return View();
    }

    // POST: AirportController/Edit/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit(int id, IFormCollection collection)
    {
        try
        {
            return RedirectToAction(nameof(Index));
        }
        catch
        {
            return View();
        }
    }

    // GET: AirportController/Delete/5
    public ActionResult Delete(int id)
    {
        return View();
    }

    // POST: AirportController/Delete/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Delete(int id, IFormCollection collection)
    {
        try
        {
            return RedirectToAction(nameof(Index));
        }
        catch
        {
            return View();
        }
    }
}
