﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AirlinesWeb.Controllers;
#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0060 // Remove unused parameter
#pragma warning disable IDE0022 // Use expression body for method
#pragma warning disable IDE0021 // Use expression body for constructor
public class FlightController : Controller
{
    private readonly IFlightRepository _flightRepository;

    public FlightController(IFlightRepository repository)
    {
        _flightRepository = repository;
    }

    // GET: FlightController
    public ActionResult Index(string searchTerm, string flightNumber, string fromAirportId, string toAirportId)
    {
        var flights = _flightRepository.GetAllFlights();

        if (!string.IsNullOrEmpty(searchTerm))
        {
            flights = flights.Where(f => f.FlightNumber.Contains(searchTerm)).ToList();
        }

        if (!string.IsNullOrEmpty(flightNumber))
        {
            flights = flights.Where(f => f.FlightNumber.Contains(flightNumber)).ToList();
        }

        if (!string.IsNullOrEmpty(fromAirportId))
        {
            flights = flights.Where(f => f.FromAirportId.ToString().Contains(fromAirportId)).ToList();
        }

        if (!string.IsNullOrEmpty(toAirportId))
        {
            flights = flights.Where(f => f.ToAirportId.ToString().Contains(toAirportId)).ToList();
        }

        ViewBag.CurrentFilter = searchTerm;

        return View(flights);
    }

    // GET: FlightController/Details/5
    public ActionResult Details(int id)
    {
        return View();
    }

    // GET: FlightController/Create
    public ActionResult Create()
    {
        return View();
    }

    // POST: FlightController/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult CreateNewFlight(Flight flight)
    {
        if (flight.Arrival < DateTime.Now)
        {
            ModelState.AddModelError("Arrival", "The date should be later than now");
        }

        if (flight.Departure > flight.Arrival)
        {
            ModelState.AddModelError("Departure", "The date should be later than now");
        }

        if (flight.FlightNumber.Length > 6 && !string.IsNullOrEmpty(flight.FlightNumber))
        {
            ModelState.AddModelError("FlightNumber", "Number must be lower than 6 chars");
        }

        if (ModelState.IsValid)
        {
            _flightRepository.CreateFlight(flight);
            return RedirectToAction(nameof(Index));
        }

        return RedirectToAction(nameof(Index));
    }

    // GET: FlightController/Edit/5
    public ActionResult Edit(int id)
    {
        return View();
    }

    // POST: FlightController/Edit/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit(int id, IFormCollection collection)
    {
        try
        {
            return RedirectToAction(nameof(Index));
        }
        catch
        {
            return View();
        }
    }

    // GET: FlightController/Delete/5
    public ActionResult Delete(int id)
    {
        return View();
    }

    // POST: FlightController/Delete/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Delete(int id, IFormCollection collection)
    {
        try
        {
            return RedirectToAction(nameof(Index));
        }
        catch
        {
            return View();
        }
    }
}
