using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;
using AirlinesWeb.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace AirlinesWeb.Controllers;
#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0052 // Remove unread private members
#pragma warning disable IDE0022 // Use expression body for method
#pragma warning disable IDE0021 // Use expression body for constructor
public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly IHomeRepository _homeRepository;

    public HomeController(ILogger<HomeController> logger, IHomeRepository homeRepository)
    {
        _logger = logger;
        _homeRepository = homeRepository;
    }

    public IActionResult Index()
    {
        var airportCount = _homeRepository.GetCount<Airport>();
        var airlineCount = _homeRepository.GetCount<Airline>();
        var flightCount = _homeRepository.GetCount<Flight>();

        var model = new DashboardViewModel
        {
            AirportCount = airportCount,
            AirlineCount = airlineCount,
            FlightCount = flightCount
        };

        return View(model);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
