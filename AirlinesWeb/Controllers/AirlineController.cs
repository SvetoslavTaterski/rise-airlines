﻿using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace AirlinesWeb.Controllers;
#pragma warning disable IDE0022 // Use expression body for method
#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0060 // Remove unused parameter
public class AirlineController : Controller
{
    private readonly IAirlineRepository _airlineRepository;

    public AirlineController(IAirlineRepository repository)
    {
        _airlineRepository = repository;
    }

    // GET: AirlineController
    public ActionResult Index(string searchTerm, string airlineName, string fleetSize)
    {
        var airlines = _airlineRepository.GetAllAirlines();

        if (!string.IsNullOrEmpty(searchTerm))
        {
            airlines = airlines.Where(f => f.AirlineName.Contains(searchTerm)).ToList();
        }

        if (!string.IsNullOrEmpty(airlineName))
        {
            airlines = airlines.Where(f => f.AirlineName.Contains(airlineName)).ToList();
        }

        if (!string.IsNullOrEmpty(fleetSize))
        {
            airlines = airlines.Where(f => f.FleetSize.ToString().Contains(fleetSize)).ToList();
        }

        ViewBag.CurrentFilter = searchTerm;

        return View(airlines);
    }

    // GET: AirlineController/Details/5
    public ActionResult Details(int id)
    {
        return View();
    }

    // GET: AirlineController/Create
    public ActionResult Create()
    {
        return View();
    }

    // POST: AirlineController/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult CreateNewAirline(Airline airline)
    {
        if (airline.Founded < new DateOnly(2024, 1, 1))
        {
            ModelState.AddModelError("Founded", "The date should be earlier than now");
        }

        if (airline.AirlineName.Length > 6 && !string.IsNullOrEmpty(airline.AirlineName))
        {
            ModelState.AddModelError("Name", "Name must be no longer than 6 and must be filled");
        }

        if (airline.FleetSize > 0)
        {
            ModelState.AddModelError("FleetSize", "Fleet size must be greater than 0");
        }

        if (airline.AirlineDescription.Length > 50)
        {
            ModelState.AddModelError("AirlineDescription", "Description must be longer than 50 chars");
        }

        if (ModelState.IsValid)
        {
            _airlineRepository.CreateAirline(airline);

            return RedirectToAction(nameof(Index));
        }

        return RedirectToAction(nameof(Index));
    }

    // GET: AirlineController/Edit/5
    public ActionResult Edit(int id)
    {
        return View();
    }

    // POST: AirlineController/Edit/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit(int id, IFormCollection collection)
    {
        try
        {
            return RedirectToAction(nameof(Index));
        }
        catch
        {
            return View();
        }
    }

    // GET: AirlineController/Delete/5
    public ActionResult Delete(int id)
    {
        return View();
    }

    // POST: AirlineController/Delete/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Delete(int id, IFormCollection collection)
    {
        _airlineRepository.RemoveAirlineById(id);

        return RedirectToAction(nameof(Index));
    }
}
