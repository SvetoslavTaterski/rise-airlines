﻿function checkIsValid() {
    const inputElements = document.querySelectorAll('.submit-element input, .departure-and-arrival input');
    const createButton = document.querySelector('.submit-button');
    let isValid = true;

    inputElements.forEach(input => {
        const error = input.nextElementSibling;

        if (input.value.length === 0 || input.value === null) {
            error.style.display = "block";
            isValid = false;
        } else if (input.value.length !== 0 || input.value !== null) {
            error.style.display = "none";
        }

    });

    if (isValid) {
        createButton.removeAttribute('disabled');
        createButton.style.backgroundColor = "#FCA311";
        createButton.style.opacity = "1";
        createButton.style.color = "";
        createButton.style.cursor = "";
    } else {
        createButton.setAttribute('disabled', 'disabled');
        createButton.style.backgroundColor = "#cccccc";
        createButton.style.color = "#666666";
        createButton.style.cursor = "not-allowed";
        createButton.style.opacity = "0.5";
    }
}