﻿function showForm() {
    const submitForm = document.getElementById("submit-form");
    const submitLabel = document.getElementById("submit-label");
    const tableAndFilter = document.getElementById("table-and-filter");
    const showFormButton = document.getElementById("show-form");

    if (showFormButton.textContent.trim() == 'Show Form') {
        //Hiding table and filter form and button
        tableAndFilter.style.display = "none";
        showFormButton.textContent = "Show Table"
        showFormButton.style.color = "white"
        //Showing submit form
        submitForm.style.display = "flex";
        submitLabel.style.display = "block";
    }
    else if (showFormButton.textContent.trim() == 'Show Table') {
        tableAndFilter.style.display = "block";
        submitForm.style.display = "none";
        submitLabel.style.display = "none";
        showFormButton.textContent = "Show Form";
    }
}

document.addEventListener("DOMContentLoaded", function () {
    const loadingOverlay = document.getElementById('loading-overlay');
    const loadingCircle = document.getElementById('loading');

    // Hide the loading overlay and circle initially
    loadingOverlay.style.display = 'none';
    loadingCircle.style.display = 'none';

    // Add event listeners to all links to trigger the loading overlay and circle
    const links = document.querySelectorAll('a[href]');
    links.forEach(link => {
        link.addEventListener('click', function (event) {
            // Prevent default action and show the loading overlay and circle
            event.preventDefault();
            const url = this.href;
            loadingOverlay.style.display = 'flex';
            loadingCircle.style.display = 'block';

            // Delay navigation to allow the loading circle to be visible
            setTimeout(() => {
                window.location.href = url;
            }, 500); // Adjust the delay as needed
        });
    });
});

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('scrollButton').addEventListener('click', function () {
        document.querySelector('.table-form').scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    });
});

function showTable() {
    const divTableThreeElements = document.getElementById("ThreeItemsDiv");
    const realTable = document.getElementsByClassName("table-form")[0];
    const showMoreButton = document.getElementById("show-table");
    showMoreButton.style.color = "white";

    if (showMoreButton.textContent.trim() == "Show More") {
        showMoreButton.textContent = "Show Less";
        divTableThreeElements.style.display = "none";
        realTable.style.display = "block";
    }
    else if (showMoreButton.textContent.trim() == "Show Less") {
        showMoreButton.textContent = "Show More";
        divTableThreeElements.style.display = "block";
        realTable.style.display = "none";
    }

}


