﻿namespace AirlinesWeb.Models;

public class DashboardViewModel
{
    public int AirportCount { get; set; }
    public int AirlineCount { get; set; }
    public int FlightCount { get; set; }
}
