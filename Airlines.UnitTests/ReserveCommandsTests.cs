﻿using DataContracts;
using DataContracts.Aircrafts;
using BusinessLogic.CommandHandlers;
using DataContracts.Exceptions;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used
namespace Airlines.UnitTests
{
    public class ReserveCommandsTests
    {
        //[Fact]
        //public void GetFlightIfExists_Works_Properly()
        //{
        //    string id = "AAA";
        //    HashSet<Flight> flights = [];
        //    Aircraft aircraftOne = new Aircraft("Aircraft");
        //    Aircraft aircraftTwo = new Aircraft("Aircraft2");
        //    Flight flightOne = new Flight("AAA", "BBB", "CCC", aircraftOne);
        //    Flight flightTwo = new Flight("ZZZ", "BBB", "CCC", aircraftTwo);

        //    flights.Add(flightOne);
        //    flights.Add(flightTwo);

        //    Flight? isFlightFound = AddFlightCommand.IsFlightExisting(id, flights);

        //    Assert.NotNull(isFlightFound);
        //}

        //[Fact]
        //public void GetFlightIfExists_Works_Properly_When_Not_Found()
        //{
        //    string id = "OOO";
        //    HashSet<Flight> flights = [];
        //    Aircraft aircraftOne = new Aircraft("Aircraft");
        //    Aircraft aircraftTwo = new Aircraft("Aircraft2");
        //    Flight flightOne = new Flight("AAA", "BBB", "CCC", aircraftOne);
        //    Flight flightTwo = new Flight("ZZZ", "BBB", "CCC", aircraftTwo);

        //    flights.Add(flightOne);
        //    flights.Add(flightTwo);

        //    Flight? isFlightFound = AddFlightCommand.IsFlightExisting(id, flights);

        //    Assert.Null(isFlightFound);
        //}

        //[Fact]
        //public void CalculateTotalWeight_Works_Properly()
        //{
        //    int smallBaggageCount = 2;
        //    int largeBaggageCount = 2;
        //    double expectedWeight = (2 * 15) + (2 * 30);
        //    double actualWeight = ReserveTicketCommand.CalculateTotalWeight(smallBaggageCount, largeBaggageCount);

        //    Assert.Equal(expectedWeight, actualWeight);
        //}

        //[Fact]
        //public void CalculateTotalVolume_Works_Properly()
        //{
        //    int smallBaggageCount = 2;
        //    int largeBaggageCount = 2;
        //    double expectedWeight = (2 * 0.045) + (2 * 0.090);
        //    double actualWeight = ReserveTicketCommand.CalculateTotalVolume(smallBaggageCount, largeBaggageCount);

        //    Assert.Equal(expectedWeight, actualWeight);
        //}

        //[Fact]
        //public void ReserveCargo_Works_Properly_When_You_Can_Reserve()
        //{
        //    HashSet<Flight> flights = [];

        //    CargoAircraft aircraftOne = new CargoAircraft("Aircraft", 50, 50);
        //    Aircraft aircraftTwo = new Aircraft("Aircraft2");

        //    Flight flightOne = new Flight("AAA", "BBB", "CCC", aircraftOne);
        //    Flight flightTwo = new Flight("ZZZ", "BBB", "CCC", aircraftTwo);

        //    flights.Add(flightOne);
        //    flights.Add(flightTwo);

        //    string id = "AAA";
        //    int weight = 12;
        //    double volume = 12;

        //    string expectedValue = "You reserved cargo!";
        //    string actualValue = ReserveCargoCommand.HandleReserveCargoCommand(id, weight, volume, flights).ToString().Trim();

        //    Assert.Equal(expectedValue, actualValue);
        //}

        //[Fact]
        //public void ReserveCargo_Works_Properly_When_You_Cant_Reserve()
        //{
        //    HashSet<Flight> flights = [];

        //    CargoAircraft aircraftOne = new CargoAircraft("Aircraft", 50, 50);
        //    Aircraft aircraftTwo = new Aircraft("Aircraft2");

        //    Flight flightOne = new Flight("AAA", "BBB", "CCC", aircraftOne);
        //    Flight flightTwo = new Flight("ZZZ", "BBB", "CCC", aircraftTwo);

        //    flights.Add(flightOne);
        //    flights.Add(flightTwo);

        //    string id = "ZZZ";
        //    int weight = 12;
        //    double volume = 12;

        //    string expectedValue = "You cant reserve cargo on this type of aircraft";
        //    string actualValue = ReserveCargoCommand.HandleReserveCargoCommand(id, weight, volume, flights).ToString().Trim();

        //    Assert.Equal(expectedValue, actualValue);
        //}

        //[Fact]
        //public void ReserveCargo_Works_Properly_When_You_Cant_Reserve_When_Not_Enough_Space_Or_Volume()
        //{
        //    HashSet<Flight> flights = [];

        //    CargoAircraft aircraftOne = new CargoAircraft("Aircraft", 50, 50);
        //    Aircraft aircraftTwo = new Aircraft("Aircraft2");

        //    Flight flightOne = new Flight("AAA", "BBB", "CCC", aircraftOne);
        //    Flight flightTwo = new Flight("ZZZ", "BBB", "CCC", aircraftTwo);

        //    flights.Add(flightOne);
        //    flights.Add(flightTwo);

        //    string id = "AAA";
        //    int weight = 100;
        //    double volume = 120;

        //    string expectedValue = "You cant reserve that cargo!";
        //    string actualValue = ReserveCargoCommand.HandleReserveCargoCommand(id, weight, volume, flights).ToString().Trim();

        //    Assert.Equal(expectedValue, actualValue);
        //}

        //[Fact]
        //public void ReserveCargo_Works_Properly_When_There_Is_No_Such_Flight()
        //{
        //    HashSet<Flight> flights = [];

        //    CargoAircraft aircraftOne = new CargoAircraft("Aircraft", 50, 50);
        //    Aircraft aircraftTwo = new Aircraft("Aircraft2");

        //    Flight flightOne = new Flight("AAA", "BBB", "CCC", aircraftOne);
        //    Flight flightTwo = new Flight("ZZZ", "BBB", "CCC", aircraftTwo);

        //    flights.Add(flightOne);
        //    flights.Add(flightTwo);

        //    string id = "OOO";
        //    int weight = 100;
        //    double volume = 120;

        //    Assert.Throws<NoSuchItemException>(() =>
        //    {
        //        ReserveCargoCommand.HandleReserveCargoCommand(id, weight, volume, flights);
        //    });
        //}
    }
}
