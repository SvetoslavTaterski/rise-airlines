﻿using DataContracts;
using BusinessLogic.CommandHandlers;
using System.Text;
using DataContracts.Exceptions;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used
namespace Airlines.UnitTests
{
    public class RouteManagementTests
    {
        [Fact]
        public void IsFlightExisting_Works_Correctly()
        {
            var flights = new HashSet<Flight>();

            var flightOne = new Flight("AAA", "BBB", "CCC", 2, 2);
            var flightTwo = new Flight("VVV", "DDD", "ZZZ", 2, 2);

            flights.Add(flightOne);
            flights.Add(flightTwo);

            string id = "AAA";

            Flight? actualResult = AddFlightCommand.IsFlightExisting(id, flights);

            Assert.NotNull(actualResult);
        }

        [Fact]
        public void IsFlightExisting_Works_Correctly_When_There_Is_No_Such_Flight()
        {
            var flights = new HashSet<Flight>();

            var flightOne = new Flight("AAA", "BBB", "CCC", 2, 2);
            var flightTwo = new Flight("VVV", "DDD", "ZZZ", 2, 2);

            flights.Add(flightOne);
            flights.Add(flightTwo);

            string id = "OOO";

            Flight? actualResult = AddFlightCommand.IsFlightExisting(id, flights);

            Assert.Null(actualResult);
        }

        [Fact]
        public void RemoveFlightFromRoute_Works_Correctly()
        {
            LinkedList<Flight> route = new LinkedList<Flight>();
            var flights = new HashSet<Flight>();

            var flightOne = new Flight("AAA", "BBB", "CCC", 2, 2);
            var flightTwo = new Flight("VVV", "DDD", "ZZZ", 2, 2);

            flights.Add(flightOne);
            flights.Add(flightTwo);

            AddFlightCommand.HandleAddFlightCommand(flightOne.Id, route, flights);
            AddFlightCommand.HandleAddFlightCommand(flightTwo.Id, route, flights);

            RemoveCommand.HandleRemove(route);
            int expectedCount = 0;
            int actualCount = route.Count;

            Assert.Equal(expectedCount, actualCount);
        }

        [Fact]
        public void RemoveFlightFromRoute_Works_Correctly_When_Removing_From_Empty_Collection()
        {
            LinkedList<Flight> route = new LinkedList<Flight>();

            Assert.Throws<CollectionIsEmptyException>(() =>
            {
                RemoveCommand.HandleRemove(route);
            });
        }

        [Fact]
        public void AddFlightCommand_Works_Correctly_When_The_Flight_Is_Already_Added()
        {
            LinkedList<Flight> route = new LinkedList<Flight>();
            var flights = new HashSet<Flight>();

            var flightOne = new Flight("AAA", "BBB", "CCC", 2, 2);
            var flightTwo = new Flight("AAA", "BBB", "CCC", 2, 2);

            flights.Add(flightOne);
            flights.Add(flightTwo);

            AddFlightCommand.HandleAddFlightCommand(flightOne.Id, route, flights);

            string expectedResult = "Flight is already added to the route!";
            string actualResult = AddFlightCommand.HandleAddFlightCommand(flightTwo.Id, route, flights).ToString().Trim();

            Assert.Equal(actualResult, expectedResult);
        }

        [Fact]
        public void AddFlightCommand_Work_Correctly_When_The_Flight_Is_Not_Found()
        {
            LinkedList<Flight> route = new LinkedList<Flight>();
            var flights = new HashSet<Flight>();

            var flightOne = new Flight("AAA", "BBB", "CCC", 2, 2);
            var flightTwo = new Flight("AAA", "BBB", "CCC", 2, 2);

            flights.Add(flightOne);
            flights.Add(flightTwo);

            Assert.Throws<NoSuchItemException>(() =>
            {
                AddFlightCommand.HandleAddFlightCommand("ZZZ", route, flights);
            });
        }

        [Fact]
        public void NewCommand_Works_Correctly()
        {
            LinkedList<Flight> route = new LinkedList<Flight>();
            var flights = new HashSet<Flight>();

            var flightOne = new Flight("AAA", "BBB", "CCC", 2, 2);
            var flightTwo = new Flight("AAA", "BBB", "CCC", 2, 2);

            flights.Add(flightOne);
            flights.Add(flightTwo);

            AddFlightCommand.HandleAddFlightCommand(flightOne.Id, route, flights);

            string actualResult = NewCommand.HandleNewCommand(route).ToString().Trim();
            string expected = "Creating a new route.";

            Assert.Equal(actualResult, expected);
        }

        [Fact]
        public void PrintCommand_Works_Correctly()
        {
            LinkedList<Flight> route = new LinkedList<Flight>();
            var flights = new HashSet<Flight>();

            var flightOne = new Flight("AAA", "BBB", "CCC", 2, 2);
            var flightTwo = new Flight("AAA", "BBB", "CCC", 2, 2);

            flights.Add(flightOne);
            flights.Add(flightTwo);

            AddFlightCommand.HandleAddFlightCommand(flightOne.Id, route, flights);

            StringBuilder actualResult = PrintCommand.HandlePrint(route);
            StringBuilder expectedResult = new StringBuilder();
            expectedResult.AppendLine("Route:");
            expectedResult.AppendLine($"Flight AAA Departure airport BBB Arrival Airport CCC");

            Assert.Equal(actualResult.ToString(), expectedResult.ToString());
        }

        [Fact]
        public void PrintCommand_Works_Correctly_When_Route_Is_Empty()
        {
            LinkedList<Flight> route = new LinkedList<Flight>();

            Assert.Throws<CollectionIsEmptyException>(() =>
            {
                PrintCommand.HandlePrint(route);
            });
        }

        //[Fact]
        //public void ReserveTicketCommand_Works_Correctly_When_There_Is_No_Space()
        //{
        //    var flights = new HashSet<Flight>();

        //    var aircraftOne = new PassengerAircraft("Aircraft1", 50, 50, 50);
        //    var aircraftTwo = new Aircraft("Aircraft1");

        //    var flightOne = new Flight("AAA", "BBB", "CCC", aircraftOne);
        //    var flightTwo = new Flight("AAA", "BBB", "CCC", aircraftTwo);

        //    flights.Add(flightOne);
        //    flights.Add(flightTwo);

        //    string actualResult = ReserveTicketCommand.ReserveTicketCommandHandler("AAA", 2, 2, 2, flights).ToString().Trim();
        //    string expectedResult = "You cant reserve Tickets for this aircraft!";

        //    Assert.Equal(actualResult, expectedResult);
        //}

        //[Fact]
        //public void ReserveTicketCommand_Works_Correctly()
        //{
        //    var flights = new HashSet<Flight>();

        //    var aircraftOne = new PassengerAircraft("Aircraft1", 1000, 1000, 1000);
        //    var aircraftTwo = new Aircraft("Aircraft1");

        //    var flightOne = new Flight("AAA", "BBB", "CCC", aircraftOne);
        //    var flightTwo = new Flight("AAA", "BBB", "CCC", aircraftTwo);

        //    flights.Add(flightOne);
        //    flights.Add(flightTwo);

        //    string actualResult = ReserveTicketCommand.ReserveTicketCommandHandler("AAA", 2, 2, 2, flights).ToString().Trim();
        //    string expectedResult = "You reserved Tickets!";

        //    Assert.Equal(actualResult, expectedResult);
        //}

        //[Fact]
        //public void ReserveTicketCommand_Works_Correctly_When_Type_Is_Not_Correct()
        //{
        //    var flights = new HashSet<Flight>();

        //    var aircraftOne = new CargoAircraft("Aircraft1", 1000, 1000);
        //    var aircraftTwo = new Aircraft("Aircraft1");

        //    var flightOne = new Flight("AAA", "BBB", "CCC", aircraftOne);
        //    var flightTwo = new Flight("AAA", "BBB", "CCC", aircraftTwo);

        //    flights.Add(flightOne);
        //    flights.Add(flightTwo);

        //    string actualResult = ReserveTicketCommand.ReserveTicketCommandHandler("AAA", 2, 2, 2, flights).ToString().Trim();
        //    string expectedResult = "You cant reserve tickets on this type of aircraft";

        //    Assert.Equal(actualResult, expectedResult);
        //}

        //[Fact]
        //public void ReserveTicketCommand_Works_Correctly_With_Private_Aircraft()
        //{
        //    var flights = new HashSet<Flight>();

        //    var aircraftOne = new PrivateAircraft("Aircraft1", 1000);
        //    var aircraftTwo = new Aircraft("Aircraft1");

        //    var flightOne = new Flight("AAA", "BBB", "CCC", aircraftOne);
        //    var flightTwo = new Flight("AAA", "BBB", "CCC", aircraftTwo);

        //    flights.Add(flightOne);
        //    flights.Add(flightTwo);

        //    string actualResult = ReserveTicketCommand.ReserveTicketCommandHandler("AAA", 2, 2, 2, flights).ToString().Trim();
        //    string expectedResult = "You reserved Tickets!";

        //    Assert.Equal(actualResult, expectedResult);
        //}

        //[Fact]
        //public void ReserveTicketCommand_Works_Correctly_With_Private_Aircraft_When_There_Is_No_Space()
        //{
        //    var flights = new HashSet<Flight>();

        //    var aircraftOne = new PrivateAircraft("Aircraft1", 1);
        //    var aircraftTwo = new Aircraft("Aircraft1");

        //    var flightOne = new Flight("AAA", "BBB", "CCC", aircraftOne);
        //    var flightTwo = new Flight("AAA", "BBB", "CCC", aircraftTwo);

        //    flights.Add(flightOne);
        //    flights.Add(flightTwo);

        //    string actualResult = ReserveTicketCommand.ReserveTicketCommandHandler("AAA", 2, 2, 2, flights).ToString().Trim();
        //    string expectedResult = "You cant reserve tickets for this aircraft!";

        //    Assert.Equal(actualResult, expectedResult);
        //}

        //[Fact]
        //public void ReserveTicketCommand_Works_Correctly_When_There_Is_No_Such_Flight()
        //{
        //    var flights = new HashSet<Flight>();

        //    var aircraftOne = new PrivateAircraft("Aircraft1", 1);
        //    var aircraftTwo = new Aircraft("Aircraft1");

        //    var flightOne = new Flight("AAA", "BBB", "CCC", aircraftOne);
        //    var flightTwo = new Flight("AAA", "BBB", "CCC", aircraftTwo);

        //    flights.Add(flightOne);
        //    flights.Add(flightTwo);

        //    Assert.Throws<NoSuchItemException>(() =>
        //    {
        //        ReserveTicketCommand.ReserveTicketCommandHandler("ZZZ", 2, 2, 2, flights);
        //    });
        //}
    }
}
