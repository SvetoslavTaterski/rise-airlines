﻿using DataContracts;
using System.Text;
using BusinessLogic.CommandHandlers;
using BusinessLogic;
using DataContracts.Exceptions;
using System.Data;
#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used

namespace Airlines.UnitTests
{
    public class ProgramCommandsTests
    {
        [Fact]
        public void IsAirlineExisting_Returns_True_When_Airline_Exists()
        {
            var airlines = new HashSet<Airline>();

            var airlineOne = new Airline("Airline1");
            var airlineTwo = new Airline("Airline2");

            airlines.Add(airlineOne);
            airlines.Add(airlineTwo);

            string[] command = ["exists", "Airline1"];

            string actual = ExistCommand.HandleExist(command, airlines).ToString().Trim();
            string expected = "True";

            Assert.Equal(expected, actual);
        }


        [Fact]
        public void ListAllAirports_Returns_All_Airports_In_Given_City()
        {

            var airports = new HashSet<Airport>();

            var airportOne = new Airport("ABC", "Airport", "Septemvri", "Bulgaria");
            var airportTwo = new Airport("CBA", "Airport", "Septemvri", "Bulgaria");

            airports.Add(airportOne);
            airports.Add(airportTwo);


            var commandLine = new string[] { "list", "Septemvri", "City" };

            // Act
            StringBuilder result = ListCommand.HandleList(commandLine, airports);

            // Assert
            Assert.NotNull(result);
            Assert.Contains("Airport", result.ToString());
            Assert.DoesNotContain("Airport2", result.ToString());
        }

        [Fact]
        public void ListAllAirports_Returns_All_Airports_In_Given_Country()
        {

            var airports = new HashSet<Airport>();

            var airportOne = new Airport("ABC", "Airport", "Septemvri", "Bulgaria");
            var airportTwo = new Airport("CBA", "Airport", "Septemvri", "Bulgaria");

            airports.Add(airportOne);
            airports.Add(airportTwo);


            var commandLine = new string[] { "list", "Bulgaria", "Country" };

            // Act
            StringBuilder result = ListCommand.HandleList(commandLine, airports);

            // Assert
            Assert.NotNull(result);
            Assert.Contains("Airport", result.ToString());
            Assert.DoesNotContain("Airport2", result.ToString());
        }

        [Fact]
        public void ListAllAirports_Returns_All_Airports_In_Given_Wrong()
        {

            var airports = new HashSet<Airport>();

            var airportOne = new Airport("ABC", "Airport", "Septemvri", "Bulgaria");
            var airportTwo = new Airport("CBA", "Airport", "Septemvri", "Bulgaria");

            airports.Add(airportOne);
            airports.Add(airportTwo);


            var commandLine = new string[] { "list", "Bulgaria", "SS" };

            // Act
            StringBuilder result = ListCommand.HandleList(commandLine, airports);

            // Assert
            Assert.NotNull(result);
        }

        [Fact]
        public void Search_Item_Returns_Correct_String_Airport()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var commandLine = new string[] { "search", "airport", "Apple" };

            var airportOne = new Airport("AAA", "Apple", "Septemvri", "Bulgaria");
            var airportTwo = new Airport("BBB", "Banana", "Septemvri", "Bulgaria");

            airports.Add(airportOne);
            airports.Add(airportTwo);

            string actualResult = SearchCommand.HandleSearch(commandLine, airports, airlines, flights).ToString().Trim();
            string expected = "There is an airport with the given name!";

            Assert.Equal(expected, actualResult);
        }

        [Fact]
        public void Search_Item_Returns_Correct_String_Airport_When_There_Is_No_Such_Type()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var commandLine = new string[] { "search", "airport", "Mango" };

            var airportOne = new Airport("AAA", "Apple", "Septemvri", "Bulgaria");
            var airportTwo = new Airport("BBB", "Banana", "Septemvri", "Bulgaria");

            airports.Add(airportOne);
            airports.Add(airportTwo);

            string actualResult = SearchCommand.HandleSearch(commandLine, airports, airlines, flights).ToString().Trim();
            string expected = "There is no such airport!";

            Assert.Equal(expected, actualResult);
        }

        [Fact]
        public void Search_Item_Returns_Correct_String_Airline()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var commandLine = new string[] { "search", "airline", "Apple" };

            var airlineOne = new Airline("Apple");
            var airlineTwo = new Airline("Banana");

            airlines.Add(airlineOne);
            airlines.Add(airlineTwo);

            string actualResult = SearchCommand.HandleSearch(commandLine, airports, airlines, flights).ToString().Trim();
            string expected = $"There is an airline with the given name!";

            Assert.Equal(expected, actualResult);
        }

        [Fact]
        public void Search_Item_Returns_Correct_String_Airline_When_There_Is_No_Such_Type()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var commandLine = new string[] { "search", "airline", "Mango" };

            var airlineOne = new Airline("Apple");
            var airlineTwo = new Airline("Banana");

            airlines.Add(airlineOne);
            airlines.Add(airlineTwo);

            string actualResult = SearchCommand.HandleSearch(commandLine, airports, airlines, flights).ToString().Trim();
            string expected = "There is no such airline!";

            Assert.Equal(expected, actualResult);
        }

        [Fact]
        public void Search_Item_Returns_Correct_String_Flight()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var commandLine = new string[] { "search", "flight", "Apple" };


            var flightOne = new Flight("Apple", "AAA", "BBB", 2, 2);
            var flightTwo = new Flight("Banana", "CCC", "VVV", 2, 2);

            flights.Add(flightOne);
            flights.Add(flightTwo);

            string actualResult = SearchCommand.HandleSearch(commandLine, airports, airlines, flights).ToString().Trim();
            string expected = $"There is an flight with the given id!";

            Assert.Equal(expected, actualResult);
        }

        [Fact]
        public void Search_Item_Returns_Correct_String_Flight_When_There_Is_No_Such_Type()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var commandLine = new string[] { "search", "flight", "Mango" };


            var flightOne = new Flight("Apple", "AAA", "BBB", 2, 2);
            var flightTwo = new Flight("Banana", "CCC", "VVV", 2, 2);

            flights.Add(flightOne);
            flights.Add(flightTwo);

            string actualResult = SearchCommand.HandleSearch(commandLine, airports, airlines, flights).ToString().Trim();
            string expected = $"There is no such flight!";

            Assert.Equal(expected, actualResult);
        }

        [Fact]
        public void Search_Items_Returns_Correct_String_When_There_Is_No_Such_Type()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var commandLine = new string[] { "search", "monkey", "Mango" };


            var flightOne = new Flight("Apple", "AAA", "BBB", 2, 2);
            var flightTwo = new Flight("Banana", "CCC", "VVV", 2, 2);

            flights.Add(flightOne);
            flights.Add(flightTwo);

            Assert.Throws<NoSuchItemException>(() =>
            {
                SearchCommand.HandleSearch(commandLine, airports, airlines, flights);
            });
        }

        [Fact]
        public void SortCommand_Works_Correctly_Airports()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var airportOne = new Airport("AAA", "BBB", "CCC", "DDD");
            var airportTwo = new Airport("ZZZ", "XXX", "FFF", "PPP");

            airports.Add(airportOne);
            airports.Add(airportTwo);

            string[] commandLine = ["sort", "airports", "ascending"];

            string actualResult = SortCommand.HandleSort(commandLine, airports, airlines, flights).ToString().Trim();
            string expectedResult = BuildMessages.BuildListMessage(airports.OrderBy(a => a.Name), airports => airports.Name).ToString().Trim();

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void SortCommand_Works_Correctly_When_Descending_Airports()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var airportOne = new Airport("AAA", "BBB", "CCC", "DDD");
            var airportTwo = new Airport("ZZZ", "XXX", "FFF", "PPP");

            airports.Add(airportOne);
            airports.Add(airportTwo);

            string[] commandLine = ["sort", "airports", "descending"];

            string actualResult = SortCommand.HandleSort(commandLine, airports, airlines, flights).ToString().Trim();
            string expectedResult = BuildMessages.BuildListMessage(airports.OrderByDescending(a => a.Name), airports => airports.Name).ToString().Trim();

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void SortCommand_Works_Correctly_Airlines()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var airline = new Airline("Airline1");
            var airlineTwo = new Airline("Airline2");

            airlines.Add(airline);
            airlines.Add(airlineTwo);

            string[] commandLine = ["sort", "airlines", "ascending"];

            string actualResult = SortCommand.HandleSort(commandLine, airports, airlines, flights).ToString().Trim();
            string expectedResult = BuildMessages.BuildListMessage(airlines.OrderBy(a => a.Name), airlines => airlines.Name).ToString().Trim();

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void SortCommand_Works_Correctly_Airlines_When_Descending()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var airline = new Airline("Airline1");
            var airlineTwo = new Airline("Airline2");

            airlines.Add(airline);
            airlines.Add(airlineTwo);

            string[] commandLine = ["sort", "airlines", "descending"];

            string actualResult = SortCommand.HandleSort(commandLine, airports, airlines, flights).ToString().Trim();
            string expectedResult = BuildMessages.BuildListMessage(airlines.OrderByDescending(a => a.Name), airlines => airlines.Name).ToString().Trim();

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void SortCommand_Works_Correctly_Flights()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var flightOne = new Flight("Apple", "AAA", "BBB", 2, 2);
            var flightTwo = new Flight("Banana", "CCC", "VVV", 2, 2);

            flights.Add(flightOne);
            flights.Add(flightTwo);

            string[] commandLine = ["sort", "flights", "ascending"];

            string actualResult = SortCommand.HandleSort(commandLine, airports, airlines, flights).ToString().Trim();
            string expectedResult = BuildMessages.BuildListMessage(flights.OrderBy(a => a.Id), flight => flight.Id).ToString().Trim();

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void SortCommand_Works_Correctly_Flights_Descending()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var flightOne = new Flight("Apple", "AAA", "BBB", 2, 2);
            var flightTwo = new Flight("Banana", "CCC", "VVV", 2, 2);

            flights.Add(flightOne);
            flights.Add(flightTwo);

            string[] commandLine = ["sort", "flights", "descending"];

            string actualResult = SortCommand.HandleSort(commandLine, airports, airlines, flights).ToString().Trim();
            string expectedResult = BuildMessages.BuildListMessage(flights.OrderByDescending(a => a.Id), flight => flight.Id).ToString().Trim();

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void SortCommand_Works_Correctly_Flights_When_No_Type_Of_Sorting_Is_Given()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();


            var flightOne = new Flight("Apple", "AAA", "BBB", 2, 2);
            var flightTwo = new Flight("Banana", "CCC", "VVV", 2, 2);

            flights.Add(flightOne);
            flights.Add(flightTwo);

            string[] commandLine = ["sort", "flights"];

            string actualResult = SortCommand.HandleSort(commandLine, airports, airlines, flights).ToString().Trim();
            string expectedResult = BuildMessages.BuildListMessage(flights.OrderBy(a => a.Id), flight => flight.Id).ToString().Trim();

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void SortCommand_Works_Correctly_Airlines_When_No_Type_Of_Sorting_Is_Given()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var airline = new Airline("Airline1");
            var airlineTwo = new Airline("Airline2");

            airlines.Add(airline);
            airlines.Add(airlineTwo);

            string[] commandLine = ["sort", "airlines"];

            string actualResult = SortCommand.HandleSort(commandLine, airports, airlines, flights).ToString().Trim();
            string expectedResult = BuildMessages.BuildListMessage(airlines.OrderBy(a => a.Name), airlines => airlines.Name).ToString().Trim();

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void SortCommand_Works_Correctly_Airports_When_No_Type_Of_Sorting_Is_Given()
        {
            var airports = new HashSet<Airport>();
            var airlines = new HashSet<Airline>();
            var flights = new HashSet<Flight>();

            var airportOne = new Airport("AAA", "BBB", "CCC", "DDD");
            var airportTwo = new Airport("ZZZ", "XXX", "FFF", "PPP");

            airports.Add(airportOne);
            airports.Add(airportTwo);

            string[] commandLine = ["sort", "airports"];

            string actualResult = SortCommand.HandleSort(commandLine, airports, airlines, flights).ToString().Trim();
            string expectedResult = BuildMessages.BuildListMessage(airports.OrderBy(a => a.Name), airports => airports.Name).ToString().Trim();

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void HandleCheck_AirportsConnected_ReturnsFalse()
        {
            // Arrange
            Graph graph = new Graph();
            graph.AddFlight("JFK", "LAX", 300, 5); // Add a flight from JFK to LAX

            // Act
            StringBuilder result = CheckCommand.HandleCheck(graph, "JFK", "LAX");

            // Assert
            Assert.Equal("False", result.ToString().Trim());
        }

        [Fact]
        public void HandleFind_Route_DoesNot_Exists_ReturnsCorrectResult()
        {
            // Arrange
            Graph graph = new Graph();
            graph.AddFlight("JFK", "LAX", 300, 5); // Add a flight from JFK to LAX

            // Act
            StringBuilder result = FindCommand.HandleFind(graph, "JFK", "OOO");

            // Assert
            string expected = "No route found from OOO to JFK.";
            Assert.Equal(expected, result.ToString().Trim());
        }

        [Fact]
        public void HandleFind_RouteExists_ReturnsCorrectResult()
        {
            // Arrange
            Graph graph = new Graph();
            graph.AddFlight("JFK", "LAX", 300, 5); // Add a flight from JFK to LAX

            // Act
            StringBuilder result = FindCommand.HandleFind(graph, "LAX", "JFK");

            // Assert
            string expected = "Route from JFK to LAX: JFK -> LAX";
            Assert.Equal(expected, result.ToString().Trim());
        }

        [Fact]
        public void SearchRouteCommandHandler_RouteExists_ReturnsCorrectResult()
        {
            // Arrange
            Graph graph = new Graph();
            graph.AddFlight("JFK", "LAX", 300, 5); // Add a flight from JFK to LAX

            // Act
            StringBuilder result = SearchRouteCommand.SearchRouteCommandHandler("JFK", "LAX", graph);

            // Assert
            string expected = "JFK->LAX"; // Assuming the route exists
            Assert.Equal(expected, result.ToString().Trim());
        }

        [Fact]
        public void SearchRouteCommandHandler_RouteExists_ReturnsCorrectResult_Time()
        {
            // Arrange
            Graph graph = new Graph();
            graph.AddFlight("JFK", "LAX", 300, 5); // Add a flight from JFK to LAX

            // Act
            StringBuilder result = SearchRouteCommand.SearchRouteCommandHandlerTime("JFK", "LAX", graph);

            // Assert
            string expected = "JFK->LAX"; // Assuming the route exists
            Assert.Equal(expected, result.ToString().Trim());
        }

        [Fact]
        public void SearchRouteCommandHandler_RouteExists_ReturnsCorrectResult_Stops()
        {
            // Arrange
            Graph graph = new Graph();
            graph.AddFlight("JFK", "LAX", 300, 5); // Add a flight from JFK to LAX

            // Act
            StringBuilder result = SearchRouteCommand.SearchRouteCommandHandlerStops("JFK", "LAX", graph);

            // Assert
            string expected = "JFK->LAX"; // Assuming the route exists
            Assert.Equal(expected, result.ToString().Trim());
        }

        [Fact]
        public void CreateAddFlightCommand_ReturnsInstanceOfAddFlightCommand()
        {
            // Arrange
            string id = "FL123";
            LinkedList<Flight> route = new LinkedList<Flight>();
            HashSet<Flight> flights = [];

            // Act
            var command = BusinessLogic.Commands.AddFlightCommand.CreateAddFlightCommand(id, route, flights);

            // Assert
            Assert.NotNull(command);
        }
    }
}
