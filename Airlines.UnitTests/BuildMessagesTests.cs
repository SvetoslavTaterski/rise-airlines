﻿using DataContracts;
using BusinessLogic;

namespace Airlines.UnitTests
{
    public class BuildMessagesTests
    {
        [Fact]
        public void BuildListMessage_ReturnsCorrectStringBuilder()
        {
            // Arrange
            var items = new List<Airport>();

            var airportOne = new Airport("ABC", "Airport", "Septemvri", "Bulgaria");
            var airportTwo = new Airport("CBA", "Airport", "Septemvri", "Bulgaria");
            var airportThree = new Airport("BCA", "Airport", "Septemvri", "Bulgaria");

            items.Add(airportOne);
            items.Add(airportTwo);
            items.Add(airportThree);

            var orderedItems = items.OrderBy(a => a.Name);
            // Act
            var result = BuildMessages.BuildListMessage(orderedItems, airport => airport.Name);

            // Assert
            var expectedResult = "Airports:" + Environment.NewLine +
                                 "Airport Airport Airport ";
            Assert.Equal(expectedResult, result.ToString());
        }
    }
}
