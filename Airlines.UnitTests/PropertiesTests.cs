﻿using DataContracts;
using DataContracts.Aircrafts;
using DataContracts.Exceptions;
#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable CA1861 // Avoid constant arrays as arguments

namespace Airlines.UnitTests
{
    public class PropertiesTests
    {
        [Fact]
        public void Airport_Id_Is_Set_Correctly()
        {
            // Arrange
            string id = "ABC";
            string name = "Airport Name";
            string city = "City";
            string country = "Country";

            // Act
            var airport = new Airport(id, name, city, country);

            // Assert
            Assert.Equal(id, airport.Id);
        }

        [Fact]
        public void Airport_City_Is_Set_Correctly()
        {
            // Arrange
            string id = "ABC";
            string name = "Airport Name";
            string city = "City";
            string country = "Country";

            // Act
            var airport = new Airport(id, name, city, country);

            // Assert
            Assert.Equal(city, airport.City);
        }

        [Fact]
        public void Airport_Country_Is_Set_Correctly()
        {
            // Arrange
            string id = "ABC";
            string name = "Airport Name";
            string city = "City";
            string country = "Country";

            // Act
            var airport = new Airport(id, name, city, country);

            // Assert
            Assert.Equal(country, airport.Country);
        }

        [Fact]
        public void Airport_Id_Writes_Error_Message_For_Invalid_Id()
        {
            // Arrange
            string invalidId = "A"; // Invalid because it doesn't meet the length criteria
            string validId = "AAA";

            string validName = "AirportName";
            string validCity = "City";
            string validCountry = "Country";

            var airport = new Airport(validId, validName, validCity, validCountry);

            var exception = Assert.Throws<PropertyIsNotValidException>(() => airport.Id = invalidId);
            Assert.Equal("Id is not valid!", exception.Message);
        }

        [Fact]
        public void Airport_Name_Writes_Error_Message_For_Invalid_Name()
        {
            string validId = "AAA";
            string validName = "AirportName";
            string invalidName = "@@@AA123";
            string validCity = "City";
            string validCountry = "Country";

            var airport = new Airport(validId, validName, validCity, validCountry);

            var exception = Assert.Throws<PropertyIsNotValidException>(() => airport.Name = invalidName);

            Assert.Equal("Name is not valid!", exception.Message);
        }

        [Fact]
        public void Airport_City_Writes_Error_Message_For_Invalid_City()
        {
            string validId = "AAA";
            string validName = "AirportName";
            string validCity = "City";
            string invalidCity = "@@@123123";
            string validCountry = "Country";

            var airport = new Airport(validId, validName, validCity, validCountry);

            var exception = Assert.Throws<PropertyIsNotValidException>(() => airport.City = invalidCity);

            Assert.Equal("City is not valid!", exception.Message);
        }

        [Fact]
        public void Airport_Country_Writes_Error_Message_For_Invalid_Country()
        {
            string validId = "AAA";
            string validName = "AirportName";
            string validCity = "City";
            string validCountry = "Country";
            string invalidCountry = "@@@aa123";

            var airport = new Airport(validId, validName, validCity, validCountry);

            var exception = Assert.Throws<PropertyIsNotValidException>(() => airport.Country = invalidCountry);

            Assert.Equal("Country is not valid!", exception.Message);
        }

        [Fact]
        public void Flight_Id_Is_Set_Correctly()
        {
            // Arrange
            string expectedId = "TestFlightName";
            string departureAirport = "AFK";
            string arrivalAirport = "JFK";


            // Act
            var flight = new Flight(expectedId, departureAirport, arrivalAirport, 2, 2);

            // Assert
            Assert.Equal(expectedId, flight.Id);
        }

        [Fact]
        public void Flight_Id_Can_Be_Updated()
        {
            // Arrange
            string expectedId = "UpdatedFlightName";
            string departureAirport = "AFK";
            string arrivalAirport = "JFK";


            // Act
            var flight = new Flight(expectedId, departureAirport, arrivalAirport, 2, 2);

            // Assert
            Assert.Equal(expectedId, flight.Id);
        }

        [Fact]
        public void Flight_ArrivalAirport_Returns_Value_Correctly()
        {
            string departureAirport = "AFK";
            string arrivalAirport = "JFK";

            var flight = new Flight("InitialFlightName", departureAirport, arrivalAirport, 2, 2);

            // Assert
            Assert.Equal(flight.ArrivalAirport, arrivalAirport);
        }

        [Fact]
        public void Flight_DepartureAirport_Returns_Value_Correctly()
        {
            string departureAirport = "AFK";
            string arrivalAirport = "JFK";


            var flight = new Flight("InitialFlightName", departureAirport, arrivalAirport, 2, 2);

            // Assert
            Assert.Equal(flight.DepartureAirport, departureAirport);
        }

        [Fact]
        public void Aircraft_Property_Works_Correctly()
        {
            Aircraft aircraft = new Aircraft("Aircraft");
            string expectedName = "Aircraft";
            string actualName = aircraft.Name;

            Assert.Equal(expectedName, actualName);
        }

        [Fact]
        public void CargoAircraft_Property_Works_Correctly()
        {
            CargoAircraft aircraft = new CargoAircraft("CargoAircraft", 12, 12);
            string expectedName = "CargoAircraft";
            int expectedLoad = 12;
            double expectedVolume = 12;

            Assert.Equal(expectedName, aircraft.Name);
            Assert.Equal(expectedLoad, aircraft.Load);
            Assert.Equal(expectedVolume, aircraft.Volume);
        }

        [Fact]
        public void PassengerAircraft_Property_Works_Correctly()
        {
            PassengerAircraft aircraft = new PassengerAircraft("CargoAircraft", 12, 12, 4);
            string expectedName = "CargoAircraft";
            int expectedLoad = 12;
            double expectedVolume = 12;
            int expectedSeats = 4;

            Assert.Equal(expectedName, aircraft.Name);
            Assert.Equal(expectedLoad, aircraft.Load);
            Assert.Equal(expectedVolume, aircraft.Volume);
            Assert.Equal(expectedSeats, aircraft.Seats);
        }

        [Fact]
        public void PrivateAircraft_Property_Works_Correctly()
        {
            PrivateAircraft aircraft = new PrivateAircraft("CargoAircraft", 4);
            string expectedName = "CargoAircraft";
            int expectedSeats = 4;

            Assert.Equal(expectedName, aircraft.Name);
            Assert.Equal(expectedSeats, aircraft.Seats);
        }

        [Fact]
        public void Aircraft_CreateCargoAircraft_Method_Works_Correctly()
        {
            Aircraft aircraft = Aircraft.CreateCargoAircraft("Aircraft", 12, 12);

            Assert.NotNull(aircraft);
        }

        [Fact]
        public void Aircraft_CreatePassengerAircraft_Method_Works_Correctly()
        {
            Aircraft aircraft = Aircraft.CreatePassangerAircraft("Aircraft", 12, 12, 2);

            Assert.NotNull(aircraft);
        }

        [Fact]
        public void Aircraft_CreatePrivateAircraft_Method_Works_Correctly()
        {
            Aircraft aircraft = Aircraft.CreatePrivateAircraft("Aircraft", 2);

            Assert.NotNull(aircraft);
        }

        [Fact]
        public void FindShortestRoute_WhenValidRouteExists_ShouldReturnRoute()
        {
            // Arrange
            var graph = new Graph();
            graph.AddFlight("DFW", "JFK", 2, 2);
            graph.AddFlight("DFW", "LAX", 2, 2);
            graph.AddFlight("LAX", "SFO", 2, 2);

            // Act
            var route = graph.FindPath("DFW", "SFO");

            // Assert
            Assert.NotNull(route);
            Assert.Equal(new[] { "DFW", "LAX", "SFO" }, route);
        }

        [Fact]
        public void FindShortestRoute_WhenStartAirportNotFound_ShouldReturnNull()
        {
            // Arrange
            var graph = new Graph();

            // Act
            var route = graph.FindPath("DFW", "SFO");

            // Assert
            Assert.Null(route);
        }

        [Fact]
        public void FindShortestRoute_WhenDestinationAirportNotFound_ShouldReturnNull()
        {
            // Arrange
            var graph = new Graph();
            graph.AddFlight("DFW", "JFK", 2, 2);

            // Act
            var route = graph.FindPath("DFW", "SFO");

            // Assert
            Assert.Null(route);
        }

        [Fact]
        public void FindFlightRoute_WhenValidRouteExists_ShouldReturnRoute()
        {
            // Arrange
            var graph = new Graph();
            graph.AddFlight("DFW", "JFK", 2, 2);
            graph.AddFlight("DFW", "LAX", 2, 2);
            graph.AddFlight("LAX", "SFO", 2, 2);

            // Act
            var route = graph.FindPath("DFW", "SFO");

            // Assert
            Assert.NotNull(route);
        }

        [Fact]
        public void FindFlightRoute_WhenStartAirportNotFound_ShouldReturnNull()
        {
            // Arrange
            var graph = new Graph();

            // Act
            var route = graph.FindPath("DFW", "SFO");

            // Assert
            Assert.Null(route);
        }

        [Fact]
        public void FindFlightRoute_WhenDestinationAirportNotFound_ShouldReturnNull()
        {
            // Arrange
            var graph = new Graph();
            graph.AddFlight("DFW", "JFK", 2, 2);

            // Act
            var route = graph.FindPath("DFW", "SFO");

            // Assert
            Assert.Null(route);
        }

        [Fact]
        public void AreAirportsConnected_WhenValidConnectionExists_ShouldReturnTrue()
        {
            // Arrange
            var graph = new Graph();
            graph.AddFlight("DFW", "JFK", 2, 2);
            graph.AddFlight("JFK", "LAX", 2, 2);

            // Act
            var isConnected = graph.AreAirportsConnected("DFW", "LAX");

            // Assert
            Assert.True(isConnected);
        }

        [Fact]
        public void AreAirportsConnected_WhenStartAirportNotFound_ShouldReturnFalse()
        {
            // Arrange
            var graph = new Graph();

            // Act
            var isConnected = graph.AreAirportsConnected("DFW", "LAX");

            // Assert
            Assert.False(isConnected);
        }

        [Fact]
        public void AreAirportsConnected_WhenDestinationAirportNotFound_ShouldReturnFalse()
        {
            // Arrange
            var graph = new Graph();
            graph.AddFlight("DFW", "JFK", 2, 2);

            // Act
            var isConnected = graph.AreAirportsConnected("DFW", "LAX");

            // Assert
            Assert.False(isConnected);
        }

        [Fact]
        public void FindRouteByCost_ReturnsCorrectRoute()
        {
            // Arrange
            Graph graph = new Graph();
            graph.AddFlight("JFK", "LAX", 300, 5); // Add a flight from JFK to LAX with price 300 and time 5 hours

            // Act
            List<string>? route = graph.FindRouteByCost("JFK", "LAX");

            // Assert
            Assert.NotNull(route);
            Assert.Equal(2, route.Count); // Direct flight from JFK to LAX
            Assert.Equal("JFK", route[0]);
            Assert.Equal("LAX", route[1]);
        }

        [Fact]
        public void FindRouteByTime_ReturnsCorrectRoute()
        {
            // Arrange
            Graph graph = new Graph();
            graph.AddFlight("JFK", "LAX", 300, 5); // Add a flight from JFK to LAX with price 300 and time 5 hours

            // Act
            List<string>? route = graph.FindRouteByTime("JFK", "LAX");

            // Assert
            Assert.NotNull(route);
            Assert.Equal(2, route.Count); // Direct flight from JFK to LAX
            Assert.Equal("JFK", route[0]);
            Assert.Equal("LAX", route[1]);
        }

        [Fact]
        public void FindRouteByLeastStops_ReturnsCorrectRoute()
        {
            // Arrange
            Graph graph = new Graph();
            graph.AddFlight("JFK", "LAX", 300, 5); // Add a flight from JFK to LAX with price 300 and time 5 hours

            // Act
            List<string>? route = graph.FindRouteByLeastStops("JFK", "LAX");

            // Assert
            Assert.NotNull(route);
            Assert.Equal(2, route.Count); // Direct flight from JFK to LAX
            Assert.Equal("JFK", route[0]);
            Assert.Equal("LAX", route[1]);
        }
    }
}
