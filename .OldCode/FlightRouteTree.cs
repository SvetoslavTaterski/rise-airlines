﻿#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable CA1854 // Prefer the 'IDictionary.TryGetValue(TKey, out TValue)' method
#pragma warning disable IDE0028 // Simplify collection initialization

public class AirportTreeNode
{
    public string AirportCode { get; }
    public List<AirportTreeNode> Destinations { get; }

    public AirportTreeNode(string airportCode)
    {
        AirportCode = airportCode;
        Destinations = new List<AirportTreeNode>();
    }
}

public class FlightRouteTree
{
    public readonly Dictionary<string, AirportTreeNode> airports;

    public FlightRouteTree() => airports = new Dictionary<string, AirportTreeNode>();

    public List<string>? FindFlightRoute(string startAirport, string destinationAirport)
    {
        if (!airports.ContainsKey(startAirport) || !airports.ContainsKey(destinationAirport))
        {
            return null; // One of the airports doesn't exist in the routes
        }

        var route = new List<string>();
        if (FindFlightRouteDFS(airports[startAirport], destinationAirport, route))
        {

            return route;
        }

        return null; // Route not found
    }

    private static bool FindFlightRouteDFS(AirportTreeNode currentNode, string destinationAirport, List<string> route)
    {
        route.Add(currentNode.AirportCode);

        if (currentNode.AirportCode == destinationAirport)
        {
            return true;
        }

        foreach (var destination in currentNode.Destinations)
        {
            if (FindFlightRouteDFS(destination, destinationAirport, route))
            {
                return true;
            }
        }

        return false;
    }
}
