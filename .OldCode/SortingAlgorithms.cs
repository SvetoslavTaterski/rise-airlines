﻿#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0180 // Use tuple to swap values

using DataContracts;

namespace BusinessLogic
{
    public static class SortingAlgorithms
    {

        public static void BubbleSort(string[] arr)
        {
            int n = arr.Length;
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - i - 1; j++)
                {
                    if (string.Compare(arr[j], arr[j + 1]) > 0)
                    {
                        string temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
        }

        public static void BubbleSort(Airport[] airports, string order)
        {
            int n = airports.Length;

            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - i - 1; j++)
                {
                    int comparisonResult;

                    if (order == "ascending")
                    {
                        comparisonResult = string.Compare(airports[j].Name, airports[j + 1].Name);
                    }
                    else if (order == "descending")
                    {
                        comparisonResult = string.Compare(airports[j + 1].Name, airports[j].Name);
                    }
                    else
                    {
                        Console.WriteLine("Error! Invalid sorting order specified.");
                        return;
                    }

                    if (comparisonResult > 0)
                    {
                        string temp = airports[j].Name;
                        airports[j].Name = airports[j + 1].Name;
                        airports[j + 1].Name = temp;
                    }
                }
            }
        }

        public static void BubbleSort(Airline[] airlines, string order)
        {
            int n = airlines.Length;

            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - i - 1; j++)
                {
                    int comparisonResult;

                    if (order == "ascending")
                    {
                        comparisonResult = string.Compare(airlines[j].Name, airlines[j + 1].Name);
                    }
                    else if (order == "descending")
                    {
                        comparisonResult = string.Compare(airlines[j + 1].Name, airlines[j].Name);
                    }
                    else
                    {
                        Console.WriteLine("Error! Invalid sorting order specified.");
                        return;
                    }

                    if (comparisonResult > 0)
                    {
                        string temp = airlines[j].Name;
                        airlines[j].Name = airlines[j + 1].Name;
                        airlines[j + 1].Name = temp;
                    }
                }
            }
        }

        public static void BubbleSort(Flight[] flights, string order)
        {
            int n = flights.Length;

            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - i - 1; j++)
                {
                    int comparisonResult;

                    if (order == "ascending")
                    {
                        comparisonResult = string.Compare(flights[j].Name, flights[j + 1].Name);
                    }
                    else if (order == "descending")
                    {
                        comparisonResult = string.Compare(flights[j + 1].Name, flights[j].Name);
                    }
                    else
                    {
                        Console.WriteLine("Error! Invalid sorting order specified.");
                        return;
                    }

                    if (comparisonResult > 0)
                    {
                        string temp = flights[j].Name;
                        flights[j].Name = flights[j + 1].Name;
                        flights[j + 1].Name = temp;
                    }
                }
            }
        }

        public static void BubbleSort(Airport[] airports)
        {
            int n = airports.Length;
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - i - 1; j++)
                {
                    if (string.Compare(airports[j].Name, airports[j + 1].Name) > 0)
                    {
                        string temp = airports[j].Name;
                        airports[j].Name = airports[j + 1].Name;
                        airports[j + 1].Name = temp;
                    }
                }
            }
        }

        public static void BubbleSort(Airline[] airlines)
        {
            int n = airlines.Length;
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - i - 1; j++)
                {
                    if (string.Compare(airlines[j].Name, airlines[j + 1].Name) > 0)
                    {
                        string temp = airlines[j].Name;
                        airlines[j].Name = airlines[j + 1].Name;
                        airlines[j + 1].Name = temp;
                    }
                }
            }
        }

        public static void BubbleSort(Flight[] flights)
        {
            int n = flights.Length;
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - i - 1; j++)
                {
                    if (string.Compare(flights[j].Name, flights[j + 1].Name) > 0)
                    {
                        string temp = flights[j].Name;
                        flights[j].Name = flights[j + 1].Name;
                        flights[j + 1].Name = temp;
                    }
                }
            }
        }

        public static void SelectionSort(string[] arr)
        {
            int n = arr.Length;
            for (int i = 0; i < n - 1; i++)
            {
                int minIndex = i;
                for (int j = i + 1; j < n; j++)
                {
                    if (string.Compare(arr[j], arr[minIndex]) < 0)
                    {
                        minIndex = j;
                    }
                }

                string temp = arr[i];
                arr[i] = arr[minIndex];
                arr[minIndex] = temp;
            }
        }
    }
}

