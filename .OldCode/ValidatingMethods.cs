﻿using DataContracts;
#pragma warning disable IDE0007 // Use implicit type

namespace BusinessLogic
{
    public static class ValidatingMethods
    {
        public static bool IsAlphanumeric(string input)
        {
            foreach (char c in input)
            {
                if (!char.IsLetterOrDigit(c))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsAlphabetic(string input)
        {
            foreach (char c in input)
            {
                if (!char.IsLetter(c))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsAirportInputCorrect(string input)
        {
            if (input.Length == 3 && !string.IsNullOrEmpty(input) && IsAlphabetic(input))
            {
                return true;
            }

            return false;
        }

        public static bool IsAirlineInputCorrect(string input)
        {
            if (input.Length < 6 && !string.IsNullOrEmpty(input))
            {
                return true;
            }

            return false;
        }

        public static bool IsFlightInputCorrect(string input)
        {
            if (IsAlphanumeric(input) && !string.IsNullOrEmpty(input))
            {
                return true;
            }

            return false;
        }

        public static string CheckSortOrder<T>(IEnumerable<T> items, Func<T, string> nameSelector)
        {
            bool ascending = true;
            bool descending = true;

            T[] array = items.ToArray();

            for (int i = 0; i < array.Length - 1; i++)
            {
                if (string.Compare(nameSelector(array[i]), nameSelector(array[i + 1])) > 0)
                {
                    ascending = false;
                    break;
                }
            }

            for (int i = 0; i < array.Length - 1; i++)
            {
                if (string.Compare(nameSelector(array[i]), nameSelector(array[i + 1])) < 0)
                {
                    descending = false;
                    break;
                }
            }

            if (ascending)
            {
                return "ascending";
            }
            else if (descending)
            {
                return "descending";
            }
            else
            {
                return "unsorted";
            }
        }

        public static string CheckSortOrder(Airline[] array)
        {
            bool ascending = true;
            bool descending = true;

            for (int i = 0; i < array.Length - 1; i++)
            {
                if (string.Compare(array[i].Name, array[i + 1].Name) > 0)
                {
                    ascending = false;
                    break;
                }
            }

            for (int i = 0; i < array.Length - 1; i++)
            {
                if (string.Compare(array[i].Name, array[i + 1].Name) < 0)
                {
                    descending = false;
                    break;
                }
            }

            if (ascending)
            {
                return "ascending";
            }
            else if (descending)
            {
                return "descending";
            }
            else
            {
                return "unsorted";
            }
        }

        public static string CheckSortOrder(Flight[] array)
        {
            bool ascending = true;
            bool descending = true;

            for (int i = 0; i < array.Length - 1; i++)
            {
                if (string.Compare(array[i].Name, array[i + 1].Name) > 0)
                {
                    ascending = false;
                    break;
                }
            }

            for (int i = 0; i < array.Length - 1; i++)
            {
                if (string.Compare(array[i].Name, array[i + 1].Name) < 0)
                {
                    descending = false;
                    break;
                }
            }

            if (ascending)
            {
                return "ascending";
            }
            else if (descending)
            {
                return "descending";
            }
            else
            {
                return "unsorted";
            }
        }
    }
}
