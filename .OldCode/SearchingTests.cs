﻿using static BusinessLogic.SearchingAlgorithms;
using DataContracts;

#pragma warning disable IDE0007 // Use implicit type
namespace Airlines.UnitTests
{
    public class SearchingTests
    {
        //[Fact]
        //public void Is_Binary_Search_Working_Correctly_When_Middle_Element_Is_Found_Airport_Ascending()
        //{
        //    Airport[] testArray = [new("APP"), new("BAN"), new("MAN")];
        //    string targetWord = "BAN";

        //    int expectedResult = 1;
        //    int actualResult = BinarySearch(testArray, targetWord);

        //    Assert.Equal(expectedResult, actualResult);
        //}

        //[Fact]
        //public void Is_Binary_Search_Working_Correctly_When_Middle_Element_Is_Found_Airport_Descending()
        //{
        //    Airport[] testArray = [new("MAN"), new("BAN"), new("APP")];
        //    string targetWord = "BAN";

        //    int expectedResult = 1;
        //    int actualResult = BinarySearch(testArray, targetWord);

        //    Assert.Equal(expectedResult, actualResult);
        //}

        //[Fact]
        //public void Is_Binary_Search_Working_Correctly_When_First_Element_Is_Found_Airport_Ascending()
        //{
        //    Airport[] testArray = [new("APP"), new("BAN"), new("MAN")];
        //    string targetWord = "APP";

        //    int expectedResult = 0;
        //    int actualResult = BinarySearch(testArray, targetWord);

        //    Assert.Equal(expectedResult, actualResult);
        //}

        //[Fact]
        //public void Is_Binary_Search_Working_Correctly_When_First_Element_Is_Found_Airport_Descending()
        //{
        //    Airport[] testArray = [new("MAN"), new("BAN"), new("APP")];
        //    string targetWord = "MAN";

        //    int expectedResult = 0;
        //    int actualResult = BinarySearch(testArray, targetWord);

        //    Assert.Equal(expectedResult, actualResult);
        //}

        //[Fact]
        //public void Is_Binary_Search_Working_Correctly_When_Last_Element_Is_Found_Airport_Ascending()
        //{
        //    Airport[] testArray = [new("APP"), new("BAN"), new("MAN")];
        //    string targetWord = "MAN";

        //    int expectedResult = 2;
        //    int actualResult = BinarySearch(testArray, targetWord);

        //    Assert.Equal(expectedResult, actualResult);
        //}

        //[Fact]
        //public void Is_Binary_Search_Working_Correctly_When_Last_Element_Is_Found_Airport_Descending()
        //{
        //    Airport[] testArray = [new("MAN"), new("BAN"), new("APP")];
        //    string targetWord = "APP";

        //    int expectedResult = 2;
        //    int actualResult = BinarySearch(testArray, targetWord);

        //    Assert.Equal(expectedResult, actualResult);
        //}

        //[Fact]
        //public void Is_Binary_Search_Working_Correctly_When_Element_Is_Not_Found_Airport_Ascending()
        //{
        //    Airport[] testArray = [new("APP"), new("BAN"), new("MAN")];
        //    string targetWord = "ERR";

        //    int expectedResult = -1;
        //    int actualResult = BinarySearch(testArray, targetWord);

        //    Assert.Equal(expectedResult, actualResult);
        //}

        //[Fact]
        //public void Is_Binary_Search_Working_Correctly_When_Element_Is_Not_Found_Airport_Descending()
        //{
        //    Airport[] testArray = [new("MAN"), new("BAN"), new("APP")];
        //    string targetWord = "ERR";

        //    int expectedResult = -1;
        //    int actualResult = BinarySearch(testArray, targetWord);

        //    Assert.Equal(expectedResult, actualResult);
        //}

        //[Fact]
        //public void Is_Binary_Search_Working_Correctly_When_Element_Is_Not_Found_Airport_Unordered()
        //{
        //    Airport[] testArray = [new("BAN"), new("MAN"), new("APP")];
        //    string targetWord = "ERR";

        //    int expectedResult = -2;
        //    int actualResult = BinarySearch(testArray, targetWord);

        //    Assert.Equal(expectedResult, actualResult);
        //}

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_Middle_Element_Is_Found_Airline_Ascending()
        {
            Airline[] testArray = [new("APP"), new("BAN"), new("MAN")];
            string targetWord = "BAN";

            int expectedResult = 1;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_Middle_Element_Is_Found_Airline_Descending()
        {
            Airline[] testArray = [new("MAN"), new("BAN"), new("APP")];
            string targetWord = "BAN";

            int expectedResult = 1;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_First_Element_Is_Found_Airline_Ascending()
        {
            Airline[] testArray = [new("APP"), new("BAN"), new("MAN")];
            string targetWord = "APP";

            int expectedResult = 0;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_First_Element_Is_Found_Airline_Descending()
        {
            Airline[] testArray = [new("MAN"), new("BAN"), new("APP")];
            string targetWord = "MAN";

            int expectedResult = 0;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_Last_Element_Is_Found_Airline_Ascending()
        {
            Airline[] testArray = [new("APP"), new("BAN"), new("MAN")];
            string targetWord = "MAN";

            int expectedResult = 2;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_Last_Element_Is_Found_Airline_Descending()
        {
            Airline[] testArray = [new("MAN"), new("BAN"), new("APP")];
            string targetWord = "APP";

            int expectedResult = 2;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_Element_Is_Not_Found_Airline_Ascending()
        {
            Airline[] testArray = [new("APP"), new("BAN"), new("MAN")];
            string targetWord = "ERR";

            int expectedResult = -1;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_Element_Is_Not_Found_Airline_Descending()
        {
            Airline[] testArray = [new("MAN"), new("BAN"), new("APP")];
            string targetWord = "ERR";

            int expectedResult = -1;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_Element_Is_Not_Found_Airline_Unordered()
        {
            Airline[] testArray = [new("BAN"), new("MAN"), new("APP")];
            string targetWord = "ERR";

            int expectedResult = -2;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_Middle_Element_Is_Found_Flight_Ascending()
        {
            Flight[] testArray = [new("APP"), new("BAN"), new("MAN")];
            string targetWord = "BAN";

            int expectedResult = 1;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_Middle_Element_Is_Found_Flight_Descending()
        {
            Flight[] testArray = [new("MAN"), new("BAN"), new("APP")];
            string targetWord = "BAN";

            int expectedResult = 1;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_First_Element_Is_Found_Flight_Ascending()
        {
            Flight[] testArray = [new("APP"), new("BAN"), new("MAN")];
            string targetWord = "APP";

            int expectedResult = 0;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_First_Element_Is_Found_Flight_Descending()
        {
            Flight[] testArray = [new("MAN"), new("BAN"), new("APP")];
            string targetWord = "MAN";

            int expectedResult = 0;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_Last_Element_Is_Found_Flight_Ascending()
        {
            Flight[] testArray = [new("APP"), new("BAN"), new("MAN")];
            string targetWord = "MAN";

            int expectedResult = 2;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_Last_Element_Is_Found_Flight_Descending()
        {
            Flight[] testArray = [new("MAN"), new("BAN"), new("APP")];
            string targetWord = "APP";

            int expectedResult = 2;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_Element_Is_Not_Found_Flight_Ascending()
        {
            Flight[] testArray = [new("APP"), new("BAN"), new("MAN")];
            string targetWord = "ERR";

            int expectedResult = -1;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_Element_Is_Not_Found_Flight_Descending()
        {
            Flight[] testArray = [new("MAN"), new("BAN"), new("APP")];
            string targetWord = "ERR";

            int expectedResult = -1;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Binary_Search_Working_Correctly_When_Element_Is_Not_Found_Flight_Unordered()
        {
            Flight[] testArray = [new("BAN"), new("MAN"), new("APP")];
            string targetWord = "ERR";

            int expectedResult = -2;
            int actualResult = BinarySearch(testArray, targetWord);

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_Existing_Method_Working_Correctly_With_Wrong_Data()
        {
            string[] testArray = ["apple", "banana", "mango"];
            string targetWord = "watermelon";

            bool result = IsExisting(targetWord, testArray);

            Assert.False(result);
        }

        [Fact]
        public void Is_Existing_Method_Working_Correctly()
        {
            string[] testArray = ["apple", "banana", "mango"];
            string targetWord = "apple";

            bool result = IsExisting(targetWord, testArray);

            Assert.True(result);
        }

        //[Fact]
        //public void Is_Existing_Method_Working_Correctly_With_Wrong_Data_Airports()
        //{
        //    Airport[] testArray = [new("BAN"), new("MAN"), new("APP")];
        //    string targetWord = "ERR";

        //    bool result = IsExisting(targetWord, testArray);

        //    Assert.False(result);
        //}

        //[Fact]
        //public void Is_Existing_Method_Working_Correctly_Airports()
        //{
        //    Airport[] testArray = [new("BAN"), new("MAN"), new("APP")];
        //    string targetWord = "APP";

        //    bool result = IsExisting(targetWord, testArray);

        //    Assert.True(result);
        //}

        [Fact]
        public void Is_Existing_Method_Working_Correctly_With_Wrong_Data_Flight()
        {
            Flight[] testArray = [new("BAN"), new("MAN"), new("APP")];
            string targetWord = "ERR";

            bool result = IsExisting(targetWord, testArray);

            Assert.False(result);
        }

        [Fact]
        public void Is_Existing_Method_Working_Correctly_Flight()
        {
            Flight[] testArray = [new("BAN"), new("MAN"), new("APP")];
            string targetWord = "APP";

            bool result = IsExisting(targetWord, testArray);

            Assert.True(result);
        }

        [Fact]
        public void Is_Existing_Method_Working_Correctly_With_Wrong_Data_Airline()
        {
            Airline[] testArray = [new("BAN"), new("MAN"), new("APP")];
            string targetWord = "ERR";

            bool result = IsExisting(targetWord, testArray);

            Assert.False(result);
        }

        [Fact]
        public void Is_Existing_Method_Working_Correctly_Airline()
        {
            Airline[] testArray = [new("BAN"), new("MAN"), new("APP")];
            string targetWord = "APP";

            bool result = IsExisting(targetWord, testArray);

            Assert.True(result);
        }
    }
}
