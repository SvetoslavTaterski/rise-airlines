﻿#pragma warning disable IDE0007 // Use implicit type

using DataContracts;
using static BusinessLogic.ValidatingMethods;

namespace BusinessLogic
{
    public static class SearchingAlgorithms
    {
        public static int BinarySearch(string[] arr, string target)
        {
            int left = 0;
            int right = arr.Length - 1;

            while (left <= right)
            {
                int mid = left + ((right - left) / 2);

                int compareResult = string.Compare(arr[mid], target);

                if (compareResult == 0)
                {
                    return mid; // Element found, return its index
                }
                else if (compareResult < 0)
                {
                    left = mid + 1; // Search in the right half
                }
                else
                {
                    right = mid - 1; // Search in the left half
                }
            }

            return -1; // Element not found
        }

        public static bool IsExisting(string item, string[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == item)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsExisting(string airlineName, Airline[] airlines)
        {
            for (int i = 0; i < airlines.Length; i++)
            {
                if (airlines[i].Name == airlineName)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsExisting(string airportName, Airport[] airports)
        {
            for (int i = 0; i < airports.Length; i++)
            {
                if (airports[i].Name == airportName)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsExisting(string flightName, Flight[] flights)
        {
            for (int i = 0; i < flights.Length; i++)
            {
                if (flights[i].Name == flightName)
                {
                    return true;
                }
            }

            return false;
        }

        public static int BinarySearch<T>(IEnumerable<T> items, string itemName, Func<T, string> nameSelector)
        {
            List<T> itemList = items.ToList();

            int left = 0;
            int right = itemList.Count - 1;

            string order = CheckSortOrder(itemList, nameSelector);

            if (order == "ascending")
            {
                while (left <= right)
                {
                    int mid = left + ((right - left) / 2);

                    int compareResult = string.Compare(nameSelector(itemList[mid]), itemName);

                    if (compareResult == 0)
                    {
                        return mid; // Element found, return its index
                    }
                    else if (compareResult < 0)
                    {
                        left = mid + 1; // Search in the right half
                    }
                    else
                    {
                        right = mid - 1; // Search in the left half
                    }
                }

                return -1; // Element not found
            }
            else if (order == "descending")
            {
                while (left <= right)
                {
                    int mid = left + ((right - left) / 2);

                    int compareResult = string.Compare(itemName, nameSelector(itemList[mid])); // Reversed comparison

                    if (compareResult == 0)
                    {
                        return mid; // Element found, return its index
                    }
                    else if (compareResult < 0)
                    {
                        left = mid + 1; // Search in the right half (reverse)
                    }
                    else
                    {
                        right = mid - 1; // Search in the left half (reverse)
                    }
                }

                return -1; // Element not found
            }
            else if (order == "unsorted")
            {
                return -2;
            }

            return -1;
        }

        public static int BinarySearch(Airport[] airports, string airportName)
        {
            int left = 0;
            int right = airports.Length - 1;

            string order = CheckSortOrder(airports);

            if (order == "ascending")
            {
                while (left <= right)
                {
                    int mid = left + ((right - left) / 2);

                    int compareResult = string.Compare(airports[mid].Name, airportName);

                    if (compareResult == 0)
                    {
                        return mid; // Element found, return its index
                    }
                    else if (compareResult < 0)
                    {
                        left = mid + 1; // Search in the right half
                    }
                    else
                    {
                        right = mid - 1; // Search in the left half
                    }
                }

                return -1; // Element not found
            }
            else if (order == "descending")
            {
                while (left <= right)
                {
                    int mid = left + ((right - left) / 2);

                    int compareResult = string.Compare(airportName, airports[mid].Name); // Reversed comparison

                    if (compareResult == 0)
                    {
                        return mid; // Element found, return its index
                    }
                    else if (compareResult < 0)
                    {
                        left = mid + 1; // Search in the right half (reverse)
                    }
                    else
                    {
                        right = mid - 1; // Search in the left half (reverse)
                    }
                }

                return -1; // Element not found
            }
            else if (order == "unsorted")
            {
                return -2;
            }

            return -1;

        }

        public static int BinarySearch(Flight[] flights, string flightName)
        {
            int left = 0;
            int right = flights.Length - 1;

            string order = CheckSortOrder(flights);

            if (order == "ascending")
            {
                while (left <= right)
                {
                    int mid = left + ((right - left) / 2);

                    int compareResult = string.Compare(flights[mid].Name, flightName);

                    if (compareResult == 0)
                    {
                        return mid; // Element found, return its index
                    }
                    else if (compareResult < 0)
                    {
                        left = mid + 1; // Search in the right half
                    }
                    else
                    {
                        right = mid - 1; // Search in the left half
                    }
                }

                return -1; // Element not found
            }
            else if (order == "descending")
            {
                while (left <= right)
                {
                    int mid = left + ((right - left) / 2);

                    int compareResult = string.Compare(flightName, flights[mid].Name); // Reversed comparison

                    if (compareResult == 0)
                    {
                        return mid; // Element found, return its index
                    }
                    else if (compareResult < 0)
                    {
                        left = mid + 1; // Search in the right half (reverse)
                    }
                    else
                    {
                        right = mid - 1; // Search in the left half (reverse)
                    }
                }

                return -1; // Element not found
            }
            else if (order == "unsorted")
            {
                return -2;
            }

            return -1;
        }
    }
}
