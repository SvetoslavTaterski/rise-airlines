﻿using DataContracts;
using System.Text;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used
namespace BusinessLogic
{
    public static class RouteManagement
    {
        public static StringBuilder AddNewFlightToRoute(string id, LinkedList<Flight> route, HashSet<Flight> flights)
        {
            StringBuilder sb = new StringBuilder();

            Flight? newFlightForRoute = IsFlightExisting(id, flights);

            if (newFlightForRoute != null)
            {
                string departureAirport = newFlightForRoute.DepartureAirport;

                if (!route.Contains(newFlightForRoute))
                {
                    if (route.Count == 0 || route.Last!.Value.ArrivalAirport == departureAirport)
                    {
                        route.AddLast(newFlightForRoute);
                        sb.AppendLine("Flight has been added to the route!");
                    }
                }
                else
                {
                    sb.AppendLine("Flight is already added to the route!");
                }
            }
            else
            {
                sb.AppendLine("There is no such flight!");
            }

            return sb;
        }

        public static StringBuilder RemoveFlightFromRoute(LinkedList<Flight> route)
        {
            StringBuilder sb = new StringBuilder();

            if (route.Count > 0)
            {
                route.RemoveLast();
                sb.AppendLine("Last flight from route removed.");
            }
            else
            {
                sb.AppendLine("The route is empty! There is nothing to remove...");
            }

            return sb;
        }

        public static StringBuilder PrintFlightsFromRoute(LinkedList<Flight> route)
        {
            StringBuilder sb = new StringBuilder();

            if (route.Count > 0)
            {
                sb.AppendLine("Route:");
                foreach (var flight in route)
                {
                    sb.AppendLine($"Flight {flight.Id} Departure airport {flight.DepartureAirport} Arrival Airport {flight.ArrivalAirport}");
                }
            }
            else
            {
                sb.AppendLine("The route is empty!");
            }

            return sb;
        }

        public static Flight? IsFlightExisting(string id, HashSet<Flight> flights)
        {
            foreach (var flight in flights)
            {
                if (flight.Id == id)
                {
                    return flight;
                }
            }
            return null;
        }
    }
}
