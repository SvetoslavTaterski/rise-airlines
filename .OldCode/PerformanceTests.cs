﻿using System.Diagnostics;
using static BusinessLogic.SortingAlgorithms;
using static BusinessLogic.SearchingAlgorithms;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used

namespace Airlines.UnitTests
{
    public class PerformanceTests
    {
        [Fact]
        public void Pseudo_Bubble_Sort_Performance_Test()
        {
            string[] testArray = ["banana", "mango", "apple"];

            Stopwatch stopwatch = Stopwatch.StartNew();
            BubbleSort(testArray);
            stopwatch.Stop();

            TimeSpan elapsed = stopwatch.Elapsed;
            Console.WriteLine($"BubbleSort took {elapsed.TotalMilliseconds} milliseconds");
        }

        [Fact]
        public void Pseudo_Selection_Sort_Performance_Test()
        {
            string[] testArray = ["banana", "mango", "apple"];

            Stopwatch stopwatch = Stopwatch.StartNew();
            SelectionSort(testArray);
            stopwatch.Stop();

            TimeSpan elapsed = stopwatch.Elapsed;
            Console.WriteLine($"SelectionSort took {elapsed.TotalMilliseconds} milliseconds");
        }

        [Fact]
        public void Pseudo_Linear_Search_Performance_Test()
        {
            string[] arr = ["apple", "banana", "cherry", "mango", "orange", "pear"];
            string target = "cherry";

            Stopwatch stopwatch = Stopwatch.StartNew();
            IsExisting(target, arr);
            stopwatch.Stop();

            double milliseconds = stopwatch.Elapsed.TotalMilliseconds;
            Console.WriteLine($"LinearSearch took {milliseconds} milliseconds");
        }

        [Fact]
        public void Pseude_Binary_Search_Performance_Test()
        {
            string[] arr = ["apple", "banana", "cherry", "mango", "orange", "pear"];
            string target = "cherry";

            Stopwatch stopwatch = Stopwatch.StartNew();
            BinarySearch(arr, target);
            stopwatch.Stop();

            double milliseconds = stopwatch.Elapsed.TotalMilliseconds;
            Console.WriteLine($"BinarySearch took {milliseconds} milliseconds");
        }
    }
}
