﻿using DataContracts;
using static Airlines.Program;
using static BusinessLogic.SortingAlgorithms;

#pragma warning disable IDE0007 // Use implicit type
namespace Airlines.UnitTests
{
    public class SortingTests
    {

        [Fact]
        public void Is_Bubble_Sort_Working_Correctly_With_Wrong_Data()
        {
            string[] testArray = ["mango", "banana", "apple"];
            string[] expectedNotSortedArray = ["banana", "mango", "apple"];

            BubbleSort(testArray);

            Assert.NotEqual(testArray, expectedNotSortedArray);
        }

        [Fact]
        public void Is_Bubble_Sort_Working_With_Empty_Array()
        {
            string[] testArray = [];
            string[] expectedEmptyArray = [];

            BubbleSort(testArray);

            Assert.Equal(testArray, expectedEmptyArray);
        }

        [Fact]
        public void Is_Bubble_Sort_Working_With_One_Element_Array()
        {
            string[] testArray = ["Apple"];
            string[] expectedArray = ["Apple"];

            BubbleSort(testArray);

            Assert.Equal(testArray, expectedArray);
        }

        [Fact]
        public void Is_Bubble_Sort_Working_With_Array_With_Duplicates()
        {
            string[] testArray = ["Apple", "Banana", "Mango", "Apple"];
            string[] expectedArray = ["Apple", "Apple", "Banana", "Mango"];

            BubbleSort(testArray);

            Assert.Equal(testArray, expectedArray);
        }

        [Fact]
        public void Is_Selection_Sort_Working_Correctly()
        {
            string[] testArray = ["mango", "banana", "apple"];
            string[] expectedArray = ["apple", "banana", "mango"];

            SelectionSort(testArray);

            Assert.Equal(testArray, expectedArray);
        }

        [Fact]
        public void Is_Selection_Sort_Working_Correctly_With_Wrong_Data()
        {
            string[] testArray = ["mango", "banana", "apple"];
            string[] expectedNotSortedArray = ["banana", "mango", "apple"];

            SelectionSort(testArray);

            Assert.NotEqual(testArray, expectedNotSortedArray);
        }

        [Fact]
        public void Is_Selection_Sort_Working_Correctly_With_Empty_Array()
        {
            string[] testArray = [];
            string[] expectedEmptyArray = [];

            SelectionSort(testArray);

            Assert.Equal(testArray, expectedEmptyArray);
        }

        [Fact]
        public void Is_Selection_Sort_Working_Correctly_With_One_Element_Array()
        {
            string[] testArray = ["Apple"];
            string[] expectedArray = ["Apple"];

            SelectionSort(testArray);

            Assert.Equal(testArray, expectedArray);
        }

        [Fact]
        public void Is_Selection_Sort_Working_Correctly_With_Array_With_Duplicates()
        {
            string[] testArray = ["Apple", "Banana", "Mango", "Apple"];
            string[] expectedArray = ["Apple", "Apple", "Banana", "Mango"];

            SelectionSort(testArray);

            Assert.Equal(testArray, expectedArray);
        }

        //[Fact]
        //public void BubbleSort_Ascending_Sorts_Correctly_Airports()
        //{
        //    Airport[] testArray = [new("MAN"), new("BAN"), new("APP")];

        //    BubbleSort(testArray, "ascending");

        //    Assert.Equal("APP", testArray[0].Name);
        //    Assert.Equal("BAN", testArray[1].Name);
        //    Assert.Equal("MAN", testArray[2].Name);
        //}

        //[Fact]
        //public void BubbleSort_Descending_Sorts_Correctly_Airports()
        //{
        //    Airport[] testArray = [new("BAN"), new("MAN"), new("APP")];

        //    BubbleSort(testArray, "descending");

        //    Assert.Equal("MAN", testArray[0].Name);
        //    Assert.Equal("BAN", testArray[1].Name);
        //    Assert.Equal("APP", testArray[2].Name);
        //}

        //[Fact]
        //public void BubbleSort_Ascending_Sorts_Correctly_Airports_Without_Order()
        //{
        //    Airport[] testArray = [new("BAN"), new("MAN"), new("APP")];

        //    BubbleSort(testArray);

        //    Assert.Equal("APP", testArray[0].Name);
        //    Assert.Equal("BAN", testArray[1].Name);
        //    Assert.Equal("MAN", testArray[2].Name);
        //}

        [Fact]
        public void BubbleSort_Ascending_Sorts_Correctly_Flights()
        {
            Flight[] testArray = [new("MAN"), new("BAN"), new("APP")];

            BubbleSort(testArray, "ascending");

            Assert.Equal("APP", testArray[0].Name);
            Assert.Equal("BAN", testArray[1].Name);
            Assert.Equal("MAN", testArray[2].Name);
        }

        [Fact]
        public void BubbleSort_Descending_Sorts_Correctly_Flights()
        {
            Flight[] testArray = [new("BAN"), new("MAN"), new("APP")];

            BubbleSort(testArray, "descending");

            Assert.Equal("MAN", testArray[0].Name);
            Assert.Equal("BAN", testArray[1].Name);
            Assert.Equal("APP", testArray[2].Name);
        }

        [Fact]
        public void BubbleSort_Ascending_Sorts_Correctly_Flights_Without_Order()
        {
            Flight[] testArray = [new("BAN"), new("MAN"), new("APP")];

            BubbleSort(testArray);

            Assert.Equal("APP", testArray[0].Name);
            Assert.Equal("BAN", testArray[1].Name);
            Assert.Equal("MAN", testArray[2].Name);
        }

        [Fact]
        public void BubbleSort_Ascending_Sorts_Correctly_Airlines()
        {
            Airline[] testArray = [new("MAN"), new("BAN"), new("APP")];

            BubbleSort(testArray, "ascending");

            Assert.Equal("APP", testArray[0].Name);
            Assert.Equal("BAN", testArray[1].Name);
            Assert.Equal("MAN", testArray[2].Name);
        }

        [Fact]
        public void BubbleSort_Descending_Sorts_Correctly_Airlines()
        {
            Airline[] testArray = [new("BAN"), new("MAN"), new("APP")];

            BubbleSort(testArray, "descending");

            Assert.Equal("MAN", testArray[0].Name);
            Assert.Equal("BAN", testArray[1].Name);
            Assert.Equal("APP", testArray[2].Name);
        }

        [Fact]
        public void BubbleSort_Ascending_Sorts_Correctly_Airlines_Without_Order()
        {
            Airline[] testArray = [new("BAN"), new("MAN"), new("APP")];

            BubbleSort(testArray);

            Assert.Equal("APP", testArray[0].Name);
            Assert.Equal("BAN", testArray[1].Name);
            Assert.Equal("MAN", testArray[2].Name);
        }
    }
}