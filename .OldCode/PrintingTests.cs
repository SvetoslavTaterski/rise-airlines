﻿using DataContracts;
using System.Text;
using static Airlines.BuildMessages;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used
namespace Airlines.UnitTests
{
    public class PrintingTests
    {
        //[Fact]
        //public void Print_Item_If_Found_Is_Working_Correctly_When_Found_With_AirLines()
        //{
        //    Airport[] testArrayAirport = [new("APP"), new("BAN"), new("MAN")];
        //    Airline[] testArrayAirlines = [new("APP"), new("BAN"), new("MAN")];
        //    Flight[] testArrayFlights = [new("APP"), new("BAN"), new("MAN")];

        //    string[] testInput = ["airline", "APP"];

        //    StringBuilder testStringBuilder = new StringBuilder();

        //    testStringBuilder.AppendLine($"Thеre is an airline with this name on index 0");

        //    StringBuilder expectedStringBuilder = BuildOutputMessage(testArrayAirport, testArrayAirlines, testArrayFlights, testInput);

        //    Assert.Equal(expectedStringBuilder.ToString(), testStringBuilder.ToString());
        //}

        //[Fact]
        //public void Print_Item_If_Found_Is_Working_Correctly_When_Not_Found_With_AirLines()
        //{
        //    Airport[] testArrayAirport = [new("APP"), new("BAN"), new("MAN")];
        //    Airline[] testArrayAirlines = [new("APP"), new("BAN"), new("MAN")];
        //    Flight[] testArrayFlights = [new("APP"), new("BAN"), new("MAN")];

        //    string[] testInput = ["airline", "ERR"];

        //    StringBuilder testStringBuilder = new StringBuilder();

        //    testStringBuilder.AppendLine("There is no airline with that name!");

        //    StringBuilder expectedStringBuilder = BuildOutputMessage(testArrayAirport, testArrayAirlines, testArrayFlights, testInput);

        //    Assert.Equal(expectedStringBuilder.ToString(), testStringBuilder.ToString());
        //}

        //[Fact]
        //public void Print_Item_If_Found_Is_Working_Correctly_When_Array_Is_Not_Sorted_AirLines()
        //{
        //    Airport[] testArrayAirport = [new("APP"), new("BAN"), new("MAN")];
        //    Airline[] testArrayAirlines = [new("BAN"), new("APP"), new("MAN")];
        //    Flight[] testArrayFlights = [new("APP"), new("BAN"), new("MAN")];

        //    string[] testInput = ["airline", "APP"];

        //    StringBuilder testStringBuilder = new StringBuilder();

        //    testStringBuilder.AppendLine("The array is not sorted! Please sort it first!");

        //    StringBuilder expectedStringBuilder = BuildOutputMessage(testArrayAirport, testArrayAirlines, testArrayFlights, testInput);

        //    Assert.Equal(expectedStringBuilder.ToString(), testStringBuilder.ToString());
        //}

        //[Fact]
        //public void Print_Item_If_Found_Is_Working_Correctly_When_Found_With_Flights()
        //{
        //    Airport[] testArrayAirport = [new("APP"), new("BAN"), new("MAN")];
        //    Airline[] testArrayAirlines = [new("APP"), new("BAN"), new("MAN")];
        //    Flight[] testArrayFlights = [new("APP"), new("BAN"), new("MAN")];

        //    string[] testInput = ["flight", "APP"];

        //    StringBuilder testStringBuilder = new StringBuilder();

        //    testStringBuilder.AppendLine($"Thеre is a flight with this name on index 0");

        //    StringBuilder expectedStringBuilder = BuildOutputMessage(testArrayAirport, testArrayAirlines, testArrayFlights, testInput);

        //    Assert.Equal(expectedStringBuilder.ToString(), testStringBuilder.ToString());

        //}

        //[Fact]
        //public void Print_Item_If_Found_Is_Working_Correctly_When_Not_Found_With_Flights()
        //{
        //    Airport[] testArrayAirport = [new("APP"), new("BAN"), new("MAN")];
        //    Airline[] testArrayAirlines = [new("APP"), new("BAN"), new("MAN")];
        //    Flight[] testArrayFlights = [new("APP"), new("BAN"), new("MAN")];

        //    string[] testInput = ["flight", "ERR"];

        //    StringBuilder testStringBuilder = new StringBuilder();

        //    testStringBuilder.AppendLine("There is no flight with that name!");

        //    StringBuilder expectedStringBuilder = BuildOutputMessage(testArrayAirport, testArrayAirlines, testArrayFlights, testInput);

        //    Assert.Equal(expectedStringBuilder.ToString(), testStringBuilder.ToString());
        //}

        //[Fact]
        //public void Print_Item_If_Found_Is_Working_Correctly_When_Array_Is_Not_Sorted_Flights()
        //{
        //    Airport[] testArrayAirport = [new("APP"), new("BAN"), new("MAN")];
        //    Airline[] testArrayAirlines = [new("BAN"), new("APP"), new("MAN")];
        //    Flight[] testArrayFlights = [new("BAN"), new("APP"), new("MAN")];

        //    string[] testInput = ["flight", "APP"];

        //    StringBuilder testStringBuilder = new StringBuilder();

        //    testStringBuilder.AppendLine("The array is not sorted! Please sort it first!");

        //    StringBuilder expectedStringBuilder = BuildOutputMessage(testArrayAirport, testArrayAirlines, testArrayFlights, testInput);

        //    Assert.Equal(expectedStringBuilder.ToString(), testStringBuilder.ToString());
        //}

        //[Fact]
        //public void Print_Item_If_Found_Is_Working_Correctly_When_Found_With_Airports()
        //{
        //    Airport[] testArrayAirport = [new("APP"), new("BAN"), new("MAN")];
        //    Airline[] testArrayAirlines = [new("APP"), new("BAN"), new("MAN")];
        //    Flight[] testArrayFlights = [new("APP"), new("BAN"), new("MAN")];

        //    string[] testInput = ["airport", "APP"];

        //    StringBuilder testStringBuilder = new StringBuilder();

        //    testStringBuilder.AppendLine($"Thеre is an airport with this name on index 0");

        //    StringBuilder expectedStringBuilder = BuildOutputMessage(testArrayAirport, testArrayAirlines, testArrayFlights, testInput);

        //    Assert.Equal(expectedStringBuilder.ToString(), testStringBuilder.ToString());
        //}

        //[Fact]
        //public void Print_Item_If_Found_Is_Working_Correctly_When_Not_Found_With_AirPorts()
        //{
        //    Airport[] testArrayAirport = [new("APP"), new("BAN"), new("MAN")];
        //    Airline[] testArrayAirlines = [new("APP"), new("BAN"), new("MAN")];
        //    Flight[] testArrayFlights = [new("APP"), new("BAN"), new("MAN")];

        //    string[] testInput = ["airport", "ERR"];

        //    StringBuilder testStringBuilder = new StringBuilder();

        //    testStringBuilder.AppendLine("There is no airport with that name!");

        //    StringBuilder expectedStringBuilder = BuildOutputMessage(testArrayAirport, testArrayAirlines, testArrayFlights, testInput);

        //    Assert.Equal(expectedStringBuilder.ToString(), testStringBuilder.ToString());
        //}

        //[Fact]
        //public void Print_Item_If_Found_Is_Working_Correctly_When_Array_Is_Not_Sorted_Airport()
        //{
        //    Airport[] testArrayAirport = [new("BAN"), new("APP"), new("MAN")];
        //    Airline[] testArrayAirlines = [new("BAN"), new("APP"), new("MAN")];
        //    Flight[] testArrayFlights = [new("BAN"), new("APP"), new("MAN")];

        //    string[] testInput = ["airport", "APP"];

        //    StringBuilder testStringBuilder = new StringBuilder();

        //    testStringBuilder.AppendLine("The array is not sorted! Please sort it first!");

        //    StringBuilder expectedStringBuilder = BuildOutputMessage(testArrayAirport, testArrayAirlines, testArrayFlights, testInput);

        //    Assert.Equal(expectedStringBuilder.ToString(), testStringBuilder.ToString());
        //}

        //[Fact]
        //public void Print_Item_If_Found_Is_Working_Correctly_When_There_Is_No_Such_Type()
        //{
        //    Airport[] testArrayAirport = [new("APP"), new("BAN"), new("MAN")];
        //    Airline[] testArrayAirlines = [new("APP"), new("BAN"), new("MAN")];
        //    Flight[] testArrayFlights = [new("APP"), new("BAN"), new("MAN")];

        //    string[] testInput = ["error", "APP"];

        //    StringBuilder testStringBuilder = new StringBuilder();

        //    testStringBuilder.AppendLine("There is no such type!");

        //    StringBuilder expectedStringBuilder = BuildOutputMessage(testArrayAirport, testArrayAirlines, testArrayFlights, testInput);

        //    Assert.Equal(expectedStringBuilder.ToString(), testStringBuilder.ToString());
        //}

        //[Fact]
        //public void Is_PrintOutput_Method_Working_Correctly_Airport()
        //{
        //    Airport[] testArrayAirport = [new("APP"), new("BAN"), new("MAN")];

        //    StringBuilder actualStringBuilder = BuildAirportList(testArrayAirport);
        //    StringBuilder expectedStringBuilder = new StringBuilder();
        //    expectedStringBuilder.AppendLine("Airports:");
        //    expectedStringBuilder.Append("APP BAN MAN ");

        //    Assert.Equal(actualStringBuilder.ToString(), expectedStringBuilder.ToString());
        //}

        [Fact]
        public void Is_PrintOutput_Method_Working_Correctly_Airline()
        {
            Airline[] testArrayAirport = [new("APP"), new("BAN"), new("MAN")];

            StringBuilder actualStringBuilder = BuildAirlineList(testArrayAirport);
            StringBuilder expectedStringBuilder = new StringBuilder();
            expectedStringBuilder.AppendLine("Airlines:");
            expectedStringBuilder.Append("APP BAN MAN ");

            Assert.Equal(actualStringBuilder.ToString(), expectedStringBuilder.ToString());
        }

        [Fact]
        public void Is_PrintOutput_Method_Working_Correctly_Flight()
        {
            Flight[] testArrayAirport = [new("APP"), new("BAN"), new("MAN")];

            StringBuilder actualStringBuilder = BuildFlightList(testArrayAirport);
            StringBuilder expectedStringBuilder = new StringBuilder();
            expectedStringBuilder.AppendLine("Flights:");
            expectedStringBuilder.Append("APP BAN MAN ");

            Assert.Equal(actualStringBuilder.ToString(), expectedStringBuilder.ToString());
        }
    }
}
