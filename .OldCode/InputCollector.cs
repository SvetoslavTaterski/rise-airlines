﻿using DataContracts;
using static BusinessLogic.SearchingAlgorithms;
using static BusinessLogic.ValidatingMethods;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable CS8604 // Possible null reference argument.
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.

namespace BusinessLogic
{
    public class InputCollector
    {
        public static void LoadData(Airport[] airports)
        {
            for (int i = 0; i < airports.Length; i++)
            {
                Console.WriteLine("Enter the name of the airport:");
                string newAirportName = Console.ReadLine();

                //if (IsExisting(newAirportName, airports))
                //{
                //    Console.WriteLine("This airport already exists!");
                //    continue;
                //}

                if (IsAirportInputCorrect(newAirportName))
                {
                    var newAirport = new Airport(newAirportName);

                    airports[i] = newAirport;
                }
                else
                {
                    Console.WriteLine("Invalid airport name!");
                }
            }
        }

        public static void LoadData(Airline[] airlines)
        {
            for (int i = 0; i < airlines.Length; i++)
            {
                Console.WriteLine("Enter the name of the airline:");
                string newAirlineName = Console.ReadLine();

                //if (IsExisting(newAirlineName, airlines))
                //{
                //    Console.WriteLine("This airline already exists!");
                //    continue;
                //}

                if (IsAirlineInputCorrect(newAirlineName))
                {
                    var newAirline = new Airline(newAirlineName);
                    airlines[i] = newAirline;
                }
                else
                {
                    Console.WriteLine("Invalid airline name!");
                }
            }
        }

        public static void LoadData(Flight[] flights)
        {
            for (int i = 0; i < flights.Length; i++)
            {
                Console.WriteLine("Enter the name of the flight:");
                string newFlightName = Console.ReadLine();

                //if (IsExisting(newFlightName, flights))
                //{
                //    Console.WriteLine("This flight already exists!");
                //    continue;
                //}

                if (IsFlightInputCorrect(newFlightName))
                {
                    var newFlight = new Flight(newFlightName);

                    flights[i] = newFlight;
                }
                else
                {
                    Console.WriteLine("Invalid name of flight!");
                }
            }
        }
    }
}
