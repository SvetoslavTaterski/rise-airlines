﻿using static Airlines.Program;
using DataContracts;
using static BusinessLogic.ValidatingMethods;

#pragma warning disable IDE0007 // Use implicit type
namespace Airlines.UnitTests
{
    public class ValidationTests
    {
        [Fact]
        public void Is_Alphanumeric_Method_Working_Correctly()
        {
            string word = "Banan1";

            bool result = IsAlphanumeric(word);

            Assert.True(result);
        }

        [Theory]
        [InlineData("Banan1")]
        [InlineData("ApPlEe2")]
        [InlineData("Watermelon3")]
        public void Is_Alphanumeric_Method_Working_Correctly_With_Multiple_Data(string word)
        {
            bool result = IsAlphanumeric(word);

            Assert.True(result);
        }

        [Fact]
        public void Is_Alphanumeric_Method_Working_Correctly_With_Wrong_Data()
        {
            string word = "Banan!!!";

            bool result = IsAlphanumeric(word);

            Assert.False(result);
        }

        [Theory]
        [InlineData("Banan1!!!")]
        [InlineData("ApPlEe2###")]
        [InlineData("Watermelon3$$$")]
        public void Is_Aplhanumeric_Method_Working_Correctly_With_Wrong_Data_And_MultipleData(string word)
        {
            bool result = IsAlphanumeric(word);

            Assert.False(result);
        }

        [Fact]
        public void Is_Alphabetic_Method_Working_Correctly()
        {
            string word = "Banan";

            bool result = IsAlphabetic(word);

            Assert.True(result);
        }

        [Theory]
        [InlineData("Banan")]
        [InlineData("ApPlEe")]
        [InlineData("Watermelon")]
        public void Is_Alphabetic_Method_Working_Correctly_With_Multiple_Data(string word)
        {
            bool result = IsAlphabetic(word);

            Assert.True(result);
        }

        [Fact]
        public void Is_Alphabetic_Method_Working_Correctly_With_Wrong_Data()
        {
            string word = "Banan123";

            bool result = IsAlphabetic(word);

            Assert.False(result);
        }

        [Theory]
        [InlineData("Banan111")]
        [InlineData("ApPlEe222")]
        [InlineData("Watermelon333")]
        public void Is_Alphabetic_Method_Working_Correctly_With_Wrong_Data_And_MultipleData(string word)
        {
            bool result = IsAlphabetic(word);

            Assert.False(result);
        }

        [Theory]
        [InlineData("AIR")]
        [InlineData("RIA")]
        [InlineData("BAN")]
        public void Is_Airport_Input_Correct_Working_Correctly(string word)
        {
            bool result = IsAirportInputCorrect(word);

            Assert.True(result);
        }

        [Theory]
        [InlineData("")]
        [InlineData("AAAAAA")]
        [InlineData("111")]
        [InlineData("!?#")]
        public void Is_Airport_Input_Correct_Working_Correctly_With_Wrong_Data(string word)
        {
            bool result = IsAirportInputCorrect(word);

            Assert.False(result);
        }

        [Theory]
        [InlineData("Airl1")]
        [InlineData("Airl2")]
        [InlineData("Airl3")]
        public void Is_Airline_Input_Correct_Working_Correctly(string word)
        {
            bool result = IsAirlineInputCorrect(word);

            Assert.True(result);
        }

        [Theory]
        [InlineData("Airl111111")]
        [InlineData("")]
        [InlineData("Airl3e")]
        public void Is_Airline_Input_Correct_Working_Correctly_With_Wrong_Data(string word)
        {
            bool result = IsAirlineInputCorrect(word);

            Assert.False(result);
        }

        [Theory]
        [InlineData("Flight")]
        [InlineData("11111")]
        [InlineData("Flight123")]
        public void Is_Flight_Input_Correct_Working_Correctly(string word)
        {
            bool result = IsFlightInputCorrect(word);

            Assert.True(result);
        }

        [Theory]
        [InlineData("")]
        [InlineData("!!!")]
        public void Is_Flight_Input_Correct_Working_Correctly_With_Wrong_Data(string word)
        {
            bool result = IsFlightInputCorrect(word);

            Assert.False(result);
        }

        [Fact]
        public void Is_CheckSortOrder_Working_Correctly_When_Ascending_Airport()
        {
            Airport[] testArray = [new("APP"), new("BAN"), new("MAN")];

            string actualResult = CheckSortOrder(testArray);
            string expectedResult = "ascending";

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_CheckSortOrder_Working_Correctly_When_Descending_Airport()
        {
            Airport[] testArray = [new("MAN"), new("BAN"), new("APP")];

            string actualResult = CheckSortOrder(testArray);
            string expectedResult = "descending";

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_CheckSortOrder_Working_Correctly_When_Array_Is_Not_Sorted_Airport()
        {
            Airport[] testArray = [new("MAN"), new("APP"), new("BAN")];

            string actualResult = CheckSortOrder(testArray);
            string expectedResult = "unsorted";

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_CheckSortOrder_Working_Correctly_When_Ascending_Airline()
        {
            Airline[] testArray = [new("APP"), new("BAN"), new("MAN")];

            string actualResult = CheckSortOrder(testArray);
            string expectedResult = "ascending";

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_CheckSortOrder_Working_Correctly_When_Descending_Airline()
        {
            Airline[] testArray = [new("MAN"), new("BAN"), new("APP")];

            string actualResult = CheckSortOrder(testArray);
            string expectedResult = "descending";

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_CheckSortOrder_Working_Correctly_When_Array_Is_Not_Sorted_Airline()
        {
            Airline[] testArray = [new("MAN"), new("APP"), new("BAN")];

            string actualResult = CheckSortOrder(testArray);
            string expectedResult = "unsorted";

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_CheckSortOrder_Working_Correctly_When_Ascending_Flight()
        {
            Flight[] testArray = [new("APP"), new("BAN"), new("MAN")];

            string actualResult = CheckSortOrder(testArray);
            string expectedResult = "ascending";

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_CheckSortOrder_Working_Correctly_When_Descending_Flight()
        {
            Flight[] testArray = [new("MAN"), new("BAN"), new("APP")];

            string actualResult = CheckSortOrder(testArray);
            string expectedResult = "descending";

            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void Is_CheckSortOrder_Working_Correctly_When_Array_Is_Not_Sorted_Flight()
        {
            Flight[] testArray = [new("MAN"), new("APP"), new("BAN")];

            string actualResult = CheckSortOrder(testArray);
            string expectedResult = "unsorted";

            Assert.Equal(expectedResult, actualResult);
        }
    }
}
