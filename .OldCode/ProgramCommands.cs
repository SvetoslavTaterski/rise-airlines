﻿using DataContracts;
using System.Text;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable IDE0305 // Simplify collection initialization
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
namespace BusinessLogic
{
    public static class ProgramCommands
    {
        public static StringBuilder SearchItem(HashSet<Airport> airports, HashSet<Airline> airlines, HashSet<Flight> flights, string[] commandLine)
        {
            StringBuilder sb = new StringBuilder();

            string type = commandLine[1];
            string name = commandLine[2];

            if (commandLine.Length > 2)
            {
                for (int i = 3; i <= commandLine.Length - 1; i++)
                {
                    name += " " + commandLine[i];
                }
            }

            if (type == "airport")
            {
                Airport airport = airports.FirstOrDefault(a => a.Name == name);

                if (airport != null)
                {
                    sb.AppendLine($"There is an airport with the given name!");
                }
                else
                {
                    sb.AppendLine("There is no such airport!");
                }
            }
            else if (type == "airline")
            {
                Airline airline = airlines.FirstOrDefault(a => a.Name == name);

                if (airline != null)
                {
                    sb.AppendLine($"There is an airline with the given name!");
                }
                else
                {
                    sb.AppendLine("There is no such airline!");
                }
            }
            else if (type == "flight")
            {
                Flight flight = flights.FirstOrDefault(a => a.Id == name);

                if (flight != null)
                {
                    sb.AppendLine($"There is an flight with the given id!");
                }
                else
                {
                    sb.AppendLine("There is no such flight!");
                }
            }
            else
            {
                sb.AppendLine("No such type!");
            }

            return sb;
        }

        public static StringBuilder SortItems(HashSet<Airport> airports, HashSet<Airline> airlines, HashSet<Flight> flights, string[] commandLine)
        {
            StringBuilder sb = new StringBuilder();

            if (commandLine.Length == 3)
            {
                string typeToSort = commandLine[1];
                string order = commandLine[2];

                if (typeToSort == "airports")
                {
                    HashSet<Airport> sortedAirports = [];

                    if (order == "ascending")
                    {
                        sortedAirports = airports.OrderBy(a => a.Name).ToHashSet();
                    }
                    else if (order == "descending")
                    {
                        sortedAirports = airports.OrderByDescending(a => a.Name).ToHashSet();
                    }

                    sb.AppendLine(BuildMessages.BuildListMessage(sortedAirports, airports => airports.Name).ToString());
                }
                else if (typeToSort == "airlines")
                {
                    HashSet<Airline> sortedAirlines = [];

                    if (order == "ascending")
                    {
                        sortedAirlines = airlines.OrderBy(a => a.Name).ToHashSet();
                    }
                    else if (order == "descending")
                    {
                        sortedAirlines = airlines.OrderByDescending(a => a.Name).ToHashSet();
                    }

                    sb.AppendLine(BuildMessages.BuildListMessage(sortedAirlines, airlines => airlines.Name).ToString());
                }
                else if (typeToSort == "flights")
                {
                    HashSet<Flight> sortedFlights = [];

                    if (order == "ascending")
                    {
                        sortedFlights = flights.OrderBy(a => a.Id).ToHashSet();
                    }
                    else if (order == "descending")
                    {
                        sortedFlights = flights.OrderByDescending(a => a.Id).ToHashSet();
                    }

                    sb.AppendLine(BuildMessages.BuildListMessage(sortedFlights, flights => flights.Id).ToString());
                }
            }
            else if (commandLine.Length == 2)
            {
                string typeToSort = commandLine[1];

                if (typeToSort == "airports")
                {
                    sb.AppendLine(BuildMessages.BuildListMessage(airports.OrderBy(a => a.Name).ToHashSet(), airports => airports.Name).ToString());
                }
                else if (typeToSort == "airlines")
                {
                    sb.AppendLine(BuildMessages.BuildListMessage(airlines.OrderBy(a => a.Name).ToHashSet(), airlines => airlines.Name).ToString());
                }
                else if (typeToSort == "flights")
                {
                    sb.AppendLine(BuildMessages.BuildListMessage(flights.OrderBy(a => a.Id).ToHashSet(), flights => flights.Id).ToString());
                }
            }

            return sb;
        }

        public static StringBuilder IsAirlineExisting(HashSet<Airline> airlines, string[] commandLine)
        {
            StringBuilder sb = new StringBuilder();

            string airlineToBeFound = commandLine[1];

            sb.AppendLine(airlines.Any(a => a.Name == airlineToBeFound).ToString());

            return sb;
        }

        public static StringBuilder ListAllAirports(HashSet<Airport> airports, string[] commandLine)
        {
            string name = commandLine[1];
            string cityOrCountry = commandLine[2];

            var sb = new StringBuilder();

            if (cityOrCountry == "City")
            {
                var allAirportsInThisCity = airports.Where(a => a.City == name).ToHashSet();
                sb.AppendLine($"All airports in {name}:");

                foreach (var airport in allAirportsInThisCity)
                {
                    sb.Append($"{airport.Name}, ");
                }

                return sb.Remove(sb.Length - 2, 2);
            }
            else if (cityOrCountry == "Country")
            {
                var allAirportsInThisCountry = airports.Where(a => a.Country == name).ToHashSet();
                sb.AppendLine($"All airports in {name}:");

                foreach (var airport in allAirportsInThisCountry)
                {
                    sb.Append($"{airport.Name}, ");
                }

                return sb.Remove(sb.Length - 2, 2);
            }

            return sb;
        }
    }
}
