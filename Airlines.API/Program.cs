
using Airlines.Persistence.Basic.Classes;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Airlines.API;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.

        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        builder.Services.AddDbContext<RISEAirlinesContext>(options =>
                options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));


        builder.Services.AddScoped<IFlightRepository, FlightRepository>();
        builder.Services.AddScoped<IAirportRepository, AirportRepository>();
        builder.Services.AddScoped<IAirlineRepository, AirlineRepository>();

        builder.Services.AddCors(options =>
        {
            options.AddPolicy(name: "MyOrigin",
                              policy =>
                              {
                                  policy.AllowAnyHeader().AllowAnyOrigin().AllowAnyMethod();
                              });
        });


        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        app.UseCors("MyOrigin");

        app.UseAuthorization();


        app.MapControllers();

        app.Run();
    }
}
