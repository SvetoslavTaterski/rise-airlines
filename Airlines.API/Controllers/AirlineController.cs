﻿using Microsoft.AspNetCore.Mvc;
using Airlines.Persistence.Basic.Interfaces;
using Airlines.Persistence.Basic.Entities;
#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0021 // Use expression body for constructor

namespace Airlines.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AirlineController : ControllerBase
{
    public IAirlineRepository _airlineRepository;

    public AirlineController(IAirlineRepository airlineRepository)
    {

        _airlineRepository = airlineRepository;

    }

    // GET: api/<AirlineController>
    [HttpGet]
    public ActionResult<IEnumerable<Airline>> GetAllAirlines()
    {
        var airlines = _airlineRepository.GetAllAirlines();

        if (airlines.Count == 0)
        {
            return BadRequest("There are no airlines!");
        }

        return Ok(airlines);
    }

    // GET api/<AirlineController>/5
    [HttpGet("{id}")]
    public ActionResult<Flight> GetAirline(int id)
    {
        var airline = _airlineRepository.GetAirlineById(id);

        if (airline == null)
        {
            return NotFound("There is no such Airline!");
        }

        return Ok(airline);
    }

    // POST api/<AirlineController>
    [HttpPost]
    public ActionResult CreateAirline([FromBody] Airline airline)
    {
        try
        {
            _airlineRepository.AddAirline(airline);
        }
        catch
        {
            return BadRequest("Something went wrong! Contact Administrator.");
        }

        return Ok(airline);
    }

    // PUT api/<AirlineController>/5
    //[HttpPut("{id}")]
    //public ActionResult EditAirline(int id, [FromBody] Airline airline)
    //{
    //    var airlineToEdit = _airlineRepository.GetAirlineById(id);

    //    if (airlineToEdit == null)
    //    {
    //        return BadRequest("There is no such airline");
    //    }

    //    airline.AirlineId = id;

    //    if (ModelState.IsValid)
    //    {
    //        _airlineRepository.UpdateAirline(airline);
    //        return Ok(airline);
    //    }

    //    return BadRequest();
    //}

    [HttpPut("{airlineName}")]
    public ActionResult EditAirline(string airlineName, [FromBody] Airline airline)
    {
        var airlineToEdit = _airlineRepository.GetAirlineByName(airlineName);

        if (airlineToEdit == null)
        {
            return BadRequest("There is no such airline");
        }

        // Update the properties of the airline entity
        airlineToEdit.Founded = airline.Founded;
        airlineToEdit.FleetSize = airline.FleetSize;
        airlineToEdit.AirlineDescription = airline.AirlineDescription;

        if (ModelState.IsValid)
        {
            _airlineRepository.UpdateAirline(airlineToEdit);
            return Ok(airlineToEdit);
        }

        return BadRequest(ModelState);
    }


    // DELETE api/<AirlineController>/5
    [HttpDelete("id/{id:int}")]
    public ActionResult DeleteAirline(int id)
    {
        var airline = _airlineRepository.GetAirlineById(id);

        if (airline == null)
        {
            return BadRequest("There is no such Airline");
        }

        _airlineRepository.RemoveAirlineById(id);
        return Ok();
    }

    [HttpDelete("airlineName/{name}")]
    public ActionResult DeleteAirline(string name)
    {
        var airline = _airlineRepository.GetAirlineByName(name);

        if (airline == null)
        {
            return BadRequest("There is no such airline");
        }

        _airlineRepository.RemoveAirlineById(airline.AirlineId);
        return Ok();
    }
}
