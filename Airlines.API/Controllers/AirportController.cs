﻿using Microsoft.AspNetCore.Mvc;
using Airlines.Persistence.Basic.Interfaces;
using Airlines.Persistence.Basic.Entities;
#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0021 // Use expression body for constructor

namespace Airlines.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AirportController : ControllerBase
{
    public IAirportRepository _airportRepository;

    public AirportController(IAirportRepository airportRepository)
    {
        _airportRepository = airportRepository;
    }

    // GET: api/<AirportController>
    [HttpGet]
    public ActionResult<IEnumerable<Airport>> GetAllAirports()
    {
        var airports = _airportRepository.GetAllAirports();

        if (airports.Count == 0)
        {
            return BadRequest("There are no airports!");
        }

        return Ok(airports);
    }

    // GET api/<AirportController>/5
    [HttpGet("{id}")]
    public ActionResult<Flight> GetAirport(int id)
    {
        var airport = _airportRepository.GetAirportById(id);

        if (airport == null)
        {
            return NotFound("There is no such airport!");
        }

        return Ok(airport);
    }

    // POST api/<AirportController>
    [HttpPost]
    public ActionResult CreateAirport([FromBody] Airport airport)
    {
        try
        {
            _airportRepository.AddAirport(airport);
        }
        catch
        {
            return BadRequest("Something went wrong! Contact Administrator.");
        }

        return Ok(airport);
    }

    // PUT api/<AirportController>/5
    //[HttpPut("{id}")]
    //public ActionResult EditAirport(int id, [FromBody] Airport airport)
    //{
    //    var airportToEdit = _airportRepository.GetAirportById(id);

    //    if (airportToEdit == null)
    //    {
    //        return BadRequest("There is no such airport");
    //    }

    //    airport.AirportId = id;

    //    if (ModelState.IsValid)
    //    {
    //        _airportRepository.UpdateAirport(airport);
    //        return Ok(airport);
    //    }

    //    return BadRequest();
    //}

    [HttpPut("{airportName}")]
    public ActionResult EditAirport(string airportName, [FromBody] Airport airport)
    {
        var airportToEdit = _airportRepository.GetAirportByName(airportName);

        if (airportToEdit == null)
        {
            return BadRequest("There is no such airport");
        }

        // Update the properties accordingly
        airportToEdit.AirportName = airport.AirportName;
        airportToEdit.Country = airport.Country;
        airportToEdit.City = airport.City;
        airportToEdit.Code = airport.Code;
        airportToEdit.RunwayCount = airport.RunwayCount;
        airportToEdit.Founded = airport.Founded;

        if (ModelState.IsValid)
        {
            _airportRepository.UpdateAirport(airportToEdit);
            return Ok(airportToEdit);
        }

        return BadRequest(ModelState);
    }

    // DELETE api/<AirportController>/5
    [HttpDelete("id/{id:int}")]
    public ActionResult DeleteAirport(int id)
    {
        var airport = _airportRepository.GetAirportById(id);

        if (airport == null)
        {
            return BadRequest("There is no such airport");
        }

        _airportRepository.RemoveAirportById(id);
        return Ok();
    }

    [HttpDelete("airportName/{airportName}")]
    public ActionResult DeleteAirport(string airportName)
    {
        var airport = _airportRepository.GetAirportByName(airportName);

        if (airport == null)
        {
            return BadRequest("There is no such airport");
        }

        _airportRepository.RemoveAirportById(airport.AirportId);
        return Ok();
    }
}
