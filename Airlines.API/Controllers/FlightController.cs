﻿using Microsoft.AspNetCore.Mvc;
using Airlines.Persistence.Basic.Interfaces;
using Airlines.Persistence.Basic.Entities;
using Microsoft.AspNetCore.Cors;
#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0021 // Use expression body for constructor

namespace Airlines.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class FlightController : ControllerBase
{
    public IFlightRepository _flightRepository;

    public FlightController(IFlightRepository flightRepository)
    {
        _flightRepository = flightRepository;
    }

    // GET: api/<FlightController>
    [HttpGet]
    public ActionResult<IEnumerable<Flight>> GetAllFlights()
    {
        var flights = _flightRepository.GetAllFlights();

        if (flights.Count == 0)
        {
            return BadRequest("There are no flights!");
        }

        return Ok(flights);
    }

    // GET api/<FlightController>/5
    [HttpGet("{id}")]
    public ActionResult<Flight> GetFlight(int id)
    {
        var flight = _flightRepository.GetFlightById(id);

        if (flight == null)
        {
            return NotFound("There is no such flight");
        }

        return Ok(flight);
    }

    // POST api/<FlightController>
    [HttpPost]
    public ActionResult CreateFlight([FromBody] Flight flight)
    {
        try
        {
            _flightRepository.AddFlight(flight);
        }
        catch
        {
            return BadRequest("Something went wrong! Contact administrator.");
        }

        return Ok();
    }

    // PUT api/<FlightController>/5
    //[HttpPut("{id}")]
    //public ActionResult EditFlight(int id, [FromBody] Flight flight)
    //{
    //    var flightToEdit = _flightRepository.GetFlightById(id);

    //    if (flightToEdit == null)
    //    {
    //        return BadRequest("There is no such flight");
    //    }

    //    flight.FlightId = id;

    //    if (ModelState.IsValid)
    //    {
    //        _flightRepository.UpdateFlight(flight);
    //        return Ok(flight);
    //    }

    //    return BadRequest();

    //}

    [HttpPut("{flightNumber}")]
    public ActionResult EditFlight(string flightNumber, [FromBody] Flight flight)
    {
        if (flight == null)
        {
            return BadRequest("Invalid flight data.");
        }

        var flightToEdit = _flightRepository.GetFlightByFlightNumber(flightNumber);

        if (flightToEdit == null)
        {
            return NotFound("There is no such flight.");
        }

        flightToEdit.FromAirportId = flight.FromAirportId;
        flightToEdit.ToAirportId = flight.ToAirportId;
        flightToEdit.Departure = flight.Departure;
        flightToEdit.Arrival = flight.Arrival;

        _flightRepository.UpdateFlight(flightToEdit);

        return Ok(flightToEdit);
    }



    // DELETE api/<FlightController>/5
    [HttpDelete("id/{id:int}")]
    public ActionResult DeleteFlight(int id)
    {
        var flight = _flightRepository.GetFlightById(id);

        if (flight == null)
        {
            return BadRequest("There is no such flight");
        }

        _flightRepository.RemoveFlightById(id);
        return Ok();

    }

    [HttpDelete("flightNumber/{flightNumber}")]
    public ActionResult DeleteFlight(string flightnumber)
    {
        var flight = _flightRepository.GetFlightByFlightNumber(flightnumber);

        if (flight == null)
        {
            return BadRequest("There is no such flight");
        }

        _flightRepository.RemoveFlightById(flight.FlightId);
        return Ok();

    }
}
