﻿using DataContracts;
using System.Text;

namespace BusinessLogic.Commands
{
    public class ReserveCargoCommand : ICommand
    {
        public string Id { get; }

        public int Weight { get; }

        public double Volume { get; }

        public HashSet<Flight> Flights { get; }

        private ReserveCargoCommand(string id, int weight, double volume, HashSet<Flight> flights)
        {
            Id = id;
            Weight = weight;
            Volume = volume;
            Flights = flights;
        }

        public StringBuilder Execute() => CommandHandlers.ReserveCargoCommand.HandleReserveCargoCommand(Id, Weight, Volume, Flights);

        public static ReserveCargoCommand CreateReserveCargoCommand(string id, int weight, double volume, HashSet<Flight> flights)
            => new(id, weight, volume, flights);
    }
}
