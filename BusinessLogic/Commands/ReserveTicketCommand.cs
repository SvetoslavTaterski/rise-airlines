﻿using DataContracts;
using System.Text;

namespace BusinessLogic.Commands
{
    public class ReserveTicketCommand : ICommand
    {
        public string Id { get; }

        public int Seats { get; }

        public int SmallBaggageCount { get; }

        public int LargeBaggageCount { get; }

        public HashSet<Flight> Flights { get; set; }

        private ReserveTicketCommand(string id, int seats, int smallBaggageCount, int largeBaggageCount, HashSet<Flight> flights)
        {
            Id = id;
            Seats = seats;
            SmallBaggageCount = smallBaggageCount;
            LargeBaggageCount = largeBaggageCount;
            Flights = flights;
        }

        public StringBuilder Execute() => CommandHandlers.ReserveTicketCommand.ReserveTicketCommandHandler(Id, Seats, SmallBaggageCount, LargeBaggageCount, Flights);

        public static ReserveTicketCommand CreateReserveTicketCommand(string id, int seats, int smallBaggageCount, int largeBaggageCount, HashSet<Flight> flights)
            => new(id, seats, smallBaggageCount, largeBaggageCount, flights);
    }
}
