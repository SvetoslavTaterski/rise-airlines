﻿using System.Text;

namespace BusinessLogic.Commands
{
    public interface ICommand
    {
        StringBuilder Execute();
    }
}
