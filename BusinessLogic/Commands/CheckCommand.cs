﻿using DataContracts;
using System.Text;

namespace BusinessLogic.Commands
{
    public class CheckCommand : ICommand
    {
        public Graph Graph { get; set; }

        public string DestinationAirport { get; set; }

        public string SourceAirport { get; set; }

        private CheckCommand(Graph graph, string destinationAirport, string sourceAirport)
        {
            Graph = graph;
            DestinationAirport = destinationAirport;
            SourceAirport = sourceAirport;
        }

        public StringBuilder Execute() => CommandHandlers.CheckCommand.HandleCheck(Graph, DestinationAirport, SourceAirport);

        public static CheckCommand CreateCheckCommand(Graph graph, string destinationAirport, string sourceAirport)
            => new(graph, destinationAirport, sourceAirport);
    }
}

