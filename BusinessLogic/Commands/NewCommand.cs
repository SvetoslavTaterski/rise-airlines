﻿using DataContracts;
using System.Text;

#pragma warning disable IDE0021 // Use expression body for constructor
namespace BusinessLogic.Commands
{
    public class NewCommand : ICommand
    {
        public LinkedList<Flight> Route { get; set; }

        private NewCommand(LinkedList<Flight> route)
        {
            Route = route;
        }

        public StringBuilder Execute() => CommandHandlers.NewCommand.HandleNewCommand(Route);

        public static NewCommand CreateNewCommand(LinkedList<Flight> route) => new(route);
    }
}
