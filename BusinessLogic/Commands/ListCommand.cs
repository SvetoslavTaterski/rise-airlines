﻿using DataContracts;
using System.Text;

namespace BusinessLogic.Commands
{
    public class ListCommand : ICommand
    {
        private string[] Input { get; }

        private HashSet<Airport> Airports { get; }

        private ListCommand(string[] input, HashSet<Airport> airports)
        {
            Input = input;
            Airports = airports;
        }

        public StringBuilder Execute() => CommandHandlers.ListCommand.HandleList(Input, Airports);

        public static ListCommand CreateListCommand(string[] input, HashSet<Airport> airports) => new(input, airports);
    }
}
