﻿using DataContracts;
using System.Text;

namespace BusinessLogic.Commands
{
    public class SortCommand : ICommand
    {
        private HashSet<Airline> Airlines { get; }

        private HashSet<Airport> Airports { get; }

        private HashSet<Flight> Flights { get; }

        private string[] Input { get; }

        private SortCommand(string[] input, HashSet<Airport> airports, HashSet<Airline> airlines, HashSet<Flight> flights)
        {
            Airlines = airlines;
            Airports = airports;
            Flights = flights;
            Input = input;
        }

        public StringBuilder Execute() => CommandHandlers.SortCommand.HandleSort(Input, Airports, Airlines, Flights);

        public static SortCommand CreateSortCommand(string[] input, HashSet<Airport> airports, HashSet<Airline> airlines, HashSet<Flight> flights)
            => new(input, airports, airlines, flights);
    }
}
