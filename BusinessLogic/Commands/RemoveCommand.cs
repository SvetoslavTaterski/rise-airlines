﻿using DataContracts;
using System.Text;

#pragma warning disable IDE0021 // Use expression body for constructor
namespace BusinessLogic.Commands
{
    public class RemoveCommand : ICommand
    {
        private LinkedList<Flight> Route { get; }

        private RemoveCommand(LinkedList<Flight> route)
        {
            Route = route;
        }

        public StringBuilder Execute() => CommandHandlers.RemoveCommand.HandleRemove(Route);

        public static RemoveCommand CreateRemoveCommand(LinkedList<Flight> route) => new(route);
    }
}
