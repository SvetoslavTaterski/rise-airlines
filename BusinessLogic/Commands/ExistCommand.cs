﻿using DataContracts;
using System.Text;

namespace BusinessLogic.Commands
{
    public class ExistCommand : ICommand
    {
        private string[] Input { get; }

        private HashSet<Airline> Airlines { get; }

        private ExistCommand(string[] input, HashSet<Airline> airlines)
        {
            Input = input;
            Airlines = airlines;
        }

        public StringBuilder Execute() => CommandHandlers.ExistCommand.HandleExist(Input, Airlines);

        public static ExistCommand CreateExistCommand(string[] input, HashSet<Airline> airlines) => new(input, airlines);
    }
}
