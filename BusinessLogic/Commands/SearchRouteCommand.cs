﻿using DataContracts;
using System.Text;

namespace BusinessLogic.Commands
{
    public class SearchRouteCommand : ICommand
    {
        public string SourceAirport { get; set; }

        public string DestinationAirport { get; set; }

        public Graph Graph { get; set; }

        private SearchRouteCommand(string sourceAirport, string destinationAirport, Graph graph)
        {
            SourceAirport = sourceAirport;
            DestinationAirport = destinationAirport;
            Graph = graph;
        }

        public StringBuilder Execute() => CommandHandlers.SearchRouteCommand.SearchRouteCommandHandler(SourceAirport, DestinationAirport, Graph);

        public StringBuilder ExecuteByTime() => CommandHandlers.SearchRouteCommand.SearchRouteCommandHandlerTime(SourceAirport, DestinationAirport, Graph);

        public StringBuilder ExecuteByStops() => CommandHandlers.SearchRouteCommand.SearchRouteCommandHandlerStops(SourceAirport, DestinationAirport, Graph);

        public static SearchRouteCommand CreateSearchRouteCommand(string sourceAirport, string destinationAirport, Graph graph)
            => new(sourceAirport, destinationAirport, graph);
    }
}
