﻿using DataContracts;
using System.Text;

namespace BusinessLogic.Commands
{
    public class AddFlightCommand : ICommand
    {
        private LinkedList<Flight> Route { get; }

        private HashSet<Flight> Flights { get; }

        private string Id { get; }

        private AddFlightCommand(string id, LinkedList<Flight> route, HashSet<Flight> flights)
        {
            Id = id;
            Route = route;
            Flights = flights;
        }

        public StringBuilder Execute() => CommandHandlers.AddFlightCommand.HandleAddFlightCommand(Id, Route, Flights);

        public static AddFlightCommand CreateAddFlightCommand(string id, LinkedList<Flight> route, HashSet<Flight> flights)
            => new(id, route, flights);
    }
}
