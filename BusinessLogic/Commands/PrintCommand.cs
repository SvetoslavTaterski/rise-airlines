﻿using System.Text;
using DataContracts;

#pragma warning disable IDE0021 // Use expression body for constructor
namespace BusinessLogic.Commands
{
    public class PrintCommand : ICommand
    {
        private LinkedList<Flight> Route { get; }

        private PrintCommand(LinkedList<Flight> route)
        {
            Route = route;
        }

        public StringBuilder Execute() => CommandHandlers.PrintCommand.HandlePrint(Route);

        public static PrintCommand CreatePrintCommand(LinkedList<Flight> route) => new(route);
    }
}
