﻿using DataContracts;
using System.Text;

namespace BusinessLogic.Commands
{
    public class FindCommand : ICommand
    {
        public Graph Graph { get; set; }

        public string DestinationAirport { get; set; }

        public string SourceAirport { get; set; }

        private FindCommand(Graph graph, string destinationAirport, string sourceAirport)
        {
            Graph = graph;
            DestinationAirport = destinationAirport;
            SourceAirport = sourceAirport;
        }

        public StringBuilder Execute() => CommandHandlers.FindCommand.HandleFind(Graph, DestinationAirport, SourceAirport);

        public static FindCommand CreateFindCommand(Graph graph, string destinationAirport, string sourceAirport)
            => new(graph, destinationAirport, sourceAirport);
    }
}
