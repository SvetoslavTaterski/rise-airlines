﻿using Airlines.Persistence.Basic.Classes;
using Airlines.Persistence.Basic.Entities;

namespace BusinessLogic.DatabaseOperations;
public class FlightOperations
{
    //public static void PrintFlightWithUsing()
    //{
    //    using var flightRepository = new FlightRepository();
    //    var flightList = flightRepository.GetAllFlights();
    //    foreach (var flight in flightList)
    //    {
    //        Console.WriteLine($"Flight Number: {flight.FlightNumber}, Departure: {flight.Departure}, Arrival: {flight.Arrival}");
    //    }
    //}

    //public static void PrintFlightById(int id)
    //{
    //    using var flightRepository = new FlightRepository();
    //    var flight = flightRepository.GetFlightById(id);

    //    Console.WriteLine($"The flight with this id has flight number-{flight.FlightNumber}");
    //}

    //public static void PrintFlightByFlightNumber(string flightNumber)
    //{
    //    using var flightRepository = new FlightRepository();
    //    var flight = flightRepository.GetFlightByFlightNumber(flightNumber);

    //    Console.WriteLine($"The flight with this number has id-{flight.FlightId}");
    //}

    //public static void PrintFlightsByTimeDifference(int timeDifference)
    //{
    //    using var flightRepository = new FlightRepository();
    //    var flights = flightRepository.GetFlightsByTimeDifference(timeDifference);

    //    foreach (var flight in flights)
    //    {
    //        Console.WriteLine($"Flight with flight number: {flight.FlightNumber} has time difference {timeDifference}");
    //    }
    //}

    //public static void AddFlight(Flight flight)
    //{
    //    using var flightRepository = new FlightRepository();

    //    if (flight == null)
    //    {
    //        throw new ArgumentNullException("Flight is null!");
    //    }
    //    else
    //    {
    //        flightRepository.AddFlight(flight);
    //        Console.WriteLine($"Flight with flight number: {flight.FlightNumber} has been added");
    //    }
    //}

    //public static void RemoveFlightById(int id)
    //{
    //    using var flightRepository = new FlightRepository();

    //    try
    //    {
    //        flightRepository.RemoveFlightById(id);
    //        Console.WriteLine($"Flight with id: {id} has been removed");

    //    }
    //    catch (ArgumentNullException)
    //    {
    //        Console.WriteLine("There was an error with removing element from the database.");
    //        throw;
    //    }
    //}

    //public static void UpdateFlight(Flight flight)
    //{
    //    using var flightRepository = new FlightRepository();

    //    try
    //    {
    //        flightRepository.UpdateFlight(flight);
    //        Console.WriteLine("Flight has been updated!");
    //    }
    //    catch (Exception)
    //    {
    //        Console.WriteLine("There was an error with updating element from the database.");
    //        throw;
    //    }
    //}
}
