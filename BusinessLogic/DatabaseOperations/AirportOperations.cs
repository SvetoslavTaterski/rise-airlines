﻿using Airlines.Persistence.Basic.Classes;
using Airlines.Persistence.Basic.Entities;

namespace BusinessLogic.DatabaseOperations
{
    public class AirportOperations
    {
        //public static void PrintAirportsWithUsing()
        //{
        //    using var airportRepository = new AirportRepository();
        //    var airports = airportRepository.GetAllAirports();

        //    foreach (var airport in airports)
        //    {
        //        Console.WriteLine($"Name: {airport.AirportName}, Country: {airport.Country}, City: {airport.City}");
        //    }
        //}

        //public static void PrintAirportById(int id)
        //{
        //    using var airportRepository = new AirportRepository();
        //    var airport = airportRepository.GetAirportById(id);

        //    Console.WriteLine($"The airport with this id has name-{airport.AirportName}");
        //}

        //public static void PrintAirportByName(string name)
        //{
        //    using var airportRepository = new AirportRepository();
        //    var airport = airportRepository.GetAirportByName(name);

        //    Console.WriteLine($"The airport with this name has id-{airport.AirportId}");
        //}

        //public static void PrintAirportsByCountry(string country)
        //{
        //    using var airportRepository = new AirportRepository();
        //    var airports = airportRepository.GetAirportsByCountry(country);

        //    foreach (var airport in airports)
        //    {
        //        Console.WriteLine($"Airport with name: {airport.AirportName} is in {country}");
        //    }
        //}

        //public static void AddAirport(Airport airport)
        //{
        //    using var airportRepository = new AirportRepository();

        //    if (airport == null)
        //    {
        //        throw new ArgumentNullException("Airport is null!");
        //    }
        //    else
        //    {
        //        airportRepository.AddAirport(airport);
        //        Console.WriteLine($"Airport with name: {airport.AirportName} has been added");
        //    }
        //}

        //public static void RemoveAirportById(int id)
        //{
        //    using var airportRepository = new AirportRepository();

        //    try
        //    {
        //        airportRepository.RemoveAirportById(id);
        //        Console.WriteLine($"Airport with id: {id} has been removed");

        //    }
        //    catch (ArgumentNullException)
        //    {
        //        Console.WriteLine("There was an error with removing element from the database.");
        //        throw;
        //    }
        //}

        //public static void UpdateAirport(Airport airport)
        //{
        //    using var airportRepository = new AirportRepository();

        //    try
        //    {
        //        airportRepository.UpdateAirport(airport);
        //        Console.WriteLine("Airport has been updated!");
        //    }
        //    catch (Exception)
        //    {
        //        Console.WriteLine("There was an error with updating element from the database.");
        //        throw;
        //    }
        //}
    }
}
