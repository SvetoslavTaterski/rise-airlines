﻿using Airlines.Persistence.Basic.Classes;
using Airlines.Persistence.Basic.Entities;

namespace BusinessLogic.DatabaseOperations
{
    public class AirlineOperations
    {
        //public static void PrintAirlinesWithUsing()
        //{
        //    using var airlineRepository = new AirlineRepository();
        //    var airlines = airlineRepository.GetAllAirlines();

        //    foreach (var airline in airlines)
        //    {
        //        Console.WriteLine($"Name: {airline.AirlineName}, Founded: {airline.Founded}, FleetSize: {airline.FleetSize}");
        //    }
        //}

        //public static void PrintAirlineById(int id)
        //{
        //    using var airlineRepository = new AirlineRepository();
        //    var airport = airlineRepository.GetAirlineById(id);

        //    Console.WriteLine($"The airline with this id has name-{airport.AirlineName}");
        //}

        //public static void PrintAirlineByName(string name)
        //{
        //    using var airlineRepository = new AirlineRepository();
        //    var airline = airlineRepository.GetAirlineByName(name);

        //    Console.WriteLine($"The airline with this name has id-{airline.AirlineId}");
        //}

        //public static void PrintAirlinesByFleetSize(int fleetSize)
        //{
        //    using var airlineRepository = new AirlineRepository();
        //    var airlines = airlineRepository.GetAirlinesByFleetSize(fleetSize);

        //    foreach (var airline in airlines)
        //    {
        //        Console.WriteLine($"Airline with name: {airline.AirlineName} has fleet size {fleetSize}");
        //    }
        //}

        //public static void AddAirline(Airline airline)
        //{
        //    using var airlineRepository = new AirlineRepository();

        //    if (airline == null)
        //    {
        //        throw new ArgumentNullException("Airline is null!");
        //    }
        //    else
        //    {
        //        airlineRepository.AddAirline(airline);
        //        Console.WriteLine($"Airline with name: {airline.AirlineName} has been added");
        //    }
        //}

        //public static void RemoveAirlineById(int id)
        //{
        //    using var airlineRepository = new AirlineRepository();

        //    try
        //    {
        //        airlineRepository.RemoveAirlineById(id);
        //        Console.WriteLine($"Airline with id: {id} has been removed");

        //    }
        //    catch (ArgumentNullException)
        //    {
        //        Console.WriteLine("There was an error with removing element from the database.");
        //        throw;
        //    }
        //}

        //public static void UpdateAirline(Airline airline)
        //{
        //    using var airlineRepository = new AirlineRepository();

        //    try
        //    {
        //        airlineRepository.UpdateAirline(airline);
        //        Console.WriteLine("Airline has been updated!");
        //    }
        //    catch (Exception)
        //    {
        //        Console.WriteLine("There was an error with updating element from the database.");
        //        throw;
        //    }
        //}
    }
}
