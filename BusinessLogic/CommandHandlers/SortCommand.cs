﻿using DataContracts;
using System.Text;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable IDE0305 // Simplify collection initialization
namespace BusinessLogic.CommandHandlers
{
    public static class SortCommand
    {
        internal static StringBuilder HandleSort(string[] commandLine, HashSet<Airport> airports, HashSet<Airline> airlines, HashSet<Flight> flights)
        {
            StringBuilder sb = new StringBuilder();

            if (commandLine.Length == 3)
            {
                string typeToSort = commandLine[1];
                string order = commandLine[2];

                if (typeToSort == "airports")
                {
                    HashSet<Airport> sortedAirports = [];

                    if (order == "ascending")
                    {
                        sortedAirports = airports.OrderBy(a => a.Name).ToHashSet();
                    }
                    else if (order == "descending")
                    {
                        sortedAirports = airports.OrderByDescending(a => a.Name).ToHashSet();
                    }

                    sb.AppendLine(BuildMessages.BuildListMessage(sortedAirports, airports => airports.Name).ToString());
                }
                else if (typeToSort == "airlines")
                {
                    HashSet<Airline> sortedAirlines = [];

                    if (order == "ascending")
                    {
                        sortedAirlines = airlines.OrderBy(a => a.Name).ToHashSet();
                    }
                    else if (order == "descending")
                    {
                        sortedAirlines = airlines.OrderByDescending(a => a.Name).ToHashSet();
                    }

                    sb.AppendLine(BuildMessages.BuildListMessage(sortedAirlines, airlines => airlines.Name).ToString());
                }
                else if (typeToSort == "flights")
                {
                    HashSet<Flight> sortedFlights = [];

                    if (order == "ascending")
                    {
                        sortedFlights = flights.OrderBy(a => a.Id).ToHashSet();
                    }
                    else if (order == "descending")
                    {
                        sortedFlights = flights.OrderByDescending(a => a.Id).ToHashSet();
                    }

                    sb.AppendLine(BuildMessages.BuildListMessage(sortedFlights, flights => flights.Id).ToString());
                }
            }
            else if (commandLine.Length == 2)
            {
                string typeToSort = commandLine[1];

                if (typeToSort == "airports")
                {
                    sb.AppendLine(BuildMessages.BuildListMessage(airports.OrderBy(a => a.Name).ToHashSet(), airports => airports.Name).ToString());
                }
                else if (typeToSort == "airlines")
                {
                    sb.AppendLine(BuildMessages.BuildListMessage(airlines.OrderBy(a => a.Name).ToHashSet(), airlines => airlines.Name).ToString());
                }
                else if (typeToSort == "flights")
                {
                    sb.AppendLine(BuildMessages.BuildListMessage(flights.OrderBy(a => a.Id).ToHashSet(), flights => flights.Id).ToString());
                }
            }

            return sb;
        }
    }
}
