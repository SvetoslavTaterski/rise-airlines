﻿using DataContracts;
using System.Text;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used

namespace BusinessLogic.CommandHandlers
{
    public static class ExistCommand
    {
        internal static StringBuilder HandleExist(string[] commandLine, HashSet<Airline> airlines)
        {
            StringBuilder sb = new StringBuilder();

            string airlineToBeFound = commandLine[1];

            sb.AppendLine(airlines.Any(a => a.Name == airlineToBeFound).ToString());

            return sb;
        }
    }
}
