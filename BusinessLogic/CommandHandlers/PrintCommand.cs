﻿using DataContracts;
using DataContracts.Exceptions;
using System.Text;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used
namespace BusinessLogic.CommandHandlers
{
    public static class PrintCommand
    {
        internal static StringBuilder HandlePrint(LinkedList<Flight> route)
        {
            StringBuilder sb = new StringBuilder();

            if (route.Count > 0)
            {
                sb.AppendLine("Route:");

                foreach (var flight in route)
                {
                    sb.AppendLine($"Flight {flight.Id} Departure airport {flight.DepartureAirport} Arrival Airport {flight.ArrivalAirport}");
                }
            }
            else
            {
                throw new CollectionIsEmptyException("The route is empty!");
            }

            return sb;
        }
    }
}
