﻿using DataContracts;
using DataContracts.Aircrafts;
using DataContracts.Exceptions;
using System.Text;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0078 // Use pattern matching
#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable IDE0060 // Remove unused parameter

namespace BusinessLogic.CommandHandlers
{
    public static class ReserveTicketCommand
    {
        internal static StringBuilder ReserveTicketCommandHandler(string id, int seats, int smallBaggageCount, int largeBaggageCount, HashSet<Flight> flights)
        {
            StringBuilder sb = new StringBuilder();

            return sb;

            //Flight? flight = GetFlightIfExist(id, flights);

            //if (flight != null)
            //{
            //    if (flight.AircraftModel.GetType().Name != "PassengerAircraft"
            //        && flight.AircraftModel.GetType().Name != "PrivateAircraft")
            //    {
            //        sb.AppendLine("You cant reserve tickets on this type of aircraft");
            //        return sb;
            //    }
            //    else
            //    {
            //        if (flight.AircraftModel.GetType().Name == "PassengerAircraft")
            //        {
            //            PassengerAircraft aircraft = (PassengerAircraft)flight.AircraftModel;

            //            if (aircraft.Load >= CalculateTotalWeight(smallBaggageCount, largeBaggageCount)
            //                && aircraft.Volume >= CalculateTotalVolume(smallBaggageCount, largeBaggageCount)
            //                && aircraft.Seats >= seats)
            //            {
            //                sb.AppendLine("You reserved Tickets!");
            //                return sb;
            //            }
            //            else
            //            {
            //                sb.AppendLine("You cant reserve Tickets for this aircraft!");
            //                return sb;
            //            }
            //        }
            //        else if (flight.AircraftModel.GetType().Name == "PrivateAircraft")
            //        {
            //            PrivateAircraft aircraft = (PrivateAircraft)flight.AircraftModel;

            //            if (aircraft.Seats >= seats)
            //            {
            //                sb.AppendLine("You reserved Tickets!");
            //                return sb;
            //            }
            //            else
            //            {
            //                sb.AppendLine("You cant reserve tickets for this aircraft!");
            //                return sb;
            //            }
            //        }
            //        throw new NoSuchItemException("There is no such type of aircraft!");
            //    }
            //}
            //else
            //{
            //    throw new NoSuchItemException("There is no such flight with the given Id!");
            //}
        }

        public static Flight? GetFlightIfExist(string id, HashSet<Flight> flights)
        {
            foreach (var flight in flights)
            {
                if (flight.Id == id)
                {
                    return flight;
                }
            }

            return null;
        }

        public static double CalculateTotalWeight(int smallBaggageCount, int largeBaggageCount)
            => (smallBaggageCount * 15) + (largeBaggageCount * 30);

        public static double CalculateTotalVolume(int smallBaggageCount, int largeBaggageCount)
            => (smallBaggageCount * 0.045) + (largeBaggageCount * 0.090);
    }
}
