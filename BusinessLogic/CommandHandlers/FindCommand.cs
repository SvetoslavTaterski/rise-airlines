﻿using DataContracts;
using System.Text;
using static DataContracts.Graph;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
namespace BusinessLogic.CommandHandlers
{
    public static class FindCommand
    {
        internal static StringBuilder HandleFind(Graph graph, string destinationAirport, string sourceAirport)
        {
            var builder = new StringBuilder();

            var routeForPrint = graph.FindPath(sourceAirport!, destinationAirport);
            if (routeForPrint != null)
            {
                builder.AppendLine($"Route from {sourceAirport} to {destinationAirport}: {string.Join(" -> ", routeForPrint)}");
            }
            else
            {
                builder.AppendLine($"No route found from {sourceAirport} to {destinationAirport}.");
            }

            return builder;
        }
    }
}
