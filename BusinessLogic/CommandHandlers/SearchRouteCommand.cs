﻿using DataContracts;
using System.Text;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable CS8604 // Possible null reference argument.
#pragma warning disable IDE0058 // Expression value is never used
namespace BusinessLogic.CommandHandlers
{
    public class SearchRouteCommand
    {
        public static StringBuilder SearchRouteCommandHandler(string sourceAirport, string destinationAirport, Graph graph)
        {
            var sb = new StringBuilder();

            string result = string.Join("->", graph.FindRouteByCost(sourceAirport, destinationAirport));
            sb.AppendLine(result);

            return sb;
        }

        public static StringBuilder SearchRouteCommandHandlerTime(string sourceAirport, string destinationAirport, Graph graph)
        {
            var sb = new StringBuilder();

            string result = string.Join("->", graph.FindRouteByTime(sourceAirport, destinationAirport));
            sb.AppendLine(result);

            return sb;
        }

        public static StringBuilder SearchRouteCommandHandlerStops(string sourceAirport, string destinationAirport, Graph graph)
        {
            var sb = new StringBuilder();

            string result = string.Join("->", graph.FindRouteByLeastStops(sourceAirport, destinationAirport));
            sb.AppendLine(result);

            return sb;
        }
    }
}
