﻿using DataContracts;
using DataContracts.Exceptions;
using System.Text;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used
namespace BusinessLogic.CommandHandlers
{
    public static class AddFlightCommand
    {
        internal static StringBuilder HandleAddFlightCommand(string id, LinkedList<Flight> route, HashSet<Flight> flights)
        {
            StringBuilder sb = new StringBuilder();

            Flight? newFlightForRoute = IsFlightExisting(id, flights);

            if (newFlightForRoute != null)
            {
                string departureAirport = newFlightForRoute.DepartureAirport;

                if (!route.Contains(newFlightForRoute))
                {
                    if (route.Count == 0 || route.Last!.Value.ArrivalAirport == departureAirport)
                    {
                        route.AddLast(newFlightForRoute);
                        sb.AppendLine("Flight has been added to the route!");
                    }
                }
                else
                {
                    sb.AppendLine("Flight is already added to the route!");
                }
            }
            else
            {
                throw new NoSuchItemException("There is no such flight!");
            }

            return sb;
        }

        public static Flight? IsFlightExisting(string id, HashSet<Flight> flights)
        {
            foreach (var flight in flights)
            {
                if (flight.Id == id)
                {
                    return flight;
                }
            }
            return null;
        }
    }
}
