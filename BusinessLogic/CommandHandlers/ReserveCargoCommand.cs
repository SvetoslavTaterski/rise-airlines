﻿using DataContracts.Aircrafts;
using DataContracts;
using System.Text;
using DataContracts.Exceptions;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable IDE0060 // Remove unused parameter
namespace BusinessLogic.CommandHandlers
{
    public static class ReserveCargoCommand
    {
        internal static StringBuilder HandleReserveCargoCommand(string id, int weight, double volume, HashSet<Flight> flights)
        {
            var sb = new StringBuilder();

            return sb;

            //Flight? flight = GetFlightIfExist(id, flights);

            //if (flight != null)
            //{
            //    if (flight.AircraftModel.GetType().Name != "CargoAircraft")
            //    {
            //        sb.AppendLine("You cant reserve cargo on this type of aircraft");
            //        return sb;
            //    }
            //    else
            //    {
            //        CargoAircraft aircraft = (CargoAircraft)flight.AircraftModel;

            //        if (aircraft.Load >= weight && aircraft.Volume >= volume)
            //        {
            //            sb.AppendLine("You reserved cargo!");
            //            return sb;
            //        }
            //        else
            //        {
            //            sb.AppendLine("You cant reserve that cargo!");
            //            return sb;
            //        }
            //    }
            //}
            //else
            //{
            //    throw new NoSuchItemException("There is no such flight with the given Id!");
            //}
        }

        public static Flight? GetFlightIfExist(string id, HashSet<Flight> flights)
        {
            foreach (var flight in flights)
            {
                if (flight.Id == id)
                {
                    return flight;
                }
            }

            return null;
        }
    }
}
