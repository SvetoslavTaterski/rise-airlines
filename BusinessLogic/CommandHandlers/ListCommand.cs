﻿using DataContracts;
using System.Text;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used
namespace BusinessLogic.CommandHandlers
{
    public static class ListCommand
    {
        internal static StringBuilder HandleList(string[] commandLine, HashSet<Airport> airports)
        {
            string name = commandLine[1];
            string cityOrCountry = commandLine[2];

            var sb = new StringBuilder();

            if (cityOrCountry == "City")
            {
                var allAirportsInThisCity = airports.Where(a => a.City == name).ToHashSet();
                sb.AppendLine($"All airports in {name}:");

                foreach (var airport in allAirportsInThisCity)
                {
                    sb.Append($"{airport.Name}, ");
                }

                return sb.Remove(sb.Length - 2, 2);
            }
            else if (cityOrCountry == "Country")
            {
                var allAirportsInThisCountry = airports.Where(a => a.Country == name).ToHashSet();
                sb.AppendLine($"All airports in {name}:");

                foreach (var airport in allAirportsInThisCountry)
                {
                    sb.Append($"{airport.Name}, ");
                }

                return sb.Remove(sb.Length - 2, 2);
            }

            return sb;
        }
    }
}
