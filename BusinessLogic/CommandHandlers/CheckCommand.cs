﻿using DataContracts;
using static DataContracts.Graph;
using System.Text;

#pragma warning disable IDE0058 // Expression value is never used
namespace BusinessLogic.CommandHandlers
{
    public static class CheckCommand
    {
        internal static StringBuilder HandleCheck(Graph graph, string destinationAirport, string sourceAirport)
        {
            var sb = new StringBuilder();
            sb.AppendLine(graph.AreAirportsConnected(sourceAirport, destinationAirport).ToString());
            return sb;
        }
    }
}
