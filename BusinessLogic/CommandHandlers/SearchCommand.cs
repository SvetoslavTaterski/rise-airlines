﻿using DataContracts;
using DataContracts.Exceptions;
using System.Text;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
namespace BusinessLogic.CommandHandlers
{
    public static class SearchCommand
    {
        internal static StringBuilder HandleSearch(string[] commandLine, HashSet<Airport> airports, HashSet<Airline> airlines, HashSet<Flight> flights)
        {
            StringBuilder sb = new StringBuilder();

            string type = commandLine[1];
            string name = commandLine[2];

            if (commandLine.Length > 2)
            {
                for (int i = 3; i <= commandLine.Length - 1; i++)
                {
                    name += " " + commandLine[i];
                }
            }

            if (type == "airport")
            {
                Airport airport = airports.FirstOrDefault(a => a.Name == name);

                if (airport != null)
                {
                    sb.AppendLine($"There is an airport with the given name!");
                }
                else
                {
                    sb.AppendLine("There is no such airport!");
                }
            }
            else if (type == "airline")
            {
                Airline airline = airlines.FirstOrDefault(a => a.Name == name);

                if (airline != null)
                {
                    sb.AppendLine($"There is an airline with the given name!");
                }
                else
                {
                    sb.AppendLine("There is no such airline!");
                }
            }
            else if (type == "flight")
            {
                Flight flight = flights.FirstOrDefault(a => a.Id == name);

                if (flight != null)
                {
                    sb.AppendLine($"There is an flight with the given id!");
                }
                else
                {
                    sb.AppendLine("There is no such flight!");
                }
            }
            else
            {
                throw new NoSuchItemException("No such type!");
            }

            return sb;
        }
    }
}
