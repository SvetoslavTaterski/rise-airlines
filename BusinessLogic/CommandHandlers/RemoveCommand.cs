﻿using DataContracts;
using DataContracts.Exceptions;
using System.Text;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used
namespace BusinessLogic.CommandHandlers
{
    public static class RemoveCommand
    {
        internal static StringBuilder HandleRemove(LinkedList<Flight> route)
        {
            StringBuilder sb = new StringBuilder();

            if (route.Count > 0)
            {
                route.RemoveLast();
                sb.AppendLine("Last flight from route removed.");
            }
            else
            {
                throw new CollectionIsEmptyException("The route is empty! There is nothing to remove...");
            }

            return sb;
        }
    }
}
