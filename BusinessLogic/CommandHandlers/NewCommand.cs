﻿using DataContracts;
using System.Text;

#pragma warning disable IDE0058 // Expression value is never used
namespace BusinessLogic.CommandHandlers
{
    public static class NewCommand
    {
        internal static StringBuilder HandleNewCommand(LinkedList<Flight> route)
        {
            var sb = new StringBuilder();
            route.Clear();
            sb.AppendLine("Creating a new route.");

            return sb;
        }
    }
}
