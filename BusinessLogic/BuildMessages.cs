﻿using System.Text;

#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable IDE0058 // Expression value is never used

namespace BusinessLogic
{
    public static class BuildMessages
    {
        internal static StringBuilder BuildListMessage<T>(IEnumerable<T> items, Func<T, string> nameSelector)
        {
            StringBuilder builder = new StringBuilder();

            string typeName = typeof(T).Name;

            builder.AppendLine(typeName + "s:");

            foreach (var item in items)
            {
                string name = nameSelector(item);

                builder.Append(name + " ");
            }

            return builder;
        }
    }
}
