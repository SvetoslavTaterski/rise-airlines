﻿#pragma warning disable IDE0032 // Use auto property
#pragma warning disable IDE0027 // Use expression body for accessor
#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

namespace DataContracts.Aircrafts
{
    public class Aircraft
    {
        private string _name;

        public Aircraft(string name)
        {
            Name = name;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public static Aircraft CreateCargoAircraft(string name, int load, double volume)
            => new CargoAircraft(name, load, volume);

        public static Aircraft CreatePassangerAircraft(string name, int load, double volume, int seats)
            => new PassengerAircraft(name, load, volume, seats);

        public static Aircraft CreatePrivateAircraft(string name, int seats)
            => new PrivateAircraft(name, seats);
    }
}
