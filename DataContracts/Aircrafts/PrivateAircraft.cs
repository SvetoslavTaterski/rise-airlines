﻿#pragma warning disable IDE0027 // Use expression body for accessor
#pragma warning disable IDE0032 // Use auto property
#pragma warning disable IDE0021 // Use expression body for constructor

namespace DataContracts.Aircrafts
{
    public class PrivateAircraft : Aircraft
    {
        private int _seats;

        public PrivateAircraft(string name, int seats) : base(name)
        {
            Seats = seats;
        }

        public int Seats
        {
            get
            {
                return _seats;
            }
            set
            {
                _seats = value;
            }
        }
    }
}
