﻿#pragma warning disable IDE0027 // Use expression body for accessor
#pragma warning disable IDE0032 // Use auto property

namespace DataContracts.Aircrafts
{
    public class PassengerAircraft : Aircraft
    {
        private int _load;
        private double _volume;
        private int _seats;

        public PassengerAircraft(string name, int load, double volume, int seats) : base(name)
        {
            Load = load;
            Volume = volume;
            Seats = seats;
        }

        public int Load
        {
            get
            {
                return _load;
            }
            set
            {
                _load = value;
            }
        }

        public double Volume
        {
            get
            {
                return _volume;
            }
            set
            {
                _volume = value;
            }
        }

        public int Seats
        {
            get
            {
                return _seats;
            }
            set
            {
                _seats = value;
            }
        }
    }
}
