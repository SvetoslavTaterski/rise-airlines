﻿#pragma warning disable IDE0027 // Use expression body for accessor
#pragma warning disable IDE0032 // Use auto property

namespace DataContracts.Aircrafts
{
    public class CargoAircraft : Aircraft
    {
        private int _load;
        private double _volume;

        public CargoAircraft(string name, int load, double volume) : base(name)
        {
            Load = load;
            Volume = volume;
        }

        public int Load
        {
            get
            {
                return _load;
            }
            set
            {
                _load = value;
            }
        }

        public double Volume
        {
            get
            {
                return _volume;
            }
            set
            {
                _volume = value;
            }
        }
    }
}
