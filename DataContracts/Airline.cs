﻿#pragma warning disable IDE0032 // Use auto property
#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
#pragma warning disable IDE0027 // Use expression body for accessor


namespace DataContracts
{
    public class Airline
    {
        private string _name;

        public Airline(string name)
        {
            Name = name;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
    }
}
