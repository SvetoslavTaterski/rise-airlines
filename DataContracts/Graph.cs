﻿#pragma warning disable IDE0058 // Expression value is never used
#pragma warning disable IDE0028 // Simplify collection initialization
#pragma warning disable IDE0290 // Use primary constructor
#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
#pragma warning disable IDE0021 // Use expression body for constructor

namespace DataContracts
{
    public class Vertex
    {
        public string Airport { get; }
        public List<Edge> Edges { get; }

        public Vertex(string airport)
        {
            Airport = airport;
            Edges = new List<Edge>();
        }
    }

    public class Edge
    {
        public Vertex Destination { get; }
        public double Price { get; }
        public double Time { get; }

        public Edge(Vertex destination, double price, double time)
        {
            Destination = destination;
            Price = price;
            Time = time;
        }
    }

    public class Graph
    {
        public List<Vertex> Vertices { get; }

        public Graph()
        {
            Vertices = new List<Vertex>();
        }

        public void AddFlight(string departureAirport, string arrivalAirport, double price, double time)
        {
            Vertex departureVertex = GetOrCreateVertex(departureAirport);
            Vertex arrivalVertex = GetOrCreateVertex(arrivalAirport);

            departureVertex.Edges.Add(new Edge(arrivalVertex, price, time));
        }

        private Vertex GetOrCreateVertex(string airport)
        {
            Vertex existingVertex = Vertices.FirstOrDefault(v => v.Airport == airport);
            if (existingVertex == null)
            {
                existingVertex = new Vertex(airport);
                Vertices.Add(existingVertex);
            }
            return existingVertex;
        }

        public bool AreAirportsConnected(string airport1, string airport2)
        {
            // Find vertices corresponding to airport1 and airport2
            Vertex vertex1 = Vertices.FirstOrDefault(v => v.Airport == airport1);
            Vertex vertex2 = Vertices.FirstOrDefault(v => v.Airport == airport2);

            // If either vertex is not found, airports are not connected
            if (vertex1 == null || vertex2 == null)
                return false;

            // Use DFS or BFS to check if there's a path from vertex1 to vertex2
            HashSet<Vertex> visited = new HashSet<Vertex>();
            Stack<Vertex> stack = new Stack<Vertex>();

            stack.Push(vertex1);
            visited.Add(vertex1);

            while (stack.Count > 0)
            {
                Vertex currentVertex = stack.Pop();
                if (currentVertex == vertex2)
                    return true;

                foreach (Edge edge in currentVertex.Edges)
                {
                    if (!visited.Contains(edge.Destination))
                    {
                        stack.Push(edge.Destination);
                        visited.Add(edge.Destination);
                    }
                }
            }

            // If no path found, airports are not connected
            return false;
        }

        public List<string>? FindPath(string startAirport, string destinationAirport)
        {
            // Find vertices corresponding to startAirport and destinationAirport
            Vertex startVertex = Vertices.FirstOrDefault(v => v.Airport == startAirport);
            Vertex destinationVertex = Vertices.FirstOrDefault(v => v.Airport == destinationAirport);

            // If either vertex is not found, return null
            if (startVertex == null || destinationVertex == null)
                return null;

            // Use DFS to find the path
            HashSet<Vertex> visited = new HashSet<Vertex>();
            Stack<(Vertex, List<string>)> stack = new Stack<(Vertex, List<string>)>();

            stack.Push((startVertex, new List<string> { startAirport }));
            visited.Add(startVertex);

            while (stack.Count > 0)
            {
                var (currentVertex, path) = stack.Pop();
                string currentAirport = path.Last();

                if (currentVertex == destinationVertex)
                    return path;

                foreach (Edge edge in currentVertex.Edges)
                {
                    if (!visited.Contains(edge.Destination))
                    {
                        List<string> newPath = new List<string>(path);
                        newPath.Add(edge.Destination.Airport);
                        stack.Push((edge.Destination, newPath));
                        visited.Add(edge.Destination);
                    }
                }
            }

            // If no path found, return null
            return null;
        }

        public List<string>? FindRouteByCost(string startAirport, string destinationAirport)
        {
            List<string> prices = new List<string>();
            // Find vertices corresponding to startAirport and destinationAirport
            Vertex startVertex = Vertices.FirstOrDefault(v => v.Airport == startAirport);
            Vertex destinationVertex = Vertices.FirstOrDefault(v => v.Airport == destinationAirport);

            // If either vertex is not found, return null
            if (startVertex == null || destinationVertex == null)
                return null;

            // Initialize data structures for BFS
            Queue<(Vertex, List<string>, double)> queue = new Queue<(Vertex, List<string>, double)>();
            HashSet<Vertex> visited = new HashSet<Vertex>();

            // Start BFS from the startVertex
            queue.Enqueue((startVertex, new List<string> { startAirport }, 0));
            visited.Add(startVertex);

            // Perform BFS
            while (queue.Count > 0)
            {
                var (currentVertex, route, totalCost) = queue.Dequeue();

                if (currentVertex == destinationVertex)
                {
                    Console.WriteLine(prices.Last());
                    return route;
                }

                foreach (Edge edge in currentVertex.Edges)
                {
                    Vertex neighbor = edge.Destination;
                    if (!visited.Contains(neighbor))
                    {
                        List<string> newRoute = new List<string>(route);
                        newRoute.Add(neighbor.Airport);
                        double newTotalCost = totalCost + edge.Price;
                        prices.Add($"Cost: {newTotalCost}");
                        queue.Enqueue((neighbor, newRoute, newTotalCost));
                        visited.Add(neighbor);
                    }
                }
            }

            // If no route found, return null
            return null;
        }

        public List<string>? FindRouteByTime(string startAirport, string destinationAirport)
        {
            List<string> time = new List<string>();

            // Find vertices corresponding to startAirport and destinationAirport
            Vertex startVertex = Vertices.FirstOrDefault(v => v.Airport == startAirport);
            Vertex destinationVertex = Vertices.FirstOrDefault(v => v.Airport == destinationAirport);

            // If either vertex is not found, return null
            if (startVertex == null || destinationVertex == null)
                return null;

            // Initialize data structures for BFS
            Queue<(Vertex, List<string>, double)> queue = new Queue<(Vertex, List<string>, double)>();
            HashSet<Vertex> visited = new HashSet<Vertex>();

            // Start BFS from the startVertex
            queue.Enqueue((startVertex, new List<string> { startAirport }, 0));
            visited.Add(startVertex);

            // Perform BFS
            while (queue.Count > 0)
            {
                var (currentVertex, route, totalTime) = queue.Dequeue();

                if (currentVertex == destinationVertex)
                {
                    Console.WriteLine(time.Last());
                    return route;
                }

                foreach (Edge edge in currentVertex.Edges)
                {
                    Vertex neighbor = edge.Destination;
                    if (!visited.Contains(neighbor))
                    {
                        List<string> newRoute = new List<string>(route);
                        newRoute.Add(neighbor.Airport);
                        double newTotalTime = totalTime + edge.Time;
                        time.Add($"Time: {newTotalTime} hours.");
                        queue.Enqueue((neighbor, newRoute, newTotalTime));
                        visited.Add(neighbor);
                    }
                }
            }

            // If no route found, return null
            return null;
        }

        public List<string>? FindRouteByLeastStops(string startAirport, string destinationAirport)
        {
            List<string> stopsToPrint = new List<string>();
            // Find vertices corresponding to startAirport and destinationAirport
            Vertex startVertex = Vertices.FirstOrDefault(v => v.Airport == startAirport);
            Vertex destinationVertex = Vertices.FirstOrDefault(v => v.Airport == destinationAirport);

            // If either vertex is not found, return null
            if (startVertex == null || destinationVertex == null)
                return null;

            // Initialize data structures for BFS
            Queue<(Vertex, List<string>, int)> queue = new Queue<(Vertex, List<string>, int)>();
            HashSet<Vertex> visited = new HashSet<Vertex>();

            // Start BFS from the startVertex
            queue.Enqueue((startVertex, new List<string> { startAirport }, 0));
            visited.Add(startVertex);

            // Perform BFS
            while (queue.Count > 0)
            {
                var (currentVertex, route, stops) = queue.Dequeue();

                if (currentVertex == destinationVertex)
                {
                    Console.WriteLine(stopsToPrint.Last());
                    return route;
                }

                foreach (Edge edge in currentVertex.Edges)
                {
                    Vertex neighbor = edge.Destination;
                    if (!visited.Contains(neighbor))
                    {
                        List<string> newRoute = new List<string>(route);
                        newRoute.Add(neighbor.Airport);
                        queue.Enqueue((neighbor, newRoute, stops + 1));
                        stopsToPrint.Add($"Stops: {stops}");
                        visited.Add(neighbor);
                    }
                }
            }

            // If no route found, return null
            return null;
        }
    }
}


