﻿#pragma warning disable IDE0290 // Use primary constructor
namespace DataContracts.Exceptions
{
    public class InvalidInputException : Exception
    {
        public InvalidInputException(string message) : base(message)
        {

        }
    }
}
