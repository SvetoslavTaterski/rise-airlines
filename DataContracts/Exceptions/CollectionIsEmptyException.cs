﻿#pragma warning disable IDE0290 // Use primary constructor
namespace DataContracts.Exceptions
{
    public class CollectionIsEmptyException : Exception
    {
        public CollectionIsEmptyException(string message) : base(message)
        {

        }
    }
}
