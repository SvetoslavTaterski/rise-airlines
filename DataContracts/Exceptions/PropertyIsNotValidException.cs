﻿#pragma warning disable IDE0290 // Use primary constructor
namespace DataContracts.Exceptions
{
    public class PropertyIsNotValidException : Exception
    {
        public PropertyIsNotValidException(string message) : base(message)
        {

        }
    }
}
