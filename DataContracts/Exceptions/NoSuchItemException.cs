﻿#pragma warning disable IDE0290 // Use primary constructor
namespace DataContracts.Exceptions
{
    public class NoSuchItemException : Exception
    {
        public NoSuchItemException(string message) : base(message)
        {

        }
    }
}
