﻿#pragma warning disable IDE0290 // Use primary constructor

namespace DataContracts.Exceptions
{
    public class InvalidCommandException : Exception
    {
        public InvalidCommandException(string message) : base(message)
        {

        }
    }
}
