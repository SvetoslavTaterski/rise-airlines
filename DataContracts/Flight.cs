﻿#pragma warning disable IDE0032 // Use auto property
#pragma warning disable IDE0021 // Use expression body for constructor
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
#pragma warning disable IDE0027 // Use expression body for accessor
using DataContracts.Aircrafts;

namespace DataContracts
{
    public class Flight
    {
        private string _id;
        private string _departureAirport;
        private string _arrivalAirport;
        private double _price;
        private double _time;

        public Flight(string id, string departureAirport, string arrivalAirport, double price, double time)
        {
            Id = id;
            DepartureAirport = departureAirport;
            ArrivalAirport = arrivalAirport;
            Price = price;
            Time = time;
        }


        public string Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string DepartureAirport
        {
            get
            {
                return _departureAirport;
            }
            set
            {
                _departureAirport = value;
            }
        }

        public string ArrivalAirport
        {
            get
            {
                return _arrivalAirport;
            }
            set
            {
                _arrivalAirport = value;
            }
        }

        public double Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
            }
        }

        public double Time
        {
            get
            {
                return _time;
            }
            set
            {
                _time = value;
            }
        }
    }
}
