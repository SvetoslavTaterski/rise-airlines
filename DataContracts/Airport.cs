﻿#pragma warning disable IDE0007 // Use implicit type
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
#pragma warning disable IDE0027 // Use expression body for accessor
#pragma warning disable IDE0078 // Use pattern matching
#pragma warning disable IDE0045 // Convert to conditional expression

using System.Text.RegularExpressions;
using DataContracts.Exceptions;

namespace DataContracts
{
    public class Airport
    {
        private string _id;
        private string _name;
        private string _city;
        private string _country;

        public Airport(string id, string name, string city, string country)
        {
            Name = name;
            Country = country;
            City = city;
            Id = id;
        }

        public string Id
        {
            get
            {
                return _id;
            }
            set
            {
                if (IsAlphanumeric(value) && CheckIfIdIsCorrectLength(value))
                {
                    _id = value;
                }
                else
                {
                    throw new PropertyIsNotValidException("Id is not valid!");
                }
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (ContainsOnlyAlphabetAndSpace(value))
                {
                    _name = value;
                }
                else
                {
                    throw new PropertyIsNotValidException("Name is not valid!");
                }
            }
        }

        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (ContainsOnlyAlphabetAndSpace(value))
                {
                    _city = value;
                }
                else
                {
                    throw new PropertyIsNotValidException("City is not valid!");
                }
            }
        }

        public string Country
        {
            get
            {
                return _country;
            }
            set
            {
                if (ContainsOnlyAlphabetAndSpace(value))
                {
                    _country = value;
                }
                else
                {
                    throw new PropertyIsNotValidException("Country is not valid!");
                }
            }
        }

        private static bool CheckIfIdIsCorrectLength(string id)
        {
            if (id.Length >= 2 && id.Length <= 4)
            {
                return true;
            }

            return false;
        }

        private static bool ContainsOnlyAlphabetAndSpace(string inputString)
        {
            string pattern = @"^[a-zA-Z ]+$";

            Regex regex = new Regex(pattern);

            return regex.IsMatch(inputString);
        }

        public static bool IsAlphanumeric(string input)
        {
            foreach (char c in input)
            {
                if (!char.IsLetterOrDigit(c))
                {
                    return false;
                }
            }
            return true;
        }
    }
}

