document.addEventListener('DOMContentLoaded', () => {
    const currentURL = window.location.pathname;

    if (currentURL.includes('flights.html')) {
        fetchFlights();
        document.getElementById('submit-flight').addEventListener('click', async (event) => {
            event.preventDefault();
            await createFlight();
        });
    } else if (currentURL.includes('airports.html')) {
        fetchAirports();
        document.getElementById('submit-airport').addEventListener('click', async (event) => {
            event.preventDefault();
            await createAirport();
        });
    } else if (currentURL.includes('airlines.html')) {
        fetchAirlines();
        document.getElementById('submit-airline').addEventListener('click', async (event) => {
            event.preventDefault();
            await createAirline();
        });
    }
});
  
async function fetchFlights() {
    try {
        const response = await fetch('https://localhost:7129/api/Flight');
        const data = await response.json();
        const flightsTableBody = document.getElementById('flights-table-body');
        flightsTableBody.innerHTML = '';
        data.forEach(flight => {
            const row = document.createElement('tr');
            row.innerHTML = `
                <td>${flight.flightNumber}</td>
                <td>${flight.fromAirportId}</td>
                <td>${flight.toAirportId}</td>
                <td>${flight.departure}</td>
                <td>${flight.arrival}</td>
                <td class="action-buttons">
                    <button onclick="editRow(this)">Edit</button>
                    <button onclick="deleteFlight('${flight.flightNumber}')">Delete</button>
                </td>
            `;
            flightsTableBody.appendChild(row);
        });
    } catch (error) {
        console.error('Error fetching flights:', error);
    }
}

async function fetchAirports() {
    try {
        const response = await fetch('https://localhost:7129/api/Airport');
        const data = await response.json();
        const airportsTableBody = document.getElementById('airport-table-body');
        airportsTableBody.innerHTML = '';
        data.forEach(airport => {
            const row = document.createElement('tr');
            row.innerHTML = `
                <td>${airport.airportName}</td>
                <td>${airport.country}</td>
                <td>${airport.city}</td>
                <td>${airport.code}</td>
                <td>${airport.runwayCount}</td>
                <td>${airport.founded}</td>
                <td class="action-buttons">
                    <button onclick="editRow(this)">Edit</button>
                    <button onclick="deleteAirport('${airport.airportName}')">Delete</button>
                </td>
            `;
            airportsTableBody.appendChild(row);
        });
    } catch (error) {
        console.error('Error fetching airports:', error);
    }
}

async function fetchAirlines() {
    try {
        const response = await fetch('https://localhost:7129/api/Airline');
        const data = await response.json();
        const airlinesTableBody = document.getElementById('airline-table-body');
        airlinesTableBody.innerHTML = '';
        data.forEach(airline => {
            const row = document.createElement('tr');
            row.innerHTML = `
                <td>${airline.airlineName}</td>
                <td>${airline.founded}</td>
                <td>${airline.fleetSize}</td>
                <td>${airline.airlineDescription}</td>
                <td class="action-buttons">
                    <button onclick="editRow(this)">Edit</button>
                    <button onclick="deleteAirline('${airline.airlineName}')">Delete</button>
                </td>
            `;
            airlinesTableBody.appendChild(row);
        });
    } catch (error) {
        console.error('Error fetching airlines:', error);
    }
}

async function createFlight() {
    const flightNumber = document.getElementById('flightId').value;
    const from = document.getElementById('fromId').value;
    const to = document.getElementById('toId').value;
    const departure = document.getElementById('departureId').value;
    const arrival = document.getElementById('arrivalId').value;

    const flight = {
        flightNumber: flightNumber,
        fromAirportId: from,
        toAirportId: to,
        departure: departure,
        arrival: arrival
    };

    try {
        const response = await fetch('https://localhost:7129/api/Flight', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(flight)
        });

        if (response.ok) {
            alert('Flight created successfully!');
            fetchFlights();
        } else {
            const errorText = await response.text();
            alert(`Error: ${errorText}`);
        }
    } catch (error) {
        console.error('Error creating flight:', error);
        alert('Error creating flight. Please try again.');
    }
}

async function createAirport() {
    const name = document.getElementById('nameId').value;
    const country = document.getElementById('countryID').value;
    const city = document.getElementById('cityId').value;
    const code = document.getElementById('codeId').value;
    const runways = document.getElementById('runwaysId').value;
    const founded = document.getElementById('foundedId').value;

    const airport = {
        airportName: name,
        country: country,
        city: city,
        code: code,
        runwayCount: runways,
        founded: founded
    };

    try {
        const response = await fetch('https://localhost:7129/api/Airport', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(airport)
        });

        if (response.ok) {
            alert('Airport created successfully!');
            fetchAirports();
        } else {
            const errorText = await response.text();
            alert(`Error: ${errorText}`);
        }
    } catch (error) {
        console.error('Error creating airport:', error);
        alert('Error creating airport. Please try again.');
    }
}

async function createAirline() {
    const name = document.getElementById('nameId').value;
    const founded = document.getElementById('foundedId').value;
    const fleetSize = document.getElementById('fleetId').value;
    const description = document.getElementById('descriptionId').value;

    const airline = {
        airlineName: name,
        founded: founded,
        fleetSize: fleetSize,
        airlineDescription: description
    };

    try {
        const response = await fetch('https://localhost:7129/api/Airline', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(airline)
        });

        if (response.ok) {
            alert('Airline created successfully!');
            fetchAirlines();
        } else {
            const errorText = await response.text();
            alert(`Error: ${errorText}`);
        }
    } catch (error) {
        console.error('Error creating airline:', error);
        alert('Error creating airline. Please try again.');
    }
}

async function deleteFlight(flightNumber) {
    if (!confirm('Are you sure you want to delete this flight?')) {
        return;
    }

    try {
        const response = await fetch(`https://localhost:7129/api/Flight/flightNumber/${flightNumber}`, {
            method: 'DELETE'
        });

        if (response.ok) {
            alert('Flight deleted successfully!');
            fetchFlights();
        } else {
            const errorText = await response.text();
            alert(`Error: ${errorText}`);
        }
    } catch (error) {
        console.error('Error deleting flight:', error);
        alert('Error deleting flight. Please try again.');
    }
}

async function deleteAirport(airportName) {
    if (!confirm('Are you sure you want to delete this airport?')) {
        return;
    }

    try {
        const response = await fetch(`https://localhost:7129/api/Airport/airportName/${airportName}`, {
            method: 'DELETE'
        });

        if (response.ok) {
            alert('Airport deleted successfully!');
            fetchAirports();
        } else {
            const errorText = await response.text();
            alert(`Error: ${errorText}`);
        }
    } catch (error) {
        console.error('Error deleting airport:', error);
        alert('Error deleting airport. Please try again.');
    }
}

async function deleteAirline(airlineName) {
    if (!confirm('Are you sure you want to delete this airline?')) {
        return;
    }

    try {
        const response = await fetch(`https://localhost:7129/api/Airline/airlineName/${airlineName}`, {
            method: 'DELETE'
        });

        if (response.ok) {
            alert('Airline deleted successfully!');
            fetchAirlines(); // Assuming you have a function to refresh the list of airlines
        } else {
            const errorText = await response.text();
            alert(`Error: ${errorText}`);
        }
    } catch (error) {
        console.error('Error deleting airline:', error);
        alert('Error deleting airline. Please try again.');
    }
}

function editRow(button) {
    const siteUrl = window.location.href;
    if (siteUrl.includes('flights.html')) {
        const row = button.closest('tr');
        const flightNumber = row.cells[0].textContent;
        const fromAirportId = row.cells[1].textContent;
        const toAirportId = row.cells[2].textContent;
        const departure = row.cells[3].textContent;
        const arrival = row.cells[4].textContent;

        document.getElementById('edit-flightNumber').value = flightNumber;
        document.getElementById('edit-fromAirportId').value = fromAirportId;
        document.getElementById('edit-toAirportId').value = toAirportId;
        document.getElementById('edit-departure').value = departure;
        document.getElementById('edit-arrival').value = arrival;

        document.getElementById('edit-flight-modal').style.display = 'block';
        
    } else if (siteUrl.includes('airlines.html')) {
        const row = button.closest('tr');
        const airlineName = row.cells[0].textContent;
        const founded = row.cells[1].textContent;
        const fleetSize = row.cells[2].textContent;
        const airlineDescription = row.cells[3].textContent;

        document.getElementById('edit-airlineName').value = airlineName;
        document.getElementById('edit-founded').value = founded;
        document.getElementById('edit-fleetSize').value = fleetSize;
        document.getElementById('edit-airlineDescription').value = airlineDescription;

        document.getElementById('edit-airline-modal').style.display = 'block';
    } else if (siteUrl.includes('airports.html')) {
        const row = button.closest('tr');
        const airportName = row.cells[0].textContent;
        const country = row.cells[1].textContent;
        const city = row.cells[2].textContent;
        const code = row.cells[3].textContent;
        const runwayCount = row.cells[4].textContent;
        const founded = row.cells[5].textContent;

        document.getElementById('edit-airportName').value = airportName;
        document.getElementById('edit-country').value = country;
        document.getElementById('edit-city').value = city;
        document.getElementById('edit-code').value = code;
        document.getElementById('edit-runwayCount').value = runwayCount;
        document.getElementById('edit-founded').value = founded;

        document.getElementById('edit-airport-modal').style.display = 'block';
    }
    
}

function closeEditModal() {
    document.getElementById('edit-flight-modal').style.display = 'none';
}

async function saveFlight() {
    const flightNumber = document.getElementById('edit-flightNumber').value;
    const fromAirportId = document.getElementById('edit-fromAirportId').value;
    const toAirportId = document.getElementById('edit-toAirportId').value;
    const departure = document.getElementById('edit-departure').value;
    const arrival = document.getElementById('edit-arrival').value;

    const flight = {
        flightNumber,
        fromAirportId,
        toAirportId,
        departure,
        arrival
    };

    try {
        const response = await fetch(`https://localhost:7129/api/Flight/${flightNumber}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(flight)
        });

        if (response.ok) {
            alert('Flight updated successfully!');
            closeEditModal();
            fetchFlights();
        } else {
            const errorText = await response.text();
            alert(`Error: ${errorText}`);
        }
    } catch (error) {
        console.error('Error updating flight:', error);
        alert('Error updating flight. Please try again.');
    }
}

async function saveAirline() {
    const airlineName = document.getElementById('edit-airlineName').value;
    const founded = document.getElementById('edit-founded').value;
    const fleetSize = document.getElementById('edit-fleetSize').value;
    const airlineDescription = document.getElementById('edit-airlineDescription').value;

    const airline = {
        airlineName,
        founded,
        fleetSize,
        airlineDescription
    };

    try {
        const response = await fetch(`https://localhost:7129/api/Airline/${encodeURIComponent(airlineName)}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(airline)
        });

        if (response.ok) {
            alert('Airline updated successfully!');
            closeEditAirlineModal();
            fetchAirlines();
        } else {
            const errorText = await response.text();
            alert(`Error: ${errorText}`);
        }
    } catch (error) {
        console.error('Error updating airline:', error);
        alert('Error updating airline. Please try again.');
    }
}

function closeEditAirlineModal() {
    document.getElementById('edit-airline-modal').style.display = 'none';
}

async function saveAirport() {
    const airportName = document.getElementById('edit-airportName').value;
    const country = document.getElementById('edit-country').value;
    const city = document.getElementById('edit-city').value;
    const code = document.getElementById('edit-code').value;
    const runwayCount = document.getElementById('edit-runwayCount').value;
    const founded = document.getElementById('edit-founded').value;

    const airport = {
        airportName,
        country,
        city,
        code,
        runwayCount,
        founded
    };

    try {
        const response = await fetch(`https://localhost:7129/api/Airport/${encodeURIComponent(airportName)}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(airport)
        });

        if (response.ok) {
            alert('Airport updated successfully!');
            closeEditAirportModal();
            fetchAirports();
        } else {
            const errorText = await response.text();
            alert(`Error: ${errorText}`);
        }
    } catch (error) {
        console.error('Error updating airport:', error);
        alert('Error updating airport. Please try again.');
    }
}
