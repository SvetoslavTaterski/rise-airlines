﻿USE RISE;

INSERT INTO Flights(FlightNumber, FromAirport, ToAirport, Departure, Arrival, AirlineId)
VALUES
(111, 'New York', 'Los Angeles', '2024-04-16T14:00:00', '2024-04-16T14:30:00', 2),
(222, 'Los Angeles', 'Chicago', '2024-04-16T15:30:00', '2024-04-16T16:30:00', 3);

SELECT * FROM Flights
JOIN Airlines ON Flights.AirlineId = Airlines.AirlineId
JOIN Airlines a1 ON Airlines.AirportId = a1.AirportId
WHERE
    CONVERT(DATE, Departure) = DATEADD(DAY, 1, CONVERT(DATE, GETDATE()));

INSERT INTO Flights(FlightNumber, FromAirport, ToAirport, Departure, Arrival, AirlineId)
VALUES
(111, 'New York', 'Los Angeles', '2024-04-16T13:57:00', '2024-04-16T14:30:00', 2),
(222, 'Los Angeles', 'Chicago', '2024-04-16T13:57:00', '2024-04-16T16:30:00', 3);

--Check for departed flights
SELECT COUNT(*) AS DepartedFlightCount
FROM Flights
WHERE Departure < GETDATE();

--Show the most farther flight
SELECT TOP 1
    FlightNumber,
    FromAirport AS DepartureAirport,
    ToAirport AS ArrivalAirport,
    Departure,
    Arrival,
    DATEDIFF(MINUTE, Departure, Arrival) AS FlightDuration
FROM
    Flights
ORDER BY
    DATEDIFF(MINUTE, Departure, Arrival) DESC;