﻿CREATE DATABASE RISE;

USE RISE;

CREATE TABLE Airports(
AirportName NVARCHAR(50) NOT NULL,
Country NVARCHAR(100) NOT NULL,
City NVARCHAR(100) NOT NULL,
Code VARCHAR(3) NOT NULL,
RunwayCount INT NOT NULL,
Founded DATE NOT NULL 
);

CREATE TABLE Airlines(
AirlineName NVARCHAR(50) NOT NULL,
Founded DATE DEFAULT GETDATE() NOT NULL,
FleetSize INT NOT NULL,
AirlineDescription NVARCHAR(500)
);

CREATE TABLE Flights(
FlightNumber VARCHAR(6) NOT NULL,
FromAirport NVARCHAR(50) NOT NULL,
ToAirport NVARCHAR(50) NOT NULL,
Departure DATETIME NOT NULL,
Arrival DATETIME NOT NULL,
);

INSERT INTO Flights(FlightNumber,FromAirport,ToAirport,Departure,Arrival)
VALUES(1,'New York', 'Los Angeles', '08:00:00', '10:00:00'),
	  (2,'Los Angeles', 'Chicago', '09:30:00', '11:30:00');

INSERT INTO AIRLINES(AirlineName,Founded,FleetSize,AirlineDescription)
VALUES('AAA','2024-01-01',10,'Description1'),
	  ('BBB','2024-02-02',20,'Description2');

INSERT INTO AIRPORTS(AirportName,Country,City,Code,RunwayCount,Founded)
VALUES('Airport1','USA','New York','AAA',3,'2002-01-01'),
	  ('Airport2','USA','Los Angeles','BBB',4,'2002-02-02');

SELECT FromAirport FROM Flights
WHERE FlightNumber = 1;

UPDATE Flights
SET ToAirport = 'New Destination'
WHERE FlightNumber = 1;

DELETE FROM Flights
WHERE FlightNumber = 1;