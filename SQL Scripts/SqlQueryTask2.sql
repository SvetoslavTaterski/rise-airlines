﻿CREATE DATABASE RISE;

USE RISE;

CREATE TABLE Airports(
AirportId INT PRIMARY KEY IDENTITY,
AirportName NVARCHAR(50) NOT NULL,
Country NVARCHAR(100) NOT NULL,
City NVARCHAR(100) NOT NULL,
Code VARCHAR(3) NOT NULL,
RunwayCount INT NOT NULL,
Founded DATE NOT NULL 
);

CREATE TABLE Airlines(
AirlineId INT PRIMARY KEY IDENTITY,
AirlineName NVARCHAR(50) NOT NULL,
Founded DATE DEFAULT GETDATE() NOT NULL,
FleetSize INT NOT NULL,
AirlineDescription NVARCHAR(500),
AirportId INT FOREIGN KEY REFERENCES Airports(AirportId) NOT NULL
);

CREATE TABLE Flights(
FlightId INT PRIMARY KEY IDENTITY,
FlightNumber VARCHAR(6) NOT NULL,
FromAirport NVARCHAR(50) NOT NULL,
ToAirport NVARCHAR(50) NOT NULL,
Departure DATETIME NOT NULL,
Arrival DATETIME NOT NULL,
AirlineId INT FOREIGN KEY REFERENCES  Airlines(AirlineId) NOT NULL
);

ALTER TABLE Flights
ADD CONSTRAINT Check_Departure_Arrival_Not_In_Past
CHECK (Departure >= GETDATE() AND Arrival >= GETDATE());

ALTER TABLE Flights
ADD TimeDifference AS (DATEDIFF(MINUTE, Departure, Arrival)) PERSISTED,
    CONSTRAINT Check_Arrival_After_Departure CHECK (DATEDIFF(MINUTE, Departure, Arrival) >= 0);

INSERT INTO Flights(FlightNumber, FromAirport, ToAirport, Departure, Arrival, AirlineId)
VALUES
(111, 'New York', 'Los Angeles', '2024-04-15T14:00:00', '2024-04-15T14:30:00', 2),
(222, 'Los Angeles', 'Chicago', '2024-04-15T15:30:00', '2024-04-15T16:30:00', 3);

INSERT INTO AIRLINES(AirlineName,Founded,FleetSize,AirlineDescription,AirportId)
VALUES('AAA','2024-01-01',10,'Description1',1),
	  ('BBB','2024-02-02',20,'Description2',2);

INSERT INTO AIRPORTS(AirportName,Country,City,Code,RunwayCount,Founded)
VALUES('Airport1','USA','New York','AAA',3,'2002-01-01'),
	  ('Airport2','USA','Los Angeles','BBB',4,'2002-02-02');

SELECT FromAirport FROM Flights
WHERE FlightNumber = 1;

UPDATE Flights
SET ToAirport = 'New Destination'
WHERE FlightNumber = 1;

DELETE FROM Flights
WHERE FlightNumber = 1;

--This script does not work because the departure time is in the past
INSERT INTO Flights(FlightNumber, FromAirport, ToAirport, Departure, Arrival, AirlineId)
VALUES
(111, 'New York', 'Los Angeles', '2002-04-15T14:00:00', '2024-04-15T14:30:00', 2),
(222, 'Los Angeles', 'Chicago', '2002-04-15T15:30:00', '2024-04-15T16:30:00', 3);

--This script does not work because arrival time is befoure departure time
INSERT INTO Flights(FlightNumber, FromAirport, ToAirport, Departure, Arrival, AirlineId)
VALUES
(111, 'New York', 'Los Angeles', '2024-04-15T14:00:00', '2023-04-15T14:30:00', 2),
(222, 'Los Angeles', 'Chicago', '2024-04-15T15:30:00', '2023-04-15T16:30:00', 3);

ALTER TABLE Flights
ADD DepartureAirportId INT FOREIGN KEY REFERENCES Airports(AirportId),
    ArrivalAirportId INT FOREIGN KEY REFERENCES Airports(AirportId);

ALTER TABLE Airports
ADD CONSTRAINT UC_Code UNIQUE (Code);

CREATE INDEX IX_Airports_Code ON Airports(Code);
CREATE INDEX IX_Flights_DepartureAirportId ON Flights(DepartureAirportId);
CREATE INDEX IX_Flights_ArrivalAirportId ON Flights(ArrivalAirportId);

